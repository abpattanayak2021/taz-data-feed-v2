import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

const TripsDailyDashboardSchema = new Schema({
    _id: {type: String},
    vin: {type: String},
    tripId: {type: String},
    tripData: {type: Object}
}, {
    timestamps: true
});

export default mongoose.model('TripDashboardDaily', TripsDailyDashboardSchema);