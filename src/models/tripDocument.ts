import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

const tripDocumentSchema = new Schema({
    _id: {type: String},
    vin: {type: String},
    tripId: {type: String},
    tripData: {type: Object},
});

export default mongoose.model('Trips', tripDocumentSchema);