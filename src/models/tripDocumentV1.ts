import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

const tripDocumentv1Schema = new Schema({
    _id: {type: String},
    vin: {type: String},
    tripId: {type: String},
    tripData: {type: Array},
});

export default mongoose.model('Tripsv1', tripDocumentv1Schema);