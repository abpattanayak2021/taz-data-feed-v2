import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

const tripDistanceSchema = new Schema({    
    companyId: {type: String},
    distance: {type: Number},
    date: {type: Date},
    count: {type: Number},
    skip: {type: Number}
});

export default mongoose.model('TripDistance', tripDistanceSchema);