import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

const tripDocumetSchema = new Schema({
    _id: {type: String},
    vin: {type: String},
    tripId: {type: String},
    tripData: {type: Object},
});

export default mongoose.model('Trips', tripDocumetSchema);