import { Request, Response, Next } from 'express';
import { prisma } from '../../Shared/DBManager/Prisma/prisma-client';
import { elastic } from '../../Shared/DBManager/Elastic/elastic';
import { db } from '../../Shared/CacheManager/cacheManager';
import * as _ from 'lodash';
const moment = require('moment');
import Logger from '../utilities/logger';


const VEHICLE_LEVEL_2_INDEX = 'vehicle002';

const searchFromElastic = async ({query, index}) => {
    return elastic.search({
        index,
        query,
    });
};

const getTripsDataTripIdV2 = async (tripId) => {
    let reqTripId = tripId;
    let status;
    let tripVin;
    if (status) {
        tripVin = await prisma.trip({ tripId: reqTripId }).vin();
        if (tripVin) {
            const vehicle = await prisma.vehicle({ vin: tripVin });
            if (!vehicle || vehicle && vehicle.status !== status) {
                return { status: 400, data: 'INVALID VIN' };
            }
        }
    }
    const query: any = {
        bool: {
            must: [{
                match_all: {},
            }, {
                match_phrase: {
                    "tripId": reqTripId
                }
            }],
            must_not: [
                {
                    match_phrase: {
                        "timestampVerified": {
                            query: false,
                        },
                    },
                },
                {
                    match_phrase: {
                        "latitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "longitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "batteryVoltageAdc": 0
                    }
                }
            ],
        },
    };

    let data = await searchFromElastic({
        index: VEHICLE_LEVEL_2_INDEX,
        query: {
            _source: [],
            query,
            size: "all",
            sort: [
                {
                    timestamp: {
                        order: "desc",
                    },
                },
            ],
        },
    });
    let body = data.searchData;
    let resp: any = {
        vin: '',
        ignition: [],
        battery: [],
        uart: [],
        location: [],
        alarm: []
    }
    body.forEach(log => {
        resp.vin = log.vin
        const type = log._source.type;
        switch (type) {
            case 'I':
                resp.ignition.push(log._source)
                break;
            case 'L':
                resp.location.push(log._source)
                break;
            case 'B':
                resp.battery.push(log._source)
                break;
            case 'A':
                resp.alarm.push(log._source)
                break;
            case 'U':
                resp.uart.push(log._source)
                break;
        }
    });
    return resp;

}

export const driverScoreController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running driver score router for driver score")
    let vin = request.query.vin;
    try {
        const lease: any = async () => {
            let filter: any = {
                asset: {
                    vehicle: {
                        vin: vin
                    }
                },                               
            };
            const companyVin = `
                fragment userOnLease on Leases{
                    id
                    status
                    startTime
                    endTime
                    leasee{
                        user{
                            firstName
                            email
                            lastName
                            phone
                        }
                    }                    
                }`

            const userEmail: any = await prisma.leases({ where: filter}).$fragment(companyVin);
            return userEmail;
        };

        const trips: any = async () => {
            let filter: any = {
                vin: vin
            };
            const tripData = `
                fragment tripsOfVin on Trips{
                    tripId                    
                }`;
            const tripid: any = await prisma.trips({ where: filter }).$fragment(tripData);
            let lastTrips = _.takeRight(tripid, 20);
            Logger.info(`Data Received ${lastTrips.length}`);
            Logger.info(`Last 20 Trips Details ${JSON.stringify(lastTrips)}`);
            return lastTrips;
        };
        let data = await trips();
        let user = await lease();
        Logger.info(`Users Details Of Vin ${user[user.length -1].leasee}`);
        let userDetails = {
            status:'',
            firstName: '',
            lastName: '',
            phone: '',
            email: ''
        }
        userDetails = {
            status: user[user.length -1].status,
            firstName: user[user.length -1].leasee.user.firstName,
            lastName: user[user.length -1].leasee.user.lastName,
            phone: user[user.length -1].leasee.user.phone,
            email: user[user.length -1].leasee.user.email
        }
        let tripsScore = [];
            for(let i = 0; i<= data.length -1; i++) {
                let tripDataJson: any = await getTripsDataTripIdV2(data[i].tripId);
                //Null Check
                if (tripDataJson.location.length > 0) {
                    let gpsSpeedData = []
                    let startTime = tripDataJson.location[0].timestamp;
                    let endTime = tripDataJson.location[tripDataJson.location.length - 1].timestamp;
                    let accelerationIndex = 0;
                    let breakingIndex = 0;
                    let deltaMili = moment(startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"));
                    let delta = moment.utc(deltaMili).format("mm");
                    Logger.info(`Time Diff in hours ${delta}`);
                    gpsSpeedData = tripDataJson.location.map(item => {
                        return item.gpsSpeed;
                    });
                    Logger.info(`GPS Speed Data: ${gpsSpeedData}`);
                    let accIncident = 0;
                    let breakIncident = 0;

                    for (let i = 0; i <= gpsSpeedData.length - 1; i++) {
                        if (gpsSpeedData[i + 1] > gpsSpeedData[i]) {
                            accIncident++;
                            if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 4 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 4) {
                                // accelerationIndex = accelerationIndex + (gpsSpeedData[i + 1] - gpsSpeedData[i]);
                                // accelerationIndex = accelerationIndex + accIncident*1;
                                accelerationIndex = accelerationIndex + 1;
                            } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 5 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 6) {
                                // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 2);
                                // accelerationIndex = accelerationIndex + accIncident*2;
                                accelerationIndex = accelerationIndex + 2;

                            } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 6 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 7) {
                                // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 3);
                                // accelerationIndex = accelerationIndex + accIncident*3;
                                accelerationIndex = accelerationIndex + 3;

                            } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 8) {
                                // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 4);
                                // accelerationIndex = accelerationIndex + accIncident*4;
                                accelerationIndex = accelerationIndex + 4;

                            } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 8) {
                                // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 5);
                                // accelerationIndex = accelerationIndex + accIncident*5;
                                accelerationIndex = accelerationIndex + 5;

                            }
                        } else if (gpsSpeedData[i + 1] < gpsSpeedData[i]) {
                            breakIncident++;
                            if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 5 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 7) {
                                // breakingIndex = breakingIndex + (gpsSpeedData[i] - gpsSpeedData[i + 1]);
                                // breakingIndex = breakingIndex + breakIncident*1;  
                                breakingIndex = breakingIndex + 1;

                            } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 9) {
                                // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 2);
                                // breakingIndex = breakingIndex + breakIncident*2;
                                breakingIndex = breakingIndex + 2;

                            } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 9 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 11) {
                                // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 3);
                                // breakingIndex = breakingIndex + breakIncident*3;
                                breakingIndex = breakingIndex + 3;

                            } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 11 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 13) {
                                // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 4);
                                // breakingIndex = breakingIndex + breakIncident*4;
                                breakingIndex = breakingIndex + 4;

                            } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 13) {
                                // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 5);
                                // breakingIndex = breakingIndex + breakIncident*5;
                                breakingIndex = breakingIndex + 5;

                            }
                        }
                    }
                    if (delta) {
                        accelerationIndex = accelerationIndex / delta;
                        breakingIndex = breakingIndex / delta;

                        Logger.info(`ACCELEARION INDEX: ${accelerationIndex}`);
                        Logger.info(`Breaking Index: ${breakingIndex}`);

                        let averageIndex = (accelerationIndex + breakingIndex) / 2;
                        let driverScore = (100 - averageIndex).toFixed(2);
                        Logger.info(`Driver Score Individual ${driverScore}`);

                        if (driverScore != 'NaN' && driverScore != '-Infinity' && driverScore != 'Infinity') {
                            tripsScore.push(driverScore)
                        }
                    }
                    Logger.info(`TripScore Length: ${tripsScore.length}`);
                }
            };
        Logger.info(`Outside ForEach of Data ${tripsScore}`);        

        let average = 0;
        
        /// map reduce
        tripsScore.forEach(item => {
            if(item != 'NaN'){            
            let items = parseFloat(item);                        
            average = average + items;
            }
        });
        Logger.info(`Average ${average}`);

        let averageScore = average / tripsScore.length;
        let dataScore = {
            driverScore: averageScore,
            user: userDetails
        }
        Logger.info(`Average Score: ${averageScore}`);

        if (averageScore) {
            response.send(dataScore);
        } else {
            response.send({ data: 'No Score Available' });
        }

        
    } catch(e) {
        Logger.error(`Error fetching driver score( vin ${vin}): ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const driverScoreTripController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running driver score router for score for individual trip")
    let tripId = request.query.tripid;
    let vin = request.query.vin;
    let startTime = request.query.starttime;
    let endTime = request.query.endtime;
    let userDetails = {
        firstName: '',
        lastName: '',
        email: '',
        phone: ''
    }
    try {
        const lease: any = async () => {
            // console.log('Inside Trips');
            let filter: any = {
                asset: {
                    vehicle: {
                        vin:vin
                    }
                },                               
            };
            // console.log('Here 1');
            const companyVin = `
                    fragment userOnLease on Leases{
                        id
                        startTime
                        endTime
                        leasee{
                            user{
                              firstName
                              email
                              lastName
                              phone
                            }
                          }                    
                    }`

            // console.log('Here 2');
            const userEmail: any = await prisma.leases({ where: filter}).$fragment(companyVin);
            // console.log('Data Received', userEmail);                   
            return userEmail;
        };
        
        let tripDataJson: any = await getTripsDataTripIdV2(tripId);

        let userData = await lease();
        // console.log('User Data', userData[0]);
        userData.forEach(item => {            
            if(moment(startTime).isBetween(item.startTime, item.endTime, undefined, '[]')){
                console.log('Yeh I found the User:)');
                let firstName = item.leasee.user.firstName;
                let email = item.leasee.user.email;
                userDetails = {
                    firstName: item.leasee.user.firstName,
                    lastName: item.leasee.user.lastName,
                    email: item.leasee.user.email,
                    phone: item.leasee.user.phone
                };
                console.log('User Details', userDetails);
            }
        });
        
        // let tripDataJson = JSON.parse(tripDataString);
        // console.log(tripDataJson.location);
        //Null Check
        if (tripDataJson.location.length > 0) {
            // console.log('Inside If');
            let gpsSpeedData = []
            let startTime = tripDataJson.location[0].timestamp;
            let endTime = tripDataJson.location[tripDataJson.location.length - 1].timestamp;
            let accelerationIndex = 0;
            let breakingIndex = 0;
            let deltaMili = moment(startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"));
            let delta = moment.utc(deltaMili).format("mm");
            console.log('Time Diff in hours', delta);
            tripDataJson.location.forEach(item => {
                gpsSpeedData.push(item.gpsSpeed);
            });
            console.log(gpsSpeedData);
            let accIncident = 0;
            let breakIncident = 0;

            for (let i = 0; i <= gpsSpeedData.length - 1; i++) {
                if (gpsSpeedData[i + 1] > gpsSpeedData[i]) {
                    accIncident++;
                    if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 4 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 4) {
                        // accelerationIndex = accelerationIndex + (gpsSpeedData[i + 1] - gpsSpeedData[i]);
                        // accelerationIndex = accelerationIndex + accIncident*1;
                        accelerationIndex = accelerationIndex + 1;
                    } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 5 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 6) {
                        // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 2);
                        // accelerationIndex = accelerationIndex + accIncident*2;
                        accelerationIndex = accelerationIndex + 2;

                    } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 6 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 7) {
                        // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 3);
                        // accelerationIndex = accelerationIndex + accIncident*3;
                        accelerationIndex = accelerationIndex + 3;

                    } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 8) {
                        // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 4);
                        // accelerationIndex = accelerationIndex + accIncident*4;
                        accelerationIndex = accelerationIndex + 4;

                    } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 8) {
                        // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 5);
                        // accelerationIndex = accelerationIndex + accIncident*5;
                        accelerationIndex = accelerationIndex + 5;

                    }
                } else if (gpsSpeedData[i + 1] < gpsSpeedData[i]) {
                    breakIncident++;
                    if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 5 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 7) {
                        // breakingIndex = breakingIndex + (gpsSpeedData[i] - gpsSpeedData[i + 1]);
                        // breakingIndex = breakingIndex + breakIncident*1;  
                        breakingIndex = breakingIndex + 1;

                    } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 9) {
                        // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 2);
                        // breakingIndex = breakingIndex + breakIncident*2;
                        breakingIndex = breakingIndex + 2;

                    } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 9 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 11) {
                        // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 3);
                        // breakingIndex = breakingIndex + breakIncident*3;
                        breakingIndex = breakingIndex + 3;

                    } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 11 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 13) {
                        // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 4);
                        // breakingIndex = breakingIndex + breakIncident*4;
                        breakingIndex = breakingIndex + 4;

                    } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 13) {
                        // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 5);
                        // breakingIndex = breakingIndex + breakIncident*5;
                        breakingIndex = breakingIndex + 5;

                    }
                }
            }

            if (delta > 0) {
                accelerationIndex = accelerationIndex / delta;
                breakingIndex = breakingIndex / delta;

                console.log('ACCELEARION INDEX', accelerationIndex)
                console.log('Breaking Index', breakingIndex);

                let averageIndex = (accelerationIndex + breakingIndex) / 2;
                console.log('Average Index', averageIndex);
                let driverScore = (100 - averageIndex).toFixed(2);
                console.log('Driver Score', driverScore);
                let dataScore = {
                    driverScore: driverScore,
                    userDetails: userDetails
                }
                console.log('User Details', dataScore);
                response.send(dataScore);
            } else {
                response.send({ data: 'No Score Available' });
            }

        } else {
            response.send({ status: 500, data: 'No Score Available' });
        }
    } catch(e) {
        Logger.error(`Error fetching driver score for trip : ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};