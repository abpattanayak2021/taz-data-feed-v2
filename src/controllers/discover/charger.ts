/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Response Object for standard api responses
 *
**/

import { Request, Response, Next } from 'express';
import { prisma } from '../../../Shared/DBManager/Prisma/prisma-client';
import * as _ from 'lodash';
import * as geohash from 'ngeohash';

import Logger from '../../utilities/logger';
import { Log } from '../../utilities/log';
import { OkResponse, BadResponse, CreatedResponse } from '../../utilities/response';

export const chargerController = async (request: Request, response: Response, next: Next) => {
    let message = "Charger Controller for discovery service called.";
    let logObj: Log = new Log("http", request, chargerController.name, message);
    Logger.log(logObj);

    let filter = {
        UID_in: ["BOLT_TEST38", "BOLT_TEST10", "BOLT_B00005", 
        "BOLT_000498", "BOLT_TEST34", "BOLT_TEST22", "BOLT_002501", 
        "BOLT_000001", "BOLT_000467", "BOLT_B00003"]
    };

    try {
        const getChargerData: any = async() => {
            /* Prisma query for charger data */
            const chargerDataQuery = `
                fragment component on Components {
                    id
                    UID
                    specs{
                        key
                        name
                        value
                    }
                }
            `
            let chargerData: any = await prisma.components({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(chargerDataQuery);
            return chargerData;
        }
        let data = [];
        data = await getChargerData();
        let chargerData = data.map((item) => {
            let lat = item.specs.find(item => item.key === "latitude").value;
            let lng =  item.specs.find(item => item.key === "longitude").value
            return {
                id: item.id,
                uid: item.UID,
                geohash: geohash.encode(lat, lng, 10),
                lat,
                lng,
            }
        });
        return response.send(new OkResponse(chargerData));
    } catch (err) {
        let errLogObj = new Log("error", request, chargerController.name, err.stack);
        Logger.log(errLogObj);
        return response.send(new BadResponse(err.message));
    }
}

export const dummyDiscoveryController = async (request: Request, response: Response, next: Next) => {
    let message = "Dummy Discovery Controller.";
    let logObj: Log = new Log("http", request,"dummyDiscoveryController", message);
    Logger.log(logObj);

    
}