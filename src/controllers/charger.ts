import { Request, Response, Next } from 'express';
import { prisma } from '../../Shared/DBManager/Prisma/prisma-client';
import { db } from '../../Shared/CacheManager/cacheManager';
import * as _ from 'lodash';
import Logger from '../utilities/logger';

export const chargerAnalyticsController = async (request: Request, response: Response, next: Next) => {
    let reqCId = request.query.id;
    let filter = {};
    let master = request.query.master;
    Logger.info("Running charger router for charger analytics");
    if(reqCId === "5cfa443da7b11b00073f9657" && master === 'true'){
        filter = {
            asset: {
                type: "COMPONENT"
            }
        }
    }else{                
        filter = {
            asset:{
                type: "COMPONENT",
                component:{
                owner:{
                    id: reqCId
                }
              }
            }
          }
    }
    
    try {
        const trips: any = async () => {
            const leaseData = `
                fragment lease on Leases{
                    bookingTime
                    leasee {
                        id
                    }
                    contract {
                        txInfo {
                            invoice {
                                passbook {
                                    amount
                                }
                            }
                        }
                    }                  
                }`
            let lease: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let tripData = [];
            let outputData = null;
            outputData = await db.get(request.url);
            Logger.info(`Length of data from cache: ${outputData ? outputData.length : null}`);
            if (!outputData){
                tripData = await trips();
                Logger.info(`Length of data from prisma: ${tripData ? tripData.length : null}`);
                let dataTemp = tripData.filter(item => {
                    if (item.bookingTime)
                        return true
                    return false
                })
                let chargerData = dataTemp.map(item => {
                    return {
                        date: item.bookingTime.split("T")[0],
                        contract: item.contract,
                        leasee: item.leasee
                    }
                })
                let dateMap = new Map();
                chargerData.map(item => {
                    let tempObj = dateMap.get(item.date);
                    if (tempObj) {
                        tempObj.count++;
                        if (!tempObj.userId.has(item.leasee.id)) {
                            tempObj.userId.add(item.leasee.id);
                            tempObj.freeUsers++;
                        }
                        let amount = 0;
                        if (item.contract && item.contract.txInfo && item.contract.txInfo.invoice && item.contract.txInfo.invoice.passbook) {
                            item.contract.txInfo.invoice.passbook.map(items => {
                                if (items.amount == 0) {
                                    tempObj.freeUserDay += 1;
                                } else {
                                    amount += items.amount ? items.amount : 0;
                                }
                            })
                        }
                        tempObj.amount += amount;
                        dateMap.set(item.date, tempObj)
                    } else {
                        let dateAmountObj = {
                            date: '',
                            amount: 0,
                            count: 0,
                            userId: new Set(),
                            freeUsers: 0,
                            freeUserDay: 0
                        };
                        dateAmountObj.date = item.date;
                        dateAmountObj.count = 1;
                        dateAmountObj.userId.add(item.leasee.id);
                        dateAmountObj.freeUsers = 1;
                        let amount = 0;
                        if (item.contract && item.contract.txInfo && item.contract.txInfo.invoice && item.contract.txInfo.invoice.passbook) {
                            item.contract.txInfo.invoice.passbook.forEach(items => {
                                if (items.amount == 0) {
                                    dateAmountObj.freeUserDay += 1;
                                } else {
                                    amount += items.amount ? items.amount : 0;
                                }
                            })
                        };
                        dateAmountObj.amount = amount;
                        dateMap.set(item.date, dateAmountObj)
                    }
                    // console.log(item.date, dateMap.get(item.date).userId)
                });
                // console.log(dateMap)
                outputData = []
                for (const dateObject of dateMap.values()){
                    outputData.push({
                        date: dateObject.date,
                        amount: dateObject.amount,
                        count: dateObject.count,
                        userId: [...dateObject.userId],
                        freeUsers: dateObject.freeUsers,
                        freeUserDay: dateObject.freeUserDay,
                    })
                }
                db.set(request.url, outputData);
            }
            response.send({status: 200, data: outputData});
        } catch (err) {
            Logger.error(`Error in charger controller for analytics: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in charger controller for analytics: ${err.stack}`);
        response.send({ status: 401, data: err.message });
    }
};

export const allChargerAnalyticsController = async (request: Request, response: Response, next: Next) => {
    let reqCId = request.query.id;
    let filter = {};
    let nullFilter = {};
    let master = request.query.master;   
    Logger.info("Running charger router for all charger analytics")
    if(reqCId === "5cfa443da7b11b00073f9657" && master === "true") {
        Logger.info('======Inside Master======');
        filter = {            
            prototype: {
                type: "CHARGING_SOCKET"
            }
        };
    } else {
        if(reqCId === "5cfa443da7b11b00073f9657" && master === "false") {
            Logger.info('======Inside Master REVOS======');
            filter = {
                owner: {
                    id: reqCId
                },                
                prototype: {
                    type: "CHARGING_SOCKET"
                }
            };
        } else {
            Logger.info('======Inside Master Other======');
            filter = {
                owner: {
                    id: reqCId
                },
                prototype: {
                    type: "CHARGING_SOCKET"
                }
            };
        }
        
    }    

    nullFilter = {
        owner: null,
        prototype: {
            type: "CHARGING_SOCKET"
        }
    };
    
    try {
        const products: any = async () => {
            const chargerData = `
                    fragment component on Components{
                        id
                        UID
                        status
                        prototype{
                            id
                            name
                        }
                        txInfo{
                            owner{
                              company{
                                id
                                name
                              }
                            }
                        }
                        owner{
                            name
                            id
                        }
                        specs{
                            id
                            key
                            value
                            unit
                            name
                            required
                            fixed
                            createdAt
                            updatedAt
                        }               
                    }`
            if(reqCId === "5cfa443da7b11b00073f9657" && master === "false"){

                let lease: any = await prisma.components({
                    where: filter,
                    orderBy: 'updatedAt_DESC',
                }).$fragment(chargerData);

                let leaseNull: any = await prisma.components({
                    where: nullFilter,
                    orderBy: 'updatedAt_DESC',
                }).$fragment(chargerData);

                const components = _.concat(lease, leaseNull);
                return components;                
            } else {
                let lease: any = await prisma.components({
                    where: filter,
                    orderBy: 'updatedAt_DESC',
                }).$fragment(chargerData);

                return lease;
            }
            
        };
        try {
            let productData = [];
            productData = await db.get(request.url);
            Logger.info(`Length of data from cache: ${productData ? productData.length : productData}`);
            if (!productData){
                productData = await products();
                Logger.info(`Length of data from prisma: ${productData ? productData.length : productData}`);
                db.set(request.url, productData);
            }
            response.send({status: 200, data: productData});
        } catch (err) {
            Logger.error(`Error in charger controller for analytics: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in charger controller for analytics: ${err.stack}`);
        response.send({ status: 401, data: err.message });
    }
};

export const allChargerLeasesController = async (request: Request, response: Response, next: Next) => {
    let reqCId = request.query.id;
    let filter = {};
    let master = request.query.master;
    Logger.info("Running charger router for all charger leases data")
    if(reqCId === "5cfa443da7b11b00073f9657" && master === "true") {
        Logger.info('======Inside Master======');
        filter = {
            asset:{
              type: "COMPONENT",          
            }
        }
    } else {
        filter = {
            asset:{
                type: "COMPONENT",
                component:{
                    owner:{
                        id: reqCId
                    }
                }
            }
        }
    }
    
    try {
        const lease: any = async () => {
            const leaseData = `
                fragment lease on Leases{
                    id
                    startTime
                    endTime
                    returnTime
                    leaseeConsumption
                    status
                    contract{
                        txInfo{
                            invoice{
                                passbook{
                                    amount
                                }
                            }
                        }
                    }
                    leasee{
                        user{
                            email
                            phone
                            firstName
                            lastName
                        }
                    }
                    asset{      
                        component{
                            id
                            UID
                            owner{
                                id
                                name
                            }
                            prototype{
                                id
                                name
                            }
                            specs{
                                id
                                key
                                value
                                unit
                                name
                                required
                                fixed
                                createdAt
                                updatedAt
                            }        
                        }
                    }                 
                }`
            let lease: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let leaseData = [];
            leaseData = await db.get(request.url);
            Logger.info(`Length of data from cache: ${leaseData ? leaseData.length : leaseData}`);
            if (!leaseData){
                leaseData = await lease();
                Logger.info(`Length of data from prisma: ${leaseData ? leaseData.length : leaseData}`);
                db.set(request.url, leaseData);
            }
            response.send({status: 200, data: leaseData});
        } catch (err) {
            Logger.error(`Error in charger controller for charger leases: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in charger controller for charger leases: ${err.stack}`);
        response.send({ status: 401, data: err.message });
    }
};

export const chargerConsumptionController = async (request: Request, response: Response, next: Next) => {
    let reqCId = request.query.id;
    let filter = {};
    let master = request.query.master; 
    Logger.info("Running charger router for charger consumption data")
    if(reqCId === "5cfa443da7b11b00073f9657" && master === "true") {
        filter = {            
            asset: {
                type: "COMPONENT"
            }
        };
    } else {
        filter = {
            asset: {
                type: "COMPONENT",
                component: {
                    owner: {
                        id: reqCId
                    }
                }
            }
        }
    }
    
    try {
        const consumption: any = async () => {
            const leaseData = `
                    fragment lease on Leases{
                        bookingTime
                        leaseeConsumption                  
                    }`
            let lease: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let consumptionData = [];
            let outputData = null;
            outputData = await db.get(request.url);
            Logger.info(`Length of data from cache: ${outputData ? outputData.length : outputData}`);
            if (!outputData){
                consumptionData = await consumption();
                Logger.info(`Length of raw data from prisma: ${consumptionData ? consumptionData.length : consumptionData}`);
                let dataTemp = consumptionData.map(item => {
                    return {
                        date: item.bookingTime.split("T")[0],
                        consumption: item.leaseeConsumption
                    };
                });
                let dateAmountMap = new Map();
                dataTemp.map(item => {
                    let tempObj = dateAmountMap.get(item.date);
                    if (tempObj) {
                        tempObj.count++;
                        tempObj.consumption += item.consumption;
                        dateAmountMap.set(item.date, tempObj);
                    } else {
                        let dateAmountObj = {
                            date: item.date,
                            consumption: item.consumption,
                            count: 1,
                        };
                        dateAmountMap.set(item.date, dateAmountObj);
                    }
                });
                outputData = [];
                for (const dateObject of dateAmountMap.values()){
                    outputData.push({
                        date: dateObject.date,
                        consumption: dateObject.consumption,
                        count: dateObject.count
                    });
                }
                db.set(request.url, outputData);
            }
            response.send({ status: 200, data: outputData });
        } catch (err) {
            Logger.error(`Error in charger controller for consumption: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in charger controller for consumption: ${err.stack}`);
        response.send({ status: 401, data: err.message });
    }
};

export const chargerIndividualDetailsController = async (request: Request, response: Response, next: Next) => {
    let uid = request.query.uid;
    let filter = {
        asset: {
            component: {
                UID: uid
            }
        }
    };
    Logger.info("Running charger router for charger individual details")
    
    try {
        //Call Prisma To Get All Details Of The Charger
        //Include Lease Details
        const lease: any = async () => {
            const leaseData = `
                fragment lease on Leases{
                    id
                    startTime
                    endTime
                    returnTime
                    bookingTime
                    leaseeConsumption
                    status
                    contract{
                        txInfo{
                            invoice{
                                passbook{
                                    amount
                                }
                            }
                        }
                    }
                    leasee{
                        id
                        user{
                            email
                            phone
                            firstName
                            lastName
                        }
                    }
                    asset{      
                        component{
                            id
                            UID
                            owner{
                                id
                                name
                            }
                            prototype{
                                id
                                name
                            }
                            specs{
                                id
                                key
                                value
                                unit
                                name
                                required
                                fixed
                                createdAt
                                updatedAt
                            }
                        }
                    }
                }`
            let lease: any = await prisma.leases({
            where: filter,
            orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let data = [];
            let chargerInfo = null;
            chargerInfo = await db.get(request.url);
            Logger.info(`Length of data from cache: ${chargerInfo.info 
                ? chargerInfo.info.length : null}`);
            if (!chargerInfo){
                data = await lease();
                Logger.info(`Length of data raw from prisma: ${data ? data.length : null}`);
                let dataTemp = data.map(item => {
                    return {
                        date: item.bookingTime.split("T")[0],
                        consumption: item.leaseeConsumption,
                        amount: item.contract,
                        count: 0,
                        leasee: item.leasee,
                    };
                });
                let dateAmountMap = new Map();
                dataTemp.map(item => {
                    let tempObj = dateAmountMap.get(item.date);
                    if (tempObj) {
                        tempObj.count++;
                        tempObj.consumption += item.consumption;
                        let amount = 0;
                        if (item.amount && item.amount.txInfo && item.amount.txInfo.invoice && 
                            item.amount.txInfo.invoice.passbook) {
                                amount = item.amount.txInfo.invoice.passbook.reduce(
                                    (total, curVal) => total + curVal.amount, 0);
                        };
                        if (item.leasee.id && !tempObj.userId.has(item.leasee.id)) {
                            tempObj.userId.add(item.leasee.id);
                            tempObj.userCount++;
                        }
                        tempObj.amount += amount;
                        dateAmountMap.set(item.date, tempObj);
                    } else {
                        let dateAmountObj = {
                            date: item.date,
                            consumption: item.consumption ? item.consumption: 0,
                            amount: 0,
                            userId: new Set(),
                            count: 1,
                            userCount: 0
                        };
                        if (item.leasee.id) {
                            dateAmountObj.userCount = 1;
                            dateAmountObj.userId.add(item.leasee.id);
                        }
                        let amount = 0;
                        if (item.amount && item.amount.txInfo && item.amount.txInfo.invoice && 
                            item.amount.txInfo.invoice.passbook) {
                                amount = item.amount.txInfo.invoice.passbook.reduce(
                                    (total, curVal) => total + curVal.amount, 0);
                        };
                        dateAmountObj.amount = amount;
                        dateAmountMap.set(item.date, dateAmountObj);
                    }
                });
                let processedDateAmount = []
                for (const dateObject of dateAmountMap.values()){
                    processedDateAmount.push({
                        date: dateObject.date,
                        consumption: dateObject.consumption,
                        amount: dateObject.amount,
                        userId: [...dateObject.userId],
                        count: dateObject.count,
                        userCount: dateObject.userCount,
                    });
                }
                chargerInfo = {
                    leases: data,
                    info: processedDateAmount
                };
                db.set(request.url, chargerInfo);
            }
            response.send({ status: 200, data: chargerInfo });
        } catch (err) {
            Logger.error(`Error in charger controller for individual charger details: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in charger controller for individual charger details: ${err.stack}`);
        response.send({ status: 401, data: err.message });
    }
};