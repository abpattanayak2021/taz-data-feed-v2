import { v4 as uuidV4 } from 'uuid';

import { Request, Response, Next } from 'express';
import { prisma } from '../../Shared/DBManager/Prisma/prisma-client';
import { elastic } from '../../Shared/DBManager/Elastic/elastic';
import { db } from '../../Shared/CacheManager/cacheManager';
import * as _ from 'lodash';
import Logger from '../utilities/logger';

import Trips from '../models/tripDocument';
import Tripsv1 from '../models/tripDocumentV1';
import TripDistance from '../models/tripDistance';

import TripDashboardDaily from '../models/tripDashboardDaily';

import { TripWrapper } from '../utilities/elastic_wrappers/trip';
const moment = require('moment');

const VEHICLE_LEVEL_2_INDEX = 'vehicle002';

const getDaysArray = (start, end) => {
    let arr = [];
    for (let dt = new Date(start); dt <= new Date(end); dt.setDate(dt.getDate() + 1)) {
        arr.push(moment(new Date(dt)).format('YYYY-MM-DD'));
    }
    return arr;
};

const searchFromElastic = async ({ query, index }) => {
    return elastic.search({
        index,
        query,
    });
};

const tripV2Logs = async (args) => {
    let {        
        size,
        vin,
        vins,
        skip,
        tripIds,
    } = args;
    if (!size)
        size = 10;
    try {
        const trips: any = async () => {
            const query: any = {
                bool: {
                    must: [
                        {
                            match_all: {},
                        },
                        {
                            bool: {
                                should: [{
                                    match_phrase: {
                                        "type": 'L'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'B'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'U'
                                    }
                                }],
                                minimum_should_match: 1
                            }
                            
                        }
                    ],
                    
                    must_not: [
                        {
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        }, 
                        {
                            match_phrase: {
                                "latitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "longitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "batteryVoltageAdc": 0
                            }
                        }
                    ],
                },
            };
            tripIds.length && tripIds.map(trip => {
                query.bool.must.push({bool: {
                    should: [],
                    minimum_should_match: 1
                }})
                query.bool.must[2].bool.should.push({
                    match_phrase: {
                        "tripId": trip,
                    },
                })
            });
            if (vin) {
                query.bool.must.push({
                    match_phrase: {
                        "vin": {
                            query: vin
                        }
                    }
                })
            }
            if (vins && vins.length) {
                let should_vins = vins.map(v => {
                    return {
                        match_phrase: {
                            "vin": v
                        }
                    }
                });
                query.bool.must.push({
                    bool: {
                        should: should_vins,
                        minimum_should_match: 1
                    }
                })
            }
            let data = await searchFromElastic({
                index: VEHICLE_LEVEL_2_INDEX,
                query: {
                    _source: ["tripId", "vin", "batteryVoltageAdc", "batteryVoltage", "latitude", "longitude", "timestamp", "type", "batteryVoltage"],
                    query,
                    size: "all",
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                            },
                        },
                    ],
                },
            });
            let body = data.searchData;
            let trips = tripIds.map((tripId) => {
                const tripLogs = body.filter(log => {
                    return log._source.tripId.toString() === tripId.toString();
                });
                let trip: any = {
                    tripId: tripId.toString(), 
                    vin: tripLogs.length && tripLogs[0]._source.vin
                };
                let batteryVoltageAdc = [];
                let locations = tripLogs.length && tripLogs.map(log => {
                    log = log._source
                    if (log.type === 'L') {
                        return {
                            timestamp: log.timestamp,
                            lat: log.latitude,
                            lng: log.longitude
                        }
                    }
                    if (log.type === 'B') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                    }
                    if (log.type = 'U') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltage || log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                    }
                }) || []
                locations = _.compact(locations);
                trip.location = locations;
                trip.batteryVoltageAdc = batteryVoltageAdc;
                return trip;
            });
            return { trips };            
        };
        try {
            let data = {};
            data = await trips();
            return data;
        } catch (err) {
            Logger.error(`Error in elastic query for trips: ${err.stack}`);
        }
    } catch (err) {
        Logger.error(`Error in elastic query for trips: ${err.stack}`);
        return { trips: [] };
    }
}

const tripIdsV2 = async (args) => {
    let { size, vin, skip, vins, startTime, endTime,
        _source, ranges, lastTripId } = args
    try {
        const query: any = {
            bool: {
                must: [
                    {
                        match_all: {},
                    },
                    {
                        exists: {
                            field: "tripId",
                        }
                    }
                ],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }, 
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };
        if (vin) {
            query.bool.must.push({
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            })
        }
        if (vins && vins.length) {
            let shouldMatchVins = vins.map(v => {
                return {
                    match_phrase: {
                        "vin": v
                    }
                }
            });
            query.bool.must.push({
                bool: {
                    should: shouldMatchVins,
                    minimum_should_match: 1
                }
            })
        }
        if (startTime && endTime) {
            query.bool.must.push({
                "range": {
                  "timestamp": {
                    "gte": startTime,
                    "lt": endTime
                  }
                }
              },)
        }
        if (ranges) {
            query.bool['filter'] = [
                {
                    bool: {
                        should: [...ranges]
                    }
                }
            ]
        }
        if (lastTripId) {
            query.bool.must.push({
                "range": {
                  "tripId": {
                    "lt": lastTripId,
                  }
                }
            })
        }        

        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: _source || ["tripId", "vin"],
                query,
                collapse: {
                    field: "tripId"
                },
                size: size,
                from: skip || 0,
                aggs: {
                    uniq_tripId: {
                        cardinality: {
                          field: "tripId",
                        },
                    },
                },
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        let tripIds = body.length && body.map(b => b._source.tripId.toString()) || [];
        let tripCount = data.aggs && data.aggs.uniq_tripId && data.aggs.uniq_tripId.value || 0;
        return { tripIds, tripCount }
    } catch (e) {
        Logger.error(`Error fetching trip Ids: ${e.stack}`);
        return {tripIds: [], tripCount: 0}
    }
}

const tripV2 = async (args) => {
    let {        
        size,
        vin,
        vins,
        skip,
        tripIds,
    } = args;
    if (!size) {
        size = 10;
    }
    try {
        const trips: any = async () => {
            const query: any = {
                bool: {
                    must: [
                        {
                            match_all: {},
                        },
                        {
                            bool: {
                                should: [{
                                    match_phrase: {
                                        "type": 'L'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'B'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'U'
                                    }
                                }],
                                minimum_should_match: 1
                            }
                            
                        }
                    ],
                    
                    must_not: [
                        {
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        }, 
                        {
                            match_phrase: {
                                "latitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "longitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "batteryVoltageAdc": 0
                            }
                        }
                    ],
                },
            };
            tripIds.length && tripIds.map(trip => {
                query.bool.must.push({bool: {
                    should: [],
                    minimum_should_match: 1
                }})
                query.bool.must[2].bool.should.push({
                    match_phrase: {
                        "tripId": trip,
                    },
                })
            });
            if (vin) {
                query.bool.must.push({
                    match_phrase: {
                        "vin": {
                            query: vin
                        }
                    }
                })
            }
            if (vins && vins.length) {
                let should_vins = vins.map(v => {
                    return {
                        match_phrase: {
                            "vin": v
                        }
                    }
                });
                query.bool.must.push({
                    bool: {
                        should: should_vins,
                        minimum_should_match: 1
                    }
                })
            }
            let data = await searchFromElastic({
                index: VEHICLE_LEVEL_2_INDEX,
                query: {
                    _source: ["tripId", "vin", "batteryVoltageAdc", "batteryVoltage", "latitude", "longitude", "timestamp", "type", "gpsSpeed", "wheelRpm", "mode", "batteryCurrent", "batteryVoltage", "odometer"],
                    query,
                    size: "all",
                    sort: [
                        {
                            timestamp: {
                            order: "desc",
                            },
                        },
                    ],
                },
            });
            let body = data.searchData;
            let trips = tripIds.map((tripId, i) => {
                const tripLogs = body.filter(log => {
                    return log._source.tripId.toString() === tripId.toString();
                });
                let trip: any = {
                    tripId: tripId.toString(), 
                    vin: tripLogs.length && tripLogs[0]._source.vin
                };
                let batteryVoltageAdc = [];
                let minGpsSpeed = 0;
                let maxGpsSpeed = 0;
                let avgGpsSpeed = 0;
                let gpsLogsCount = 0;
                let maxWheelRPM = 0;
                let minWheelRPM = 0;
                let energy = 0;
                let mode = {
                    RIDE: 0,
                    ECONOMY: 0,
                    SPORT: 0
                }
                let modeCount = 0;
                let locations = tripLogs.length && tripLogs.map(log => {
                    log = log._source
                    // trip.vin = log.vin;
                    if (log.type === 'L') {
                        if (log.gpsSpeed && log.gpsSpeed !== 0) {
                            if (minGpsSpeed === 0 || minGpsSpeed > log.gpsSpeed) {
                                minGpsSpeed = log.gpsSpeed
                            }
                            if (maxGpsSpeed === 0 || maxGpsSpeed < log.gpsSpeed) {
                                maxGpsSpeed = log.gpsSpeed
                            }
                            avgGpsSpeed += log.gpsSpeed;
                            gpsLogsCount++;
                        }
                        return {
                            timestamp: log.timestamp,
                            lat: log.latitude,
                            lng: log.longitude
                        }
                    }
                    if (log.type === 'B') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                    }
                    if (log.type === 'U') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltage || log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                        if (log.wheelRpm && log.wheelRpm !== 0) {
                            if (minWheelRPM === 0 || minWheelRPM > log.wheelRpm) {
                                minWheelRPM = log.wheelRpm
                            }
                            if (maxWheelRPM === 0 || maxWheelRPM < log.wheelRpm) {
                                maxWheelRPM = log.wheelRpm
                            }
                        }
                        if (log.batteryCurrent && log.batteryVoltage) {
                            const power = parseFloat(log.batteryCurrent) * parseFloat(log.batteryVoltage);
                            energy = energy + power;
                        }
                        if (log.mode === 0) {
                            mode.ECONOMY += 1;
                        }
                        if (log.mode === 1) {
                            mode.RIDE += 1;
                        }
                        if (log.mode === 2) {
                            mode.SPORT += 1;
                        }
                        modeCount++;
                    }
                }) || []
                locations = _.compact(locations);
                trip.location = locations;
                trip.batteryVoltageAdc = batteryVoltageAdc;
                trip.avgGpsSpeed = avgGpsSpeed/(gpsLogsCount || 1);
                trip.maxGpsSpeed = maxGpsSpeed;
                trip.minGpsSpeed = minGpsSpeed;
                trip.maxWheelRPM = maxWheelRPM;
                trip.minWheelRPM = minWheelRPM;
                trip.energy = energy / 3600000;
                trip.mode = {
                    ECONOMY: mode.ECONOMY * 100 / modeCount,
                    RIDE: mode.RIDE * 100 / modeCount,
                    SPORT: mode.SPORT * 100 / modeCount
                }
                let distance = 0;
                let tripLogEndIndex = tripLogs.length && _.findIndex(tripLogs, l => l._source.type === 'U');
                let tripLogStartIndex = tripLogs.length && _.findLastIndex(tripLogs, l => l._source.type === 'U');
                if (tripLogStartIndex > -1 && tripLogEndIndex > -1) {
                    let endOdo = tripLogs[tripLogEndIndex]._source.odometer;
                    let startOdo = tripLogs[tripLogStartIndex]._source.odometer;
                    distance = endOdo - startOdo;
                }
                if (distance === 0) {
                    const polyline = locations;
                    for (let i = 0; i < polyline.length; i++) {
                        if ( polyline[i].lat === 0 && polyline[i].lng === 0) {
                            polyline.splice(i, 1);
                            i--;
                        }
                    }
                    if (polyline.length > 2) {
                        for (let i = 0; i < polyline.length - 1; i++) {
                            const rad = (x) => {
                                return x * Math.PI / 180;
                            };
                            const R = 6378137; // Earth’s mean radius in meter
                            const dLat = rad(polyline[i + 1].lat - polyline[i].lat);
                            const dLong = rad(polyline[i + 1].lng - polyline[i].lng);
                            const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(rad(polyline[i].lat)) * Math.cos(rad(polyline[i + 1].lat)) *
                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            const d = R * c;
                            // console.log('Distance in meters', d);
                            // returns the distance in meter
                            if(d < 1000){
                                distance = distance + d;  
                            }
                        }
                    }
                }
                trip.distance = distance;
                trip.startTime = new Date(parseInt(tripId))
                if (tripLogs.length) {
                    trip.endTime = typeof tripLogs[0]._source.timestamp === 'number' ? new Date(tripLogs[0]._source.timestamp) : tripLogs[0]._source.timestamp;
                    trip.startTime = typeof tripLogs[tripLogs.length - 1]._source.timestamp === 'number' ? new Date(tripLogs[tripLogs.length - 1]._source.timestamp) : tripLogs[tripLogs.length - 1]._source.timestamp;
                } else {
                    trip.endTime = trip.startTime;
                }
                return (trip);
            });
            return {trips};
            
        };
        try {
            let data = {};
            data = await trips();
            return data;
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch (err) {
        Logger.error(err.stack);
        return {trips: []};
    }
}

//TripIds from Elastic Based ON range
const tripIdsV2Range = async (args) => {
    let {       
        vin,        
        startTime,
        endTime,
        skip       
    } = args;
    try {
        const query: any = {
            bool: {
                must: [
                    {
                        match_all: {},
                    },
                    {
                        match_phrase: {
                            "vin": {
                                query: vin,
                            },
                        },
                    },
                    {
                        exists: {
                          field: "tripId",
                        },
                    },
                    {
                        range: {
                            "timestamp": {
                                "gte": startTime,
                                "lte": endTime
                            }
                        }
                    }
                ],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }, 
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };        
        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: ["tripId", "vin"],
                query,
                collapse: {
                    field: "tripId"
                },
                size: 10000,
                from:  skip,
                aggs: {
                    uniq_tripId: {
                        cardinality: {
                          field: "tripId",
                        },
                    },
                },
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        // console.log('tripIdsV2 body VIN: ', JSON.stringify(data));
        let tripId = body.length && body.map(b => b._source.tripId.toString()) || [];
        let tripCount = data.aggs && data.aggs.uniq_tripId && data.aggs.uniq_tripId.value || 0;
        return { tripId, tripCount }
    } catch (e) {
        Logger.error(`Error fetching trip Ids: ${e.stack}`);
        return {tripIds: [], tripCount: 0};
    }
}

const calculateTotalDistance = async (rules, startTime, endTime) => {
    let vehicles = await prisma.vehicles({
        where: rules
    });
    const vins = vehicles.map(v => v.vin);
    const metricsFragment = `
            fragment vinOfVehicles on Vehicles{
               vin
               metrics {
                   odometer
               }
            }`
    let vehicleMetrics: [any] = await prisma.vehicles({
        where: {
            vin_in: vins
        }
    }).$fragment(metricsFragment);
    // console.log('tripsIds', tripIds.length);
    let { tripCount } = await tripIdsV2({ size: 1, vins });
    let totalDistance = 0;
    vehicleMetrics.length && vehicleMetrics.forEach(v => {
        if (v.metrics && v.metrics.odometer) {
            totalDistance += v.metrics.odometer;
        }
    });
    return { vins, totalDistance, tripCount }
}

const calculateDateDistances = async (tripIds, datesInBetween) => {
    let data = {};
    if (!tripIds.length) {
        return [];
    }
    let doc = await Trips.find({ tripId: { '$in': tripIds } }, { tripId: 1, 'tripData.distance': 1 }, null);
    let existingTripsIds = doc.length && doc.map((d: any) => d.tripId) || [];
    let newTripIds = tripIds.filter(t => !existingTripsIds.includes(t));
    let newTrips = [];
    for (let i = 0; i < newTripIds.length; i = i + 1000) {
        const start = new Date().getTime();
        let { trips }: any = await tripV2({ tripIds: newTripIds.slice(i, i + 1000) });
        const end = new Date().getTime();
        Logger.info(`tripsV2: ${ (end-start)/1000 }`);
        let shortedData = trips.map(trip => {
            let tripData = _.cloneDeep(trip);
            delete tripData.location;
            delete tripData.batteryVoltageAdc;
            return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
        });
        await Trips.insertMany(shortedData).then((result: any) => {
            Logger.info(`Saving the trips in DB: ${result.length}`);
            // return res.send({ trips, tripCount });
            newTrips = [...newTrips, ...trips];
        }).catch(err => {
            Logger.info(err.stack);
            // return res.send({ status: 400, data: 'INTERNAL SERVER ERROR' });
        });

    }
    const start = new Date().getTime();
    tripIds.map(tripId => {
        let date = moment(parseInt(tripId)).format('YYYY-MM-DD');
        if (!data[date]) {
            data[date] = {
                date: new Date(date),
                distance: 0,
                count: 0
            }
        }
        let existingTrip: any = doc.find((d: any) => d.tripId === tripId)
        if (existingTrip) {
            data[date].distance = data[date].distance + (existingTrip.tripData.distance || 0);
            data[date].count += 1
        } else {
            let newTrip = newTrips.find(trip => trip.tripId === tripId);
            data[date].distance = data[date].distance + (newTrip && newTrip.distance || 0);
            data[date].count += 1
        }
    });
    const end = new Date().getTime();
    Logger.info(`tripDistances: ${ (end-start)/1000 }`);
    return Object.values(data);
}

/*
    Exported Controllers
*/

export const tripsV2Controller = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trips data (mongo + elastic)");
    let reqVin = request.query.vin;
    let first = parseInt(request.query.first);
    let skip = parseInt(request.query.skip);
    let status = request.query.status;
    try {
        if (status) {
            let vehicle = await prisma.vehicle({ vin: reqVin });
            // console.log('vehicle', vehicle, reqVin);
            if (!vehicle || vehicle && vehicle.status !== status) {
                return response.send({ status: 400, data: 'INVALID VIN' });
            }
        }
        
        let doc: any = await Trips.find({ 
            vin: reqVin, 'tripData.distance': {'$gte': 100}}, null, 
            { limit: first, skip, sort: { tripId: -1 }})
        let args: any = {
            vin: reqVin,
            size: first,
            skip: 0
        };
        let resp = [];
        if (doc.length) {
            const existingTripIds = doc.map(el => el.tripId);
            let existingTripsLogs: any = await tripV2Logs({ ...args, tripIds: existingTripIds });
            existingTripsLogs = existingTripsLogs.trips;
            doc = doc.map(el => {
                let existingTripLogs = existingTripsLogs.find(trip => trip.tripId === el.tripId);
                return {
                    ...el,
                    tripData: {
                        ...el.tripData,
                        location: existingTripLogs && existingTripLogs.location || [],
                        batteryVoltageAdc: existingTripLogs && existingTripLogs.batteryVoltageAdc || []
                    }
                }
            })
            doc = doc.map(d => d.tripData);
            resp.push(...doc);
        }
        
        let { tripCount } = await tripIdsV2(args);
        while (resp.length < first) {
            Logger.info(`resp ${resp.length}, ${args}`);
            const lastTripId = resp[resp.length - 1] && resp[resp.length - 1].tripId;
            if (lastTripId) {
                args = {
                    ...args,
                    size: first - resp.length,
                    lastTripId
                }
            } else {
                let lastTripIdDb: any = await Trips.findOne({ vin: reqVin, 'tripData.distance': {$gte: 100}}, {tripId: 1}, { sort: { tripId: 1 }});
                if (lastTripIdDb) {
                    args = {
                        ...args,
                        size: first - resp.length,
                        lastTripId: lastTripIdDb.tripId
                    }
                }
            }
            let { tripIds } = await tripIdsV2(args);
            if (!tripIds.length) {
                break;
            }
            Logger.info(`tripIds: ${tripIds}`);
            let tripsInDb: any = await Trips.find({ vin: reqVin, tripId: { '$in': tripIds }}, {tripId: 1}, { sort: { tripId: -1 }});
            tripsInDb = tripsInDb.map(trip => trip.tripId);
            tripIds = tripIds.filter(trip => !tripsInDb.includes(trip));
            Logger.info(`newtripids: ${tripIds}, ${tripsInDb}`);
            if (tripIds.length) {
                let { trips }: any = await tripV2({ ...args, tripIds });
                // console.log('trips', trips);
                let shortedData = trips.map(trip => {
                    let tripData = _.cloneDeep(trip);
                    delete tripData.location;
                    delete tripData.batteryVoltageAdc;
                    return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
                });
    
                await Trips.insertMany(shortedData);
                trips = trips.filter(trip => {
                    return trip.distance >= 100;
                })
                resp.push(...trips);
                if (!trips.length) {
                    args.skip = args.skip + tripIds.length + tripsInDb.length;
                } else {
                    args.skip = 0;
                }
            } else {
                args.skip = args.skip + tripsInDb.length;
            }
        }
        let filteredResults = resp.filter((trip) => {
            if (Date.parse(trip.startTime) <= Date.parse("2021-01-02")){
                Logger.info(`removed trip with start time ${trip.startTime} (less than 2021-01-02)`);
                return false;
            }
            return true;
        });
        return response.send({ status: 200, trips: filteredResults, tripCount });
    } catch(e) {
        Logger.error(`Error fetching trips data (v3) for ${reqVin}: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsControllerDashboard = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips controller for trips with distance greater than 100");
    let reqVin = request.query.vin;
    let first = parseInt(request.query.first);
    let skip = parseInt(request.query.skip);
    let status = request.query.status;
    console.log(request.url);
    try {
        if (status) {
            let vehicle = await prisma.vehicle({ vin: reqVin });
            // console.log('vehicle', vehicle, reqVin);
            if (!vehicle || vehicle && vehicle.status !== status) {
                return response.send({ status: 400, data: 'INVALID VIN' });
            }
        }
        let docsInDb: any = await TripDashboardDaily.find(
            { 
                vin: reqVin
            },
            null,
            { limit: first, skip, sort: { tripId: -1 }}
        );
        let args: any = {
            vin: reqVin,
            size: 10,
            skip: 0
        };
        let { tripCount } = await tripIdsV2(args);
        let tripIds = null;
        let requiredTrips = [];

        if (docsInDb.length) {
            docsInDb = docsInDb.map(doc => doc.tripData);
            requiredTrips.push(...docsInDb);
        }
        // console.log(requiredTrips);
        while (requiredTrips.length < first) {
            let lastTripId = tripIds && tripIds[tripIds.length - 1]
            let lastTripIdInDb = requiredTrips && requiredTrips[requiredTrips.length - 1]
            if (lastTripId) {
                args = {
                    ...args,
                    lastTripId
                }
            } else if (lastTripIdInDb) {
                args = {
                    ...args,
                    lastTripId
                }
            } 
            ({ tripIds } = await tripIdsV2(args));
            if (!tripIds.length) {
               Logger.info("Trips exhausted");
               break;
            }
            Logger.info(`tripIds: ${tripIds}`); 
            let { trips }: any = await tripV2({ ...args, tripIds });
            trips = trips.filter(trip => {
                return trip.distance >= 100 && Date.parse(trip.startTime) >= Date.parse("2021-01-02");
            });
            requiredTrips.push(...trips);
            if (requiredTrips.length >= first)
                requiredTrips = requiredTrips.slice(0, first);
            let formattedData = trips.map(trip => {
                let tripData = _.cloneDeep(trip);
                return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
            });
            await TripDashboardDaily.insertMany(formattedData);
            console.log(requiredTrips.length, formattedData.length);
        }        
        response.send({ status: 200, data: {
            tripCount,
            trips: requiredTrips
        }});
    } catch(e) {
        Logger.error(`Error fetching trips for dashboard (distance >= 100) for VIN: ${reqVin}: ${e.stack}`);
        response.send({status: 500, data: e.message});
    }
}

export const tripsV2RangeController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trips data within range (mongo + elastic)");
    let reqVin = request.query.vin;    
    let startTime = request.query.start;
    let endTime = request.query.end;        
    let status = request.query.status;
    try {
        if (status) {
            let vehicle = await prisma.vehicle({ vin: reqVin });
            if (!vehicle || vehicle && vehicle.status !== status) {
                return response.send({ status: 400, data: 'INVALID VIN' });
            }
        }

        let args = {
            vin: reqVin,            
            endTime: endTime,
            startTime: startTime,
            skip: 0
        };
        let {tripCount} = await tripIdsV2Range(args);
        let noOfIterations = Math.ceil(tripCount/10000);
        let tripIds = [];
        for(let i = 0; i<= noOfIterations -1; i++){
            args.skip = i*10000;
            args = {
                ...args
            };
            let { tripId } = await tripIdsV2Range(args);
            tripIds = [...tripIds, ...tripId];
        }
        Logger.info(`Trip Ids ${tripIds}`);

        Trips.find({ vin: reqVin, tripId: { '$in': tripIds } }, null, { sort: { tripId: -1 } }, async function (err, doc: any) {
            Logger.info('Finding Trips in DB');
            if (!doc.length) {
                let { trips }: any = await tripV2({ ...args, tripIds });
                Logger.info(`trips: ${trips}`);
                let shortedData = trips.map(trip => {
                    let tripData = _.cloneDeep(trip);
                    delete tripData.location;
                    delete tripData.batteryVoltageAdc;
                    return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
                });
                
                Trips.insertMany(shortedData).then(result => {
                    return response.send({ trips, tripCount });
                }).catch(err => {
                    Logger.error(err.stack);
                    return response.send({ status: 400, data: 'INTERNAL SERVER ERROR' });
                });
            } else {
                const existingTripIds = doc.map(el => el.tripId);
                const newTripIds = tripIds.filter(tripId => !existingTripIds.includes(tripId));
                let existingTripsLogs: any = await tripV2Logs({ ...args, tripIds: existingTripIds });
                existingTripsLogs = existingTripsLogs.trips;
                doc = doc.map(el => {
                    let existingTripLogs = existingTripsLogs.find(trip => trip.tripId === el.tripId);
                    return {
                        ...el,
                        tripData: {
                            ...el.tripData,
                            location: existingTripLogs && existingTripLogs.location || [],
                            batteryVoltageAdc: existingTripLogs && existingTripLogs.batteryVoltageAdc || []
                        }
                    }
                })
                if (newTripIds.length) {
                    let { trips }: any = await tripV2({ ...args, tripIds: newTripIds });

                    let shortedData = trips.map(trip => {
                        let tripData = _.cloneDeep(trip);
                        delete tripData.location;
                        delete tripData.batteryVoltageAdc;
                        return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
                    });
                    Trips.insertMany(shortedData).then(result => {
                        Logger.info('Saving trips in DB');
                        trips = [...trips, ...doc.map(el => el.tripData)];
                        trips = trips.sort((a, b) => {
                            return parseInt(b.tripId) - parseInt(a.tripId);
                        })
                        return response.send({ trips, tripCount });
                    }).catch(err => {
                        Logger.error(err.stack);
                        return response.send({ status: 400, data: 'INTERNAL SERVER ERROR' });
                    });
                } else {
                    return response.send({ 
                        status: 200,
                        trips: doc.map(el => el.tripData), 
                        tripCount 
                    });
                }
            }
        });
    } catch(e) {
        Logger.error(`Error fetching trips range for ${reqVin}: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};


export const tripsController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trips data");
    let reqVin = request.query.vin;
    let first = request.query.first;
    let skip = request.query.skip;
    try {
        let elasticFirst = 0;
        const doesVinExit = await Trips.exists({ _id: reqVin });
        if (doesVinExit == true) {
            elasticFirst = 10
        } else {
            elasticFirst = 50
        }
        let args = {
            vin: reqVin,
            size: elasticFirst,
        };
        // console.log('Value', elasticFirst);                
        let data = [] = await TripWrapper(args);
        let shortedData = data.reverse();
        Tripsv1.findById(reqVin, function (err, doc) {
            Logger.info('Finding Trips in DB');
            if (!doc) {
                let trips = new Tripsv1({
                    _id: reqVin,
                    tripData: shortedData
                });
                trips.save().then(result => {
                    // console.log("Result of Retrieval",typeof(result))
                    Logger.info('Saving the trips in DB');
                    let results = result["tripData"];
                    // Logger.info(`Results: ${JSON.stringify(results)}`);
                    let dataTemp = JSON.stringify(result["tripData"]);
                    let dataMain = JSON.parse(dataTemp);
                    let data = [];
                    for (let i = parseInt(skip); i <= (parseInt(skip) + 9); i++) {
                        data.push(dataMain[i]);
                    };
                    response.send(data);
                }).catch(err => {
                    Logger.error(err.stack);
                });
            } else {
                let dataStr = JSON.stringify(doc.tripData);
                let datajson = JSON.parse(dataStr);
                shortedData.forEach(element => {
                    let match = datajson.find(el => el.startTime == element.startTime);
                    if (!match) {
                        datajson.unshift(element);
                    };
                });
                let newData = datajson.sort(function (a, b) {
                    return (b.startTime < a.startTime) ? -1 : ((b.startTime > a.startTime) ? 1 : 0);
                });
                doc.tripData = newData;
                doc.save();
                let data = [];
                for (let i = parseInt(skip); i <= (parseInt(skip) + 9); i++) {
                    data.push(newData[i]);
                };
                response.send({status: 200, data});
            }
        });
    } catch(e) {
        Logger.error(`Error fetching trips data for ${reqVin}: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsIdController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for individual trip data");
    let reqTripId = request.query.tripid;
    try {
        const trips: any = async () => {
            const tripData = `
                fragment tripid on Trips{
                    id
                    tripId
                    vin
                    distance
                    energy
                    maxWheelRPM
                    minWheelRPM
                    avgGpsSpeed
                    maxGpsSpeed
                    minGpsSpeed
                    batteryVoltageAdc
                    createdAt
                    updatedAt
                    startTime
                    endTime
                    location{
                        id
                        lat
                        lng
                    }
                    mode{
                        id
                        ECONOMY
                        RIDE
                        SPORT
                        createdAt
                        updatedAt
                    }
                }`
            let trip: any = await prisma.trips({
                where: {
                    tripId: reqTripId,
                },
            }).$fragment(tripData);

            return trip;
        };
        try {
            let data = [];
            data = await trips();
            // console.log(data);
            response.send({status: 200, data});
        } catch (err) {
            Logger.info(err.stack);
        }
    } catch(e) {
        Logger.error(`Error fetching trips data for tripId: ${reqTripId}: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsElasticController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip data (elastic)");
    let reqTripId = request.query.tripid;
    let status = request.query.status;
    let tripVin;
    if (status) {
        // tripVin = await prisma.trip({ tripId: reqTripId }).vin();
        Trips.findOne({ tripId: reqTripId }, async (err, latestTrip: any) => {
            tripVin = latestTrip && latestTrip.tripId;
            if (tripVin) {
                const vehicle = await prisma.vehicle({ vin: tripVin });
                if (!vehicle || vehicle && vehicle.status !== status) {
                    return response.send({ status: 400, data: 'INVALID VIN' });
                }
            }
        })
    }
    try {
        const query: any = {
            bool: {
                must: [{
                    match_all: {},
                }, {
                    match_phrase: {
                        "tripId": reqTripId
                    }
                }],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    },
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };
    
        let data = await searchFromElastic({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: [],
                query,
                size: "all",
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        let resp: any = {
            vin: '',
            ignition: [],
            battery: [],
            uart: [],
            location: [],
            alarm: []
        }
        body.map(log => {
            resp.vin = log.vin
            const type = log._source.type;
            switch (type) {
                case 'I':
                    resp.ignition.push(log._source)
                    break;
                case 'L':
                    resp.location.push(log._source)
                    break;
                case 'B':
                    resp.battery.push(log._source)
                    break;
                case 'A':
                    resp.alarm.push(log._source)
                    break;
                case 'U':
                    resp.uart.push(log._source)
                    break;
            }
        });
        response.send({status: 200, data: resp});
    } catch(e) {
        Logger.error(`Error fetching trips data (elastic): ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsAnalyticsController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip analytics");
    let reqCId = request.query.id;
    let first = request.query.first;
    let skip = request.query.skip;
    try {
        const trips: any = async () => {
            let where = {
                id: reqCId,
            };
            const companyVin = `
                    fragment vinOfCompany on Company{
                        vehicles{
                            vin
                        }
                    }`

            const vinOfCompany = await prisma.companies({ where: where }).$fragment(companyVin);
            // console.log('Data Received', vinOfCompany[0].vehicles);
            let vins = [];
            vins = vinOfCompany[0].vehicles.map(item => {
                return item.vin;
            });
            const tripData = `
                    fragment trips on Trips{ 
                        id
                        tripId
                        vin
                        startTime
                        endTime
                        location{
                            lat
                            lng
                        }
                    }`
            let trip: any = await prisma.trips({
                where: {
                    vin_in: vins,
                },
                orderBy: 'updatedAt_DESC',
                first: parseInt(first),
                skip: parseInt(skip)
            }).$fragment(tripData);

            return trip;
        };
        try {
            let data = [];
            data = await trips();
            request.setTimeout(300000);
            // console.log(data);
            response.send({status: 200, data});
            // db.set(`${reqCId}_All_Trips`, data);
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch(e) {
        Logger.error(`Error fetching trips analytics: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsAnalyticsAllController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip analytics for a company");
    let reqCId = request.query.id;
    let first = parseInt(request.query.first);
    let skip = parseInt(request.query.skip);
    try {
        const companies = await prisma.companies({ where: {
            id: reqCId
        }});
        if (!companies[0])
            return response.send({status: 500, data: 'INVALID COMPANY'});
        const company = companies[0];
        let rules: any = {
            status: "RENTAL",
        };
        if (["DISTRIBUTOR", "RIDE_SHARING"].indexOf(company.type) > -1) {
            rules = {
                ...rules,
                distributor: {
                    id: company.id,
                },
                status_not: "DELETED",
            };
        } else {
            rules = {
                ...rules,
                oem: {
                    id: company.id,
                },
                status_not: "DELETED",
            };
        };
        const companyVin = `
            fragment vinOfVehicles on Vehicles{
                vin
            }`

        const vinOfCompany: any = await prisma.vehicles({ where: rules }).$fragment(companyVin);
        // console.log('Data Received', vinOfCompany);                
        const vins = vinOfCompany.map(vin => vin.vin);
        let resp = [];
        let doc: any = await Trips.find({ vin: {'$in': vins}, 'tripData.distance': {'$gte': 100}}, null, { limit: first, skip})
        Logger.info(`docs size: ${doc.length}`);
        let args: any = {
            vins,
            size: first,
            skip: 0
        };
        if (doc.length) {
            const existingTripIds = doc.map(el => el.tripId);
            let existingTripsLogs: any = await tripV2Logs({ ...args, tripIds: existingTripIds });
            // console.log('existingTripsLogs',existingTripsLogs);
            existingTripsLogs = existingTripsLogs.trips;
            doc = doc.map(el => {
                let existingTripLogs = existingTripsLogs.find(trip => trip.tripId === el.tripId);
               Logger.info(`existingTripLogs ${existingTripLogs}`);
                return {
                    ...el,
                    tripData: {
                        ...el.tripData,
                        location: existingTripLogs && existingTripLogs.location || [],
                        batteryVoltageAdc: existingTripLogs && existingTripLogs.batteryVoltageAdc || []
                    }
                }
            })
            doc = doc.map(d => d.tripData);
            resp.push(...doc);
        }
        
        let { tripCount } = await tripIdsV2(args);
        args = {
            ...args,
            skip: resp.length
        }
        while (resp.length < first) {
            Logger.info(`response: ${resp.length} ${args}`);
            args = {
                ...args,
                size: first - resp.length
            }
            let { tripIds } = await tripIdsV2(args);
            if (!tripIds.length) {
                break;
            }
            Logger.info(`tripIds: ${tripIds}`);
            let tripsInDb: any = await Trips.find({ vin: {'$in': vins}, tripId: { '$in': tripIds }}, {tripId: 1});
            tripsInDb = tripsInDb.map(trip => trip.tripId);
            tripIds = tripIds.filter(trip => !tripsInDb.includes(trip));
            Logger.info(`newtripids ${tripIds}, ${tripsInDb}`);
            if (tripIds.length) {
                let { trips }: any = await tripV2({ ...args, tripIds });
                // console.log('trips', trips);
                let shortedData = trips.map(trip => {
                    let tripData = _.cloneDeep(trip);
                    delete tripData.location;
                    delete tripData.batteryVoltageAdc;
                    return { _id: uuidV4(), vin: trip.vin, tripId: trip.tripId, tripData }
                });

                await Trips.insertMany(shortedData);
                trips = trips.filter(trip => {
                    return trip.distance >= 100;
                })
                resp.push(...trips);
                args.skip = args.skip + tripIds.length + tripsInDb.length;
            } else {
                args.skip = args.skip + tripsInDb.length;
            }
        }
        return response.send({ trips: resp, tripCount });
    } catch(e) {
        Logger.error(`Error fetching for trip analytics for a company: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsAnalyticsLeaseController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip leases for a company");
    let reqCId = request.query.id;
    try {
        const trips: any = async () => {
            const leaseData = `
                fragment lease on Leases{
                    bookingTime
                    leasee{
                      id
                    }                
                    contract{
                        txInfo{
                            passbook{
                                amount
                                status
                            }
                        }
                    }                  
                }`
            let lease: any = await prisma.leases({
                where: {
                    leasor: {
                        company: {
                            id: reqCId
                        }
                    }
                },
                orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let data = [];
            data = await trips();
            // console.log('Length data', data.length);
            let dataTemp = data.map(item => {
                if (item.bookingTime) {
                    let curDate = item.bookingTime;
                    let tempDate = curDate.split('T');
                    return {
                        date: tempDate[0],
                        contract: item.contract,
                        leasee: item.leasee
                    }
                } else {
                    Logger.info(`Booking time missing for trip ${JSON.stringify(item)}`);
                }
            })
            let dateAmountMap = new Map();
            dataTemp.map(item => {
                let tempObj = dateAmountMap.get(item.date);
                if (tempObj) {
                    tempObj.count++;
                    if (item.leasee.id && !tempObj.userId.has(item.leasee.id)) {
                        tempObj.userId.add(item.leasee.id);
                        tempObj.userCount++;
                    } else {
                        tempObj.userId.add(item.leasee)
                    }
                    let amount = 0;
                    if (item.contract && item.contract.txInfo && item.contract.txInfo.passbook) {
                        item.contract.txInfo.passbook.map(items => {
                            if (items.status == 'COMPLETED' || items.status == 'PAID') {
                                amount += items.amount ? items.amount : 0;
                            }
                        })
                    }
                    tempObj.amount += amount;
                    dateAmountMap.set(item.date, tempObj);
                } else {
                    let dateAmountObj = {
                        date: item.date,
                        amount: 0,
                        userId: new Set(),
                        userCount: 0,
                        count: 1
                    };
                    dateAmountObj.date = item.date;
                    if (item.leasee.id){
                        dateAmountObj.userCount = 1;
                        dateAmountObj.userId.add(item.leasee.id);
                    }
                    let amount = 0;
                    if (item.contract && item.contract.txInfo && item.contract.txInfo.passbook) {
                        item.contract.txInfo.passbook.map(items => {
                            if (items.status == 'COMPLETED' || items.status == 'PAID') {
                                amount += items.amount ? items.amount : 0;
                            }
                        })
                    };
                    dateAmountObj.amount = amount;
                    dateAmountMap.set(item.date, dateAmountObj);
                }
            });

            let earningDataAnalytics = {
                data: [],
                totalEarning: 0,
                totalBookings: 0
            }
            for (let entry of dateAmountMap.values()){
                earningDataAnalytics.data.push({
                    date: entry.date,
                    amount: entry.amount,
                    userId: [...entry.userId],
                    userCount: entry.userCount,
                    count: entry.count
                });
                earningDataAnalytics.totalEarning += entry.amount;
                earningDataAnalytics.totalBookings += entry.count;
            }
            response.send({ status: 200, ...earningDataAnalytics });
            // db.set(`${reqCId}_Lease_Data`, earningDataAnalytics);
            // res.send(data)
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch(e) {
        Logger.error(`Error fetching for trip analytics for a company: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsAnalyticsV2Controller = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip analytics for a company");
    let reqCId = request.query.id;
    let startTime = request.query.startTime;
    let endTime = request.query.endTime || moment().format('YYYY-MM-DD');
    if (!startTime) {
        startTime = new Date(endTime);
        startTime = startTime.setDate(startTime.getDate() - 7);
        startTime = moment(startTime).format('YYYY-MM-DD');
    }
    let datesInBetween = getDaysArray(startTime, endTime);

    const companies = await prisma.companies({ where: { id: reqCId } });
    if (!companies[0]) {
        return response.send({status: 500, data: 'INVALID COMPANY'})
    }
    const company = companies[0];
    let rules: any = {
        status: "RENTAL",
    };
    if (["DISTRIBUTOR", "RIDE_SHARING"].indexOf(company.type) > -1) {
        rules = {
            ...rules,
            distributor: {
                id: company.id,
            },
            status_not: "DELETED",
        };
    } else {
        rules = {
            ...rules,
            oem: {
                id: company.id,
            },
            status_not: "DELETED",
        };
    };
    try {
        let currentDate = moment(new Date()).format('YYYY-MM-DD');
        let { vins, totalDistance, tripCount } = await calculateTotalDistance(rules, startTime, endTime);
        TripDistance.find({ companyId: reqCId, date: { '$gte': startTime, '$lte': endTime } }, { distance: 1, count: 1, date: 1 }, null, async (err, doc: any) => {
            Logger.info('finding dates in db');
            if (!doc.length) {
                const args = {
                    size: 10000,
                    vins,
                    startTime: startTime || "now-7d",
                    endTime: endTime || "now"
                }
                let tripIdsV2Data = await tripIdsV2(args);
                let tripIds = tripIdsV2Data.tripIds || [];
                let tripCountAll = tripIdsV2Data.tripCount;
                let skip = 10000;
                while (tripIds.length < tripCountAll) {
                    let newTripIdsData = await tripIdsV2({ ...args, skip });
                    tripIds = [...tripIds, ...newTripIdsData.tripIds];
                    skip += newTripIdsData.tripIds.length;
                    if (newTripIdsData.tripIds.length === 0) {
                        break;
                    }
                }
                if (tripIds.length) {
                    let data: any = await calculateDateDistances(tripIds, datesInBetween);
                    let dataDb = data.map(d => {
                        return { ...d, companyId: reqCId }
                    })
                    await TripDistance.insertMany(dataDb);
                    let finalDataDates = data.map(d => moment(d.date).format('YYYY-MM-DD'));
                    let zeroDistanceDates = datesInBetween.filter(d => !finalDataDates.includes(d))
                    if (zeroDistanceDates.length) {
                        zeroDistanceDates.map(d => data.push({ count: 0, distance: 0, date: new Date(d) }))
                    }
                    data = data.sort((a, b) => {
                        return b.date - a.date;
                    })
                    response.send({ status: 200, data, tripCount, totalDistance })
                } else {
                    response.send({ status: 200, data: [], tripCount: 0, totalDistance: 0 })
                }
            } else {
                let datesInDb = doc.map(d => moment(d.date).format('YYYY-MM-DD'));
                let filterdDates = datesInBetween.filter(d => {
                    if (d === currentDate) {
                        return d;
                    }
                    return !datesInDb.includes(d);
                });
                let ranges = [];
                filterdDates.length && filterdDates.map(date => {
                    ranges.push({
                        range: {
                            "timestamp": {
                                "gte": date,
                                "lte": `${date}T23:59:59`
                            }
                        }
                    })
                })
                const args = {
                    size: 10000,
                    vins,
                    ranges
                }
                let tripIdsV2Data = await tripIdsV2(args);
                let tripIds = tripIdsV2Data.tripIds || [];
                let tripCountAll = tripIdsV2Data.tripCount;
                Logger.info(`tripCountAll: ${tripCountAll}`)
                let skip = 10000;
                while (tripIds.length < tripCountAll) {
                    let newTripIdsData = await tripIdsV2({ ...args, skip });
                    Logger.info(`newTripIdsData ${newTripIdsData.tripIds.length}`);
                    tripIds = [...tripIds, ...newTripIdsData.tripIds];
                    Logger.info(`tripIds len ${tripIds.length}`);
                    skip += newTripIdsData.tripIds.length;
                    if (newTripIdsData.tripIds.length === 0) {
                        break;
                    }
                }

                Logger.info(`tripsIds: ${tripIds.length}, ${tripCountAll}`);

                let filteredTripIds = tripIds.filter(tripId => {
                    return filterdDates.includes(moment(parseInt(tripId)).format('YYYY-MM-DD'))
                });
                let data: any = [];
                if (filteredTripIds.length) {
                    data = await calculateDateDistances(filteredTripIds, datesInBetween);
                }
                let dataDb = data && data.length && data.map(d => {
                    return { ...d, companyId: reqCId }
                }) || [];
                let currentDateDataIndex = data && data.length && data.findIndex(d => {
                    Logger.info(moment(d.date).format('YYYY-MM-DD'), currentDate);
                    return moment(d.date).format('YYYY-MM-DD') === currentDate;
                });
                let currentDateData;
                Logger.info(`currentDateDataIndex: ${currentDateDataIndex}, ${JSON.stringify(data)}, ${JSON.stringify(dataDb)}`);
                if (currentDateDataIndex > -1) {
                    currentDateData = dataDb[currentDateDataIndex];
                    dataDb.splice(currentDateDataIndex, 1)
                    // data.splice(currentDateDataIndex, 1)
                    // TripDistance.remove({companyId: reqCId, date: currentDate})
                }
                if (dataDb) {
                    await TripDistance.insertMany(dataDb)
                }
                if (currentDateData) {
                    await TripDistance.findOneAndUpdate({ companyId: reqCId, date: currentDate }, currentDateData)
                }
                let finalData: any = _.unionBy(doc, data, 'date');
                let finalDataDates = finalData.map(d => moment(d.date).format('YYYY-MM-DD'));
                let zeroDistanceDates = datesInBetween.filter(d => !finalDataDates.includes(d))
                if (zeroDistanceDates.length) {
                    zeroDistanceDates.map(d => finalData.push({ count: 0, distance: 0, date: new Date(d) }))
                }
                finalData = finalData.sort((a, b) => {
                    return b.date - a.date;
                });
                response.send({ status: 200, data: finalData, tripCount, totalDistance });
            }
        })
    } catch(e) {
        Logger.error(`Error fetching for trip analytics for a company: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const tripsAnalyticsLeaseHealthController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running trips router for trip leases health for a company");
    let reqCId = request.query.id;
    try {
        const trips: any = async (skip) => {
            // console.log('Trips Called');
            let filter: any = {
                company: {
                    id: reqCId,
                },
                status: "RENTAL"
            };
            const companyVin = `
                fragment vinOfVehicles on Vehicles{
                    vin                    
                }`

            const vinOfCompany: any = await prisma.vehicles({ where: filter }).$fragment(companyVin);
            // console.log('Data Received', vinOfCompany);                
            const vins = vinOfCompany.map(vin => vin.vin);
            // console.log('Vin Filetred', JSON.parse(vins));

            return vins;
            // console.log('Trips Fetched')
        };
        try {
            let data = [];
            data = await trips();
            let lastRecords = await fetch('https://api.revos.in/v2/devices/active?token=1234');
            let records = await lastRecords.json();
            let pingTimesData = records.data;
            // console.log('Data Test', pingTimesData);
            let allVehicles = pingTimesData.filter(v => {
                return data.includes(v.vin);
            })
            Logger.info(`Filter: ${allVehicles.length}`);
            let healthy = 0;
            let unknown = 0;                
            allVehicles.map(item => {
                if(item.connected){
                    healthy++
                } else {
                    unknown++
                }
            });                
            let healthData = {
                data: allVehicles,
                healthy: healthy,
                unknown: unknown
            }
            response.send({ status: 200, ...healthData});
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch(e) {
        Logger.error(`Error fetching for trip analytics for a company: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};