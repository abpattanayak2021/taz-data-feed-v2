import { Request, Response, Next } from 'express';
import { prisma } from '../../Shared/DBManager/Prisma/prisma-client';
import { elastic } from '../../Shared/DBManager/Elastic/elastic';
import { db } from '../../Shared/CacheManager/cacheManager';
import * as _ from 'lodash';
import Logger from '../utilities/logger';

const VEHICLE_LEVEL_2_INDEX = 'vehicle002';

const createElasticQuery = (type, vin, startTime, endTime, isSmart) => {
    const query: any = {
        bool: {
            must: [{
                match_all: {},
            },
            {
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            }],
            must_not: [
                {
                    match_phrase: {
                        "timestampVerified": {
                            query: false,
                        },
                    },
                }, 
                {
                    match_phrase: {
                        "latitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "longitude": {
                            query: 0
                        }
                    }
                }
            ],
        },
    };
    if (!isSmart) {
        query.bool.must_not.push({
            match_phrase: {
                "batteryVoltageAdc": 0
            }
        });
    }
    if (startTime && endTime) {
        Logger.info(`startendtime: ${startTime}, ${endTime}, ${parseInt(startTime)}`);
        query.bool.must.push({
            range: {
                timestamp: {
                    gte: startTime,
                    lte: endTime,
                },
            },
        });
    }
    if (Array.isArray(type)) {
        query.bool.must.push({
            bool: {
                should: [],
                minimum_should_match: 1
            }
        })
        type.forEach((t) => {
            query.bool.must[2].bool.should.push({
                match_phrase: {
                    "type": t,
                },
            });
        });
    } else if (type) {
        query.bool.must.push({
            match_phrase: {
                "type": {
                    query: type,
                },
            },
        });
    }
    return query;
};

const searchFromElastic = async ({query, index}) => {
    return elastic.search({
        index,
        query,
    });
};

const getVehicleSnapshotElastic = async(args) => {
    const { vin } = args;
    try {
        let _source = [];
        let vehicle: any = await prisma.vehicle({ vin });
        vehicle = {
            ...vehicle,
            model: await prisma.vehicle({vin}).model()
        }
        if (!vehicle) {
            Logger.error(`INVALID VIN: ${vin}`);
            throw new Error(`INVALID VIN: ${vin}`);
        }
        let isSmart = false;
        if (vehicle.model && ["SMART", "SMART_PLUS"].includes(vehicle.model.protocol)) {
            isSmart = true;
        }
        let query = createElasticQuery(null, vin, null, null, isSmart);
        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source,
                query,
                collapse: {
                    field: "type.keyword"
                },
                size: 10,
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        let resp: any = {
            vin: vin,
            ignition: [],
            battery: [],
            uart: [],
            location: [],
            alarm: []
        }
        body.map(log => {
            const type = log._source.type;
            if (typeof log._source.timestamp === 'number') {
                log._source.timestamp = new Date(log._source.timestamp);
            }
            switch(type) {
                case 'I':
                    resp.ignition.push(log._source)
                    break;
                case 'L':
                    resp.location.push(log._source)
                    break;
                case 'B':
                    resp.battery.push(log._source)
                    break;
                case 'A':
                    resp.alarm.push(log._source)
                    break;
                case 'U':
                    resp.uart.push(log._source)
                    break;
            }
        });
        let processedResponse = {
            vin,
            ignition: resp.ignition.length && resp.ignition[0] || {},
            battery: resp.battery.length && resp.battery[0] || {},
            uart: resp.uart.length && resp.uart[0] || {},
            location: resp.location.length && resp.location[0] || {},
            alarm: resp.alarm.length && resp.alarm[0] || {},
        };
        return processedResponse;

    } catch (e) {
        Logger.error(`Error fetching vehicle snapshot from elastic ${e.stack}`);
        throw new Error("Error fetching vehicle snapshot");
    }
}

const getVehicleLogsElastic = async(args) => {
    let { vin, startTime, endTime, tripId } = args;
    try {
        let vinRide = null;
        let endTimeCurrent = null;
        let startTimeCurrent = null;
        if (tripId) {
            let currentRide: any = await prisma.ride({ id: tripId });
            currentRide = {
                ...currentRide,
                vehicle: await prisma.ride({ id: tripId }).vehicle(),
            };
            vinRide = currentRide.vehicle.vin;
            startTimeCurrent = currentRide.startTime ? Date.parse(currentRide.startTime) : Date.now();
            endTimeCurrent = currentRide.endTime ? Date.parse(currentRide.endTime) : Date.now();
        }
        if (vinRide)
            vin = vinRide;
        let isSmart = false;
        let vehicle: any = await prisma.vehicle({vin});
        if (!vehicle) {
            throw new Error('INVALID VIN');
        }
        vehicle = {
            ...vehicle,
            model: await prisma.vehicle({vin}).model(),
        }
        if (vehicle.model && ["SMART", "SMART_PLUS"].includes(vehicle.model.protocol)) {
            isSmart = true;
        }
        if (!endTime && !endTimeCurrent) {
            endTime = Date.now();
        }
        if (!startTime && !startTimeCurrent) {
            const d = new Date();
            d.setDate(d.getDate() - 7);
            startTime = Date.parse(d.toString());
        }
        let _source = [];
        let query = createElasticQuery(null, vin || vinRide, startTime || startTimeCurrent, endTime || endTimeCurrent, isSmart);
        let data = await searchFromElastic({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source,
                query,
                size: "all",
                sort: [
                    {
                      timestamp: {
                        order: "desc",
                      },
                    },
                  ],
            },
        });

        let body = data.searchData;
        if (data.total === 0) {
            query = createElasticQuery(null, vin || vinRide, null, null, isSmart);
            data = await elastic.searchWithoutScroll({
                index: VEHICLE_LEVEL_2_INDEX,
                query: {
                    _source,
                    query,
                    collapse: {
                        field: "type.keyword"
                    },
                    size: 10,
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                            },
                        },
                    ],
                },
            });
            body = data.searchData;
        }
        let resp: any = {
            vin: vin || vinRide,
            ignition: [],
            battery: [],
            uart: [],
            location: [],
            alarm: []
        }
        body.map(log => {
            const type = log._source.type;
            if (typeof log._source.timestamp === 'number') {
                log._source.timestamp = new Date(log._source.timestamp);
            }
            switch(type) {
                case 'I':
                    resp.ignition.push(log._source)
                    break;
                case 'L':
                    resp.location.push(log._source)
                    break;
                case 'B':
                    resp.battery.push(log._source)
                    break;
                case 'A':
                    resp.alarm.push(log._source)
                    break;
                case 'U':
                    resp.uart.push(log._source)
                    break;
            }
        });
        return resp;
    } catch (e) {
        Logger.error(`Error fetching vehicle logs from elastic: ${e.stack}`);
        throw new Error("Error fetching vehicle logs");
    }
}

export const vehicleSnapshotController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running vehicle router for vehicle snapshot")
    let vin = request.query.vin;
    try {
        const data = await getVehicleSnapshotElastic({ vin });
        response.send({status: 200, data});
    } catch(e) {
        Logger.error(`Error fetching vehicle snapshot( vin ${vin}): ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const vehicleLogsController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running vehicle router for vehicle logs")
    let vin = request.query.vin
    let startTime = request.query.startTime;
    let endTime = request.query.endTime;
    let tripId = request.query.tripid;
    let mobile = request.query.mobile;
    let count = request.query.count;
    try {
        let data = await getVehicleLogsElastic({ 
            vin,
            startTime,
            endTime,
            tripId
        });
        if (mobile === "true") {
            let logs = [];
            const mergeByTimestamp = (a1, a2) => {
                let mergedLogs = _.merge(a1, a2);
                return mergedLogs;
            }
            logs = mergeByTimestamp(data.location, data.battery);
            logs = mergeByTimestamp(logs, data.ignition);
            logs = mergeByTimestamp(logs, data.uart);
            logs = mergeByTimestamp(logs, data.alarm);
            data = [...logs]
            if(count && count < data.length){
                data = _.slice(data, 0, count);                                
            };

        }
        response.send({status: 200, data});
    } catch(e) {
        Logger.error(`Error fetching vehicle logs (vin ${vin}): ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const vehicleRentalController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running vehicle router for all rental vehicle data")
    let packageName = request.query.package;
    let { locationBounds } = request.body;
    if (!packageName) {
        return response.send({status: 500, data: 'INVALID COMPANY'})
    }
    try {
        const companies = await prisma.companies({ where: {
                apps_some: {
                    package: packageName
                }
            }
        });
        if (!companies[0]) {
            return response.send({status: 500, data: 'INVALID COMPANY'})
        }
        const company = companies[0];
        let rules: any = {
            status: "RENTAL",
            rentalStatus: "AVAILABLE"
        };
        if (["DISTRIBUTOR", "RIDE_SHARING"].indexOf(company.type) > -1) {
            rules = {
                ...rules,
                distributor: {
                    id: company.id,
                },
                status_not: "DELETED",
            };
        } else {
            rules = {
                ...rules,
                oem: {
                    id: company.id,
                },
                status_not: "DELETED",
            };
        };
        let allVehicleStates: any = await fetch(`https://api.revos.in/v2/vehiclestate?token=1234`);
        allVehicleStates = await allVehicleStates.json();
        let vehicleData = [];
        vehicleData = allVehicleStates.data.length && allVehicleStates.data.map(v => {
            return {
                vin: v.vin,
                latitude: v.state.location.latitude || 0,
                longitude: v.state.location.longitude || 0,
                batteryVoltageAdc: v.state.battery && v.state.battery.batteryVoltageAdc || 0,
                batteryVoltage: v.state.uart && v.state.uart.batteryVoltage || 0
            }
        }) || [];
        vehicleData = _.compact(vehicleData);
        if (!vehicleData.length) {
            return response.send({status: 200, data: []});
        }
        if (locationBounds) {
            let {
                latitudeTop,
                latitudeBottom,
                longitudeLeft,
                longitudeRight
            } = locationBounds;
            vehicleData = vehicleData.filter(v => {
                if (v.latitude >= latitudeBottom && v.latitude <= latitudeTop && v.longitude >= longitudeLeft && v.longitude <= longitudeRight) {
                    return v;
                }
            }) || [];
        }
        let vins = vehicleData.map(v => v.vin);
        let rentalVehicleFragment = `
            fragment rentalVehicles on Vehicles {
                vin
                status
                rentalStatus
                lastMarkedLatitude
                lastMarkedLongitude
                isDeliverable
                company {
                    id
                    name
                    phone
                }
                oem {
                    id
                    name
                    phone
                }
                distributor {
                    id
                    name
                    phone
                }
                model {
                    name
                    company {
                        id
                        name
                        phone
                    }
                    config {
                        batteryMinVoltage
                        batteryMaxVoltage
                    }
                }
            }`;
        let vehicles = [];
        if (company.name === 'REVOS') {
            vehicles = await prisma.vehicles({where: { vin_in: vins }}).$fragment(rentalVehicleFragment);
        } else {
            vehicles = await prisma.vehicles({where: { vin_in: vins, ...rules }}).$fragment(rentalVehicleFragment);
        }
        let finalData = [];
        vehicles.length && vehicles.forEach(v => {
            delete v.lastMarkedLatitude;
            delete v.lastMarkedLongitude;
            let vehicle = vehicleData.find(ve => ve.vin === v.vin);
            finalData.push({
                ...v,
                ...vehicle
            });
        });
        return response.send({status: 200, data: finalData});
    } catch(e) {
        Logger.error(`Error fetching all rental vehicle data: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};

export const individualVehicleController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running vehicle router for individual vehicle details")
    let vin = request.query.vin;
    try {
        const leases: any = async () => {
            const leaseData = `
                fragment lease on Leases{
                    id
                    status
                    type
                    startOdo
                    endOdo
                    createdAt
                    updatedAt
                    startTime
                    endTime
                    bookingTime
                    asset{
                        type
                        vehicle{
                            id
                            vin
                        }
                    }
                    leasor{
                        company{
                            name
                            id
                        }
                    }
                    leasee{
                        user{
                            firstName
                            lastName
                            phone
                            email
                        }
                    }
                    contract{
                        txInfo{
                            passbook{
                            amount
                            status
                            }
                        }
                    }                  
                }`
            let lease: any = await prisma.leases({
                where: {
                    asset: {
                        vehicle: {
                            vin: vin
                        }
                    }
                },
                orderBy: 'updatedAt_DESC',
            }).$fragment(leaseData);
            return lease;
        };
        try {
            let data = [];
            data = await leases();
            // console.log('Length data', data.length);
            let dataTemp = data.map(item => {
                if (item.bookingTime) {
                    return {
                        date: item.bookingTime.split('T')[0],
                        contract: item.contract,
                        leasee: item.leasee
                    };
                } else {
                    Logger.info(`Booking time null`);
                }
            })
            // console.log('Length', dataTemp.length);
            // console.log(data);
            let dateAmountMap = new Map();
            dataTemp.map(item => {
                let tempObj = dateAmountMap.get(item.date);
                if (tempObj) {
                    tempObj.count++;
                    if (item.leasee.id && !tempObj.userId.has(item.leasee.id)) {
                        tempObj.userId.add(item.leasee.id);
                        tempObj.userCount++;
                    } else {
                        tempObj.userId.add(item.leasee);
                    }
                    let amount = 0;
                    if (item.contract && item.contract.txInfo && item.contract.txInfo.passbook) {
                        item.contract.txInfo.passbook.map(items => {
                            if (items.status == 'COMPLETED' || items.status == 'PAID') {
                                amount += items.amount ? items.amount : 0;
                            }
                        })
                    }
                    tempObj.amount += amount;
                    dateAmountMap.set(item.date, tempObj);
                } else {
                    let dateAmountObj = {
                        date: item.date,
                        amount: 0,
                        userId: new Set(),
                        userCount: 0,
                        count: 1
                    };
                    dateAmountObj.userCount = 1;
                    dateAmountObj.userId.add(item.leasee.id);
                    let amount = 0;
                    if (item.contract && item.contract.txInfo && item.contract.txInfo.passbook) {
                        item.contract.txInfo.passbook.map(items => {
                            if (items.status == 'COMPLETED' || items.status == 'PAID') {
                                amount += items.amount ? items.amount : 0;
                            }
                        })
                    };
                    dateAmountObj.amount = amount;
                    dateAmountMap.set(item.date, dateAmountObj);
                }
            });

            let earningDataAnalytics = {
                data: [],
                totalEarning: 0,
                totalBookings: 0
            }

            for (let entry of dateAmountMap.values()){
                earningDataAnalytics.data.push({
                    date: entry.date,
                    amount: entry.amount,
                    userId: [...entry.userId],
                    userCount: entry.userCount,
                    count: entry.count
                });
                earningDataAnalytics.totalEarning += entry.amount;
                earningDataAnalytics.totalBookings += entry.count;
            }
            // console.log('Lease Data', earningDataAnalytics);
            let vehicleLeaseInfo = {
                leases: data,
                info: earningDataAnalytics 
            }
            response.send({status: 200, data: vehicleLeaseInfo});
       
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch(e) {
        Logger.error(`Error fetching individual vehicle details: ${e.stack}`);
        response.send({status: 500, data: "TRY AGAIN LATER"});
    }   
};