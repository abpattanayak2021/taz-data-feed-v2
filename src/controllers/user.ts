import { Request, Response, Next } from 'express';
import { prisma } from '../../Shared/DBManager/Prisma/prisma-client';
import * as _ from 'lodash';
import Logger from '../utilities/logger';

import * as moment from "moment";

export const userVehicleController =  async (request: Request, response: Response, next: Next) => {
    const companyId = request.query.id
    Logger.info("Running user router for vehicles")
    try {
        // prisma query for vehicle leases
        let leases: any = async () => {
            let filter: any = {
                asset: {
                    type: "VEHICLE"
                },
                leasor: {
                    company: {
                        id: companyId
                    }
                }
            };

            const vehicleLeasesOfCompany = `
                fragment lease on Leases {
                    id
                    status
                    type
                    bookingTime
                    startTime
                    endTime
                    asset {
                      vehicle {
                        id
                        vin
                        rentalStatus
                      }
                    }
                    contract {
                        txInfo {
                            invoice {
                                passbook {
                                    amount
                                    status
                                }
                            }
                        }
                    }
                    leasee {
                        user {
                            id
                            firstName
                            lastName
                            email
                            phone
                        }
                    }
                    leasor {
                        company {
                            id
                            name
                        }
                    }
                }`

            let userLeases: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(vehicleLeasesOfCompany);

            return userLeases;
        };

        let data = await leases();
        let filteredUsersList = []
        // Filter Data For Unique Users
        // Populate the filteredUserList Array
        // Send the data in Response        
        data.forEach(item => {
            if (item.leasee && item.leasee.user) {
                let userObj = {
                    id: '',
                    firstName: '',
                    lastName: '',
                    email: '',
                    phone: ''
                }
                let findUserInFilteredUsersList = filteredUsersList.find(o => o.id === item.leasee.user.id)
                // console.log("-----------------Filter User--------------", findUserInFilteredUsersList);
                if (!findUserInFilteredUsersList) {
                    userObj.id = item.leasee.user.id;
                    userObj.firstName = item.leasee.user.firstName;
                    userObj.lastName = item.leasee.user.lastName;
                    userObj.email = item.leasee.user.email;
                    userObj.phone = item.leasee.user.phone;
                    filteredUsersList.push(userObj);
                };
            }
        });
        let count = _.size(filteredUsersList);
        let responseData = {
            totalcount: count,
            data: filteredUsersList
        };
        response.json({ status: 200, data: responseData });
    } catch (err) {
        // Console the error
        // Send the error Response
        Logger.error(`Error in user controller for vehicles: ${err.stack}`);
        response.json({ status: 400, data: err.message });
    };
    // return response.json({
    //     status: 200,
    //     companyId,
    //     message: "User Vehicles"
    // })
}

export const userIndividualVehicleController = async (req: Request, res: Response, next: Next) => {
    Logger.info("Running user router for individual vehicle")
    let userId = req.query.id;
    let companyId = req.query.companyId;
    let filter = {};

    //TODO - Remove if/else condition from filter and make the query including the companyId once frontend changes are done.
    if(companyId){
        filter = {
                asset: {
                    type: "VEHICLE"
                },
                leasee: {
                    user: {
                        id: userId
                    }
                },
                leasor:{
                    company:{
                        id: companyId
                    }
                }
            };
    } else {
        filter = {
                asset: {
                    type: "VEHICLE"
                },
                leasee: {
                    user: {
                        id: userId
                    }
                },                
            };
    };

    try {
        // Query the data from PRISMA
        // Return data from the query function
        let leases: any = async () => {
            const userData = `
                fragment lease on Leases{
                    id
                    status
                    type
                    bookingTime
                    startTime
                    endTime
                    asset{
                      vehicle{
                        id
                        vin
                        rentalStatus
                      }
                    }
                    contract{
                        txInfo{
                            invoice{
                                passbook{
                                    amount
                                    status
                                }
                            }
                        }
                    }
                    leasee{
                      user{
                        id
                        firstName
                        lastName
                        email
                        phone
                      }
                    }
                    leasor{
                      company{
                        id
                        name
                      }
                    }
                }`

            let userLeases: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(userData);

            return userLeases;
        };

        let data = await leases();
        console.log('------Data Drivers------', data)        
        let usersData = {
            duration: 0,
            earning: 0,            
            bookings: 0
        }
        // Filter Data For Unique Users
        // Assign individual fileds of the usersData Object
        // Send the data in Response        
        data.forEach(item => {
            // && item.contract.txInfo && item.contract.txInfo.passbook
            if(item.contract && item.contract.txInfo && item.contract.txInfo.invoice && item.contract.txInfo.invoice.passbook){
                let transactions = item.contract.txInfo.invoice.passbook
                transactions.forEach(txn => {
                    usersData.earning += txn.amount
                });
                // usersData.earning += item.item.contract.txInfo.passbook.amount;
            };            
            if(item.startTime && item.endTime){
                usersData.duration += moment(item.endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(item.startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"), 'minutes');
            };            
        });
        usersData.bookings = _.size(data);
        let response = {
            userData: usersData,
            data: data
        };
        res.json({ status: 200, response: response });

    } catch (err) {
        // Console the error
        // Send the error Response
        console.error(err);
        res.json({ status: 400, message: "Error Fetching Details!" })
    };
};

export const userChargerController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running user router for chargers")
    let companyId = request.query.id;
    try {
        // Query the data from PRISMA
        // Return data from the query function
        let leases: any = async () => {
            let filter: any = {
                asset: {
                    type: "COMPONENT",
                    component: {
                        owner: {
                            id: companyId
                        }
                    }
                }
            };

            const userData = `
                fragment lease on Leases{
                    id
                    startTime
                    endTime
                    leaseeConsumption
                    status
                    leasee{
                        user{
                            id
                            email
                            phone
                            firstName
                            lastName
                        }
                    }
                }`

            let userLeases: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(userData);

            return userLeases;
        };

        let data = await leases();
        let users = new Set();
        // Filter Data For Unique Users
        // Populate the users Set
        // Send the usersData in Response      
        const filteredUserData =  data.filter(item => {
            if (!item.leasee || !item.leasee.user)
                return false;
            if (users.has(item.leasee.user.id))
                return false;
            users.add(item.leasee.user.id)
            return true
        })

        const usersData = filteredUserData.map(item => {
            return {
                id: item.leasee.user.id,
                firstName: item.leasee.user.firstName,
                lastName: item.leasee.user.lastName,
                email: item.leasee.user.email,
                phone: item.leasee.user.phone
            }
        })
        let count = _.size(usersData);
        let responseData = {
            totalcount: count,
            data: usersData
        };
        response.json({ status: 200, response: responseData });
    } catch (err) {
        // Console the error
        // Send the error Response
        console.log(err)
        Logger.error(err);
        response.json({ status: 400, message: "Error Fetching Details!" })
    };
}

export const userIndividualChargerController = async (request: Request, response: Response, next: Next) => {
    Logger.info("Running user router for individual charger")
    let userId = request.query.id;
    try {
        // Query the data from PRISMA
        // Return data from the query function
        let leases: any = async () => {
            let filter: any = {
                asset: {
                    type: "COMPONENT"                    
                },
                leasee:{
                    user:{
                        id: userId
                    }
                }
            };

            const userData = `
            fragment lease on Leases{
                id
                startTime
                endTime
                leaseeConsumption
                status
                contract{
                    txInfo{
                        invoice{
                            passbook{
                                amount
                                status
                            }
                        }
                    }
                }
                leasee{
                    user{
                        id
                        email
                        phone
                        firstName
                        lastName
                    }
                }
                asset{      
                    component{
                        id
                        UID
                        owner{
                        id
                        name
                        }
                        prototype{
                        id
                        name
                        }
                        specs{
                        id
                        key
                        value
                        unit
                        name
                        required
                        fixed
                        createdAt
                        updatedAt
                        }        
                    }
                }
            }`

            let userLeases: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(userData);

            return userLeases;
        };

        let data = await leases();
        let usersData = {
            duration: 0,
            earning: 0,
            consumption: 0,
            bookings: 0
        }
        // Filter Data For Unique Users
        // Populate the users Set
        // Send the usersData in Response
        data.map(item => {
            if(item.contract && item.contract.txInfo && item.contract.txInfo.passbook){
                usersData.earning += item.item.contract.txInfo.passbook.amount;
            };
            if(item.leaseeConsumption){
                usersData.consumption += item.leaseeConsumption;
            };
            if(item.startTime && item.endTime){
                usersData.duration += moment(item.endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(item.startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"), 'minutes');
            };  
            return {
                id: item.leasee.user.id,
                firstName: item.leasee.user.firstName,
                lastName: item.leasee.user.lastName,
                email: item.leasee.user.email,
                phone: item.leasee.user.phone
            }
        })
        usersData.bookings = _.size(data);
        let responseData = {
            userData: usersData,
            data: data
        };
        response.json({ status: 200, response: responseData });
    } catch (err) {
        // Console the error
        // Send the error Response
        console.log(err)
        Logger.error(err);
        response.json({ status: 400, message: "Error Fetching Details!" })
    };
}