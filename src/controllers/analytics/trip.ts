import { Request, Response, Next } from 'express';
import { prisma } from '../../../Shared/DBManager/Prisma/prisma-client';

import * as _ from 'lodash';
import Logger from '../../utilities/logger';

import * as moment from "moment";
import Trips from '../../models/analytics/trip';
import Tripsv1 from '../../models/analytics/tripDocumentV1' 

import { trip } from '../../../Shared/DBManager/Elastic';
import { elastic } from '../../../Shared/DBManager/Elastic/elastic'
import { v4 as uuidV4 } from 'uuid';

const VEHICLE_LEVEL_2_INDEX = 'vehicle002';
const _ = require('lodash');

declare var $: any;
const searchFromElastic = async ({ query, index }) => {
    return elastic.search({
        index,
        query,
    });
};

const calculateDuration = (startTime, endTime) => {
    if (!startTime || !endTime)
        return null;
    let timeStart = new Date(startTime).getTime();
    let timeEnd = new Date(endTime).getTime();
    let hourDiff = timeEnd - timeStart; //in ms
    let secDiff = Math.floor(hourDiff/1000); //in s
    let minDiff = Math.floor(secDiff/60) ; //in minutes
    let hDiff = hourDiff / 3600 / 1000; //in hours
    let humanReadable = {
        hours: Math.floor(hDiff),
        minutes: minDiff - 60 * Math.floor(hDiff),
        seconds: secDiff - ( 3600 * Math.floor(hDiff) +  60 * minDiff - 60 * Math.floor(hDiff))
    }
    return humanReadable;
}

const tripIdsV2 = async (args) => {
    let { size, vin, skip, vins, startTime, endTime,
        _source, ranges, lastTripId } = args
    try {
        const query: any = {
            bool: {
                must: [
                    {
                        match_all: {},
                    },
                    {
                        exists: {
                          field: "tripId",
                        },
                    }
                ],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }, 
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };
        if (vin) {
            query.bool.must.push({
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            })
        }
        if (vins && vins.length) {
            let shouldMatchVins = vins.map(v => {
                return {
                    match_phrase: {
                        "vin": v
                    }
                }
            });
            query.bool.must.push({
                bool: {
                    should: shouldMatchVins,
                    minimum_should_match: 1
                }
            })
        }
        if (startTime && endTime) {
            query.bool.must.push({
                "range": {
                  "timestamp": {
                    "gte": startTime,
                    "lt": endTime
                  }
                }
              },)
        }
        if (ranges) {
            query.bool['filter'] = [
                {
                    bool: {
                        should: [...ranges]
                    }
                }
            ]
        }
        if (lastTripId) {
            query.bool.must.push({
                "range": {
                  "tripId": {
                    "lt": lastTripId,
                  }
                }
              })
        }

        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: _source || ["tripId", "vin"],
                query,
                collapse: {
                    field: "tripId"
                },
                size: size,
                from: skip || 0,
                aggs: {
                    uniq_tripId: {
                        cardinality: {
                          field: "tripId",
                        },
                    },
                },
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        let tripIds = body.length && body.map(b => b._source.tripId.toString()) || [];
        let tripCount = data.aggs && data.aggs.uniq_tripId && data.aggs.uniq_tripId.value || 0;
        return { tripIds, tripCount }
    } catch (e) {
        Logger.error(`Error fetching trip Ids: ${e.stack}`);
        return {tripIds: [], tripCount: 0}
    }
}


const tripV2 = async (args) => {
    let {        
        size,
        vin,
        vins,
        skip,
        tripIds,
    } = args;
    if (!size) {
        size = 10;
    }
    try {
        const trips: any = async () => {
            const query: any = {
                bool: {
                    must: [
                        {
                            match_all: {},
                        },
                        {
                            bool: {
                                should: [{
                                    match_phrase: {
                                        "type": 'L'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'B'
                                    }
                                }, {
                                    match_phrase: {
                                        "type": 'U'
                                    }
                                }],
                                minimum_should_match: 1
                            }
                            
                        }
                    ],
                    
                    must_not: [
                        {
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        }, 
                        {
                            match_phrase: {
                                "latitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "longitude": {
                                    query: 0
                                }
                            }
                        },
                        {
                            match_phrase: {
                                "batteryVoltageAdc": 0
                            }
                        }
                    ],
                },
            };
            tripIds.length && tripIds.map(trip => {
                query.bool.must.push({bool: {
                    should: [],
                    minimum_should_match: 1
                }})
                query.bool.must[2].bool.should.push({
                    match_phrase: {
                        "tripId": trip,
                    },
                })
            });
            if (vin) {
                query.bool.must.push({
                    match_phrase: {
                        "vin": {
                            query: vin
                        }
                    }
                })
            }
            if (vins && vins.length) {
                let should_vins = vins.map(v => {
                    return {
                        match_phrase: {
                            "vin": v
                        }
                    }
                });
                query.bool.must.push({
                    bool: {
                        should: should_vins,
                        minimum_should_match: 1
                    }
                })
            }
            let data = await searchFromElastic({
                index: VEHICLE_LEVEL_2_INDEX,
                query: {
                    _source: ["tripId", "vin", "batteryVoltageAdc", "batteryVoltage", "latitude", "longitude", "timestamp", "type", "gpsSpeed", "wheelRpm", "mode", "batteryCurrent", "batteryVoltage", "odometer"],
                    query,
                    size: "all",
                    sort: [
                        {
                            timestamp: {
                            order: "desc",
                            },
                        },
                    ],
                },
            });
            let body = data.searchData;
            let trips = tripIds.map((tripId, i) => {
                const tripLogs = body.filter(log => {
                    return log._source.tripId.toString() === tripId.toString();
                });
                let trip: any = {
                    tripId: tripId.toString(), 
                    vin: tripLogs.length && tripLogs[0]._source.vin
                };
                let batteryVoltageAdc = [];
                let minGpsSpeed = 0;
                let maxGpsSpeed = 0;
                let avgGpsSpeed = 0;
                let gpsLogsCount = 0;
                let maxWheelRPM = 0;
                let minWheelRPM = 0;
                let energy = 0;
                let mode = {
                    RIDE: 0,
                    ECONOMY: 0,
                    SPORT: 0
                }
                let modeCount = 0;
                let locations = tripLogs.length && tripLogs.map(log => {
                    log = log._source
                    // trip.vin = log.vin;
                    if (log.type === 'L') {
                        if (log.gpsSpeed && log.gpsSpeed !== 0) {
                            if (minGpsSpeed === 0 || minGpsSpeed > log.gpsSpeed) {
                                minGpsSpeed = log.gpsSpeed
                            }
                            if (maxGpsSpeed === 0 || maxGpsSpeed < log.gpsSpeed) {
                                maxGpsSpeed = log.gpsSpeed
                            }
                            avgGpsSpeed += log.gpsSpeed;
                            gpsLogsCount++;
                        }
                        return {
                            timestamp: log.timestamp,
                            lat: log.latitude,
                            lng: log.longitude
                        }
                    }
                    if (log.type === 'B') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                    }
                    if (log.type === 'U') {
                        batteryVoltageAdc.push({
                            voltage: log.batteryVoltage || log.batteryVoltageAdc,
                            timestamp: log.timestamp
                        });
                        if (log.wheelRpm && log.wheelRpm !== 0) {
                            if (minWheelRPM === 0 || minWheelRPM > log.wheelRpm) {
                                minWheelRPM = log.wheelRpm
                            }
                            if (maxWheelRPM === 0 || maxWheelRPM < log.wheelRpm) {
                                maxWheelRPM = log.wheelRpm
                            }
                        }
                        if (log.batteryCurrent && log.batteryVoltage) {
                            const power = parseFloat(log.batteryCurrent) * parseFloat(log.batteryVoltage);
                            energy = energy + power;
                        }
                        if (log.mode === 0) {
                            mode.ECONOMY += 1;
                        }
                        if (log.mode === 1) {
                            mode.RIDE += 1;
                        }
                        if (log.mode === 2) {
                            mode.SPORT += 1;
                        }
                        modeCount++;
                    }
                }) || []
                locations = _.compact(locations);
                trip.location = locations;
                trip.batteryVoltageAdc = batteryVoltageAdc;
                trip.avgGpsSpeed = avgGpsSpeed/(gpsLogsCount || 1);
                trip.maxGpsSpeed = maxGpsSpeed;
                trip.minGpsSpeed = minGpsSpeed;
                trip.maxWheelRPM = maxWheelRPM;
                trip.minWheelRPM = minWheelRPM;
                trip.energy = energy / 3600000;
                trip.mode = {
                    ECONOMY: mode.ECONOMY * 100 / modeCount,
                    RIDE: mode.RIDE * 100 / modeCount,
                    SPORT: mode.SPORT * 100 / modeCount
                }
                let distance = 0;
                let tripLogEndIndex = tripLogs.length && _.findIndex(tripLogs, l => l._source.type === 'U');
                let tripLogStartIndex = tripLogs.length && _.findLastIndex(tripLogs, l => l._source.type === 'U');
                if (tripLogStartIndex > -1 && tripLogEndIndex > -1) {
                    let endOdo = tripLogs[tripLogEndIndex]._source.odometer;
                    let startOdo = tripLogs[tripLogStartIndex]._source.odometer;
                    distance = endOdo - startOdo;
                }
                if (distance === 0) {
                    const polyline = locations;
                    for (let i = 0; i < polyline.length; i++) {
                        if ( polyline[i].lat === 0 && polyline[i].lng === 0) {
                            polyline.splice(i, 1);
                            i--;
                        }
                    }
                    if (polyline.length > 2) {
                        for (let i = 0; i < polyline.length - 1; i++) {
                            const rad = (x) => {
                                return x * Math.PI / 180;
                            };
                            const R = 6378137; // Earth’s mean radius in meter
                            const dLat = rad(polyline[i + 1].lat - polyline[i].lat);
                            const dLong = rad(polyline[i + 1].lng - polyline[i].lng);
                            const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(rad(polyline[i].lat)) * Math.cos(rad(polyline[i + 1].lat)) *
                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            const d = R * c;
                            // console.log('Distance in meters', d);
                            // returns the distance in meter
                            if(d < 1000){
                                distance = distance + d;  
                            }
                        }
                    }
                }
                trip.distance = distance;
                trip.startTime = new Date(parseInt(tripId))
                if (tripLogs.length) {
                    trip.endTime = typeof tripLogs[0]._source.timestamp === 'number' ? new Date(tripLogs[0]._source.timestamp) : tripLogs[0]._source.timestamp;
                    trip.startTime = typeof tripLogs[tripLogs.length - 1]._source.timestamp === 'number' ? new Date(tripLogs[tripLogs.length - 1]._source.timestamp) : tripLogs[tripLogs.length - 1]._source.timestamp;
                } else {
                    trip.endTime = trip.startTime;
                }
                return (trip);
            });
            return {trips};
            
        };
        try {
            let data = {};
            data = await trips();
            return data;
        } catch (err) {
            Logger.error(err.stack);
        }
    } catch (err) {
        Logger.error(err.stack);
        return {trips: []};
    }
}


export const getTripsDataTripIdV2 = async (tripId) => {
    let reqTripId = tripId;
    let status;
    let tripVin;
    if (status) {
        tripVin = await prisma.trip({ tripId: reqTripId }).vin();
        if (tripVin) {
            const vehicle = await prisma.vehicle({ vin: tripVin });
            if (!vehicle || vehicle && vehicle.status !== status) {
                return { status: 400, data: 'INVALID VIN' };
            }
        }
    }
    const query: any = {
        bool: {
            must: [{
                match_all: {},
            }, {
                match_phrase: {
                    "tripId": reqTripId
                }
            }],
            must_not: [
                {
                    match_phrase: {
                        "timestampVerified": {
                            query: false,
                        },
                    },
                },
                {
                    match_phrase: {
                        "latitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "longitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "batteryVoltageAdc": 0
                    }
                }
            ],
        },
    };

    let data = await searchFromElastic({
        index: VEHICLE_LEVEL_2_INDEX,
        query: {
            _source: [],
            query,
            size: "all",
            sort: [
                {
                    timestamp: {
                        order: "desc",
                    },
                },
            ],
        },
    });
    let body = data.searchData;
    let resp: any = {
        vin: '',
        ignition: [],
        battery: [],
        uart: [],
        location: [],
        alarm: []
    }
    body.forEach(log => {
        resp.vin = log.vin
        const type = log._source.type;
        switch (type) {
            case 'I':
                resp.ignition.push(log._source)
                break;
            case 'L':
                resp.location.push(log._source)
                break;
            case 'B':
                resp.battery.push(log._source)
                break;
            case 'A':
                resp.alarm.push(log._source)
                break;
            case 'U':
                resp.uart.push(log._source)
                break;
        }
    });
    return resp;

}

export const getDriverScore = async (req: Request, res: Response, next: Next) => {
    let vin = req.query.vin;
    try {

        const lease: any = async () => {
            // console.log('Inside Trips');
            let filter: any = {
                asset: {
                    vehicle: {
                        vin:vin
                    }
                },                               
            };
            // console.log('Here 1');
            const companyVin = `
                fragment userOnLease on Leases{
                    id
                    status
                    startTime
                    endTime
                    leasee{
                        user{
                            firstName
                            email
                            lastName
                            phone
                        }
                        }                    
                }`

            // console.log('Here 2');
            const userEmail: any = await prisma.leases({ where: filter}).$fragment(companyVin);
            // console.log('Data Received', userEmail);                   
            return userEmail;
        };

        const trips: any = async () => {
            // console.log('Inside Trips');
            let filter: any = {
                vin: vin
            };
            const tripData = `
                fragment tripsOfVin on Trips{
                    tripId                    
                }`
            const tripid: any = await prisma.trips({ where: filter }).$fragment(tripData);
            console.log('Trips Details', tripid);
            // let latestTripId = tripid[tripid.length - 1].tripId;
            let lastTrips = _.takeRight(tripid, 20);
            console.log('Data Received', lastTrips.length);
            return lastTrips;
        };
        let data = await trips();
        let user = await lease();
        console.log("Users Details Of Vin", user[user.length -1].leasee);
        let userDetails = {
            status:'',
            firstName: '',
            lastName: '',
            phone: '',
            email: ''
        }
        userDetails = {
            status: user[user.length -1].status,
            firstName: user[user.length -1].leasee.user.firstName,
            lastName: user[user.length -1].leasee.user.lastName,
            phone: user[user.length -1].leasee.user.phone,
            email: user[user.length -1].leasee.user.email
        }
        let tripsScore = [];
        
            for(let i = 0; i<= data.length -1; i++) {
                // try {
                    let tripDataJson: any = await getTripsDataTripIdV2(data[i].tripId);
                    // let tripDataJson = JSON.parse(tripDataString);
                    // console.log(tripDataJson.location);
                    //Null Check
                    if (tripDataJson.location.length > 0) {
                        console.log('Inside If');
                        let gpsSpeedData = []
                        let startTime = tripDataJson.location[0].timestamp;
                        let endTime = tripDataJson.location[tripDataJson.location.length - 1].timestamp;
                        let accelerationIndex = 0;
                        let breakingIndex = 0;
                        let deltaMili = moment(startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"));
                        let delta = moment.utc(deltaMili).format("mm");
                        console.log('Time Diff in hours', delta);
                        tripDataJson.location.forEach(item => {
                            gpsSpeedData.push(item.gpsSpeed);
                        });
                        console.log(gpsSpeedData);
                        let accIncident = 0;
                        let breakIncident = 0;
    
                        for (let i = 0; i <= gpsSpeedData.length - 1; i++) {
                            if (gpsSpeedData[i + 1] > gpsSpeedData[i]) {
                                accIncident++;
                                if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 4 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 4) {
                                    // accelerationIndex = accelerationIndex + (gpsSpeedData[i + 1] - gpsSpeedData[i]);
                                    // accelerationIndex = accelerationIndex + accIncident*1;
                                    accelerationIndex = accelerationIndex + 1;
                                } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 5 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 6) {
                                    // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 2);
                                    // accelerationIndex = accelerationIndex + accIncident*2;
                                    accelerationIndex = accelerationIndex + 2;
    
                                } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 6 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 7) {
                                    // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 3);
                                    // accelerationIndex = accelerationIndex + accIncident*3;
                                    accelerationIndex = accelerationIndex + 3;
    
                                } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 8) {
                                    // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 4);
                                    // accelerationIndex = accelerationIndex + accIncident*4;
                                    accelerationIndex = accelerationIndex + 4;
    
                                } else if ((gpsSpeedData[i + 1] - gpsSpeedData[i]) > 8) {
                                    // accelerationIndex = accelerationIndex + ((gpsSpeedData[i + 1] - gpsSpeedData[i]) * 5);
                                    // accelerationIndex = accelerationIndex + accIncident*5;
                                    accelerationIndex = accelerationIndex + 5;
    
                                }
                            } else if (gpsSpeedData[i + 1] < gpsSpeedData[i]) {
                                breakIncident++;
                                if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 5 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 7) {
                                    // breakingIndex = breakingIndex + (gpsSpeedData[i] - gpsSpeedData[i + 1]);
                                    // breakingIndex = breakingIndex + breakIncident*1;  
                                    breakingIndex = breakingIndex + 1;
    
                                } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 7 && (gpsSpeedData[i + 1] - gpsSpeedData[i]) < 9) {
                                    // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 2);
                                    // breakingIndex = breakingIndex + breakIncident*2;
                                    breakingIndex = breakingIndex + 2;
    
                                } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 9 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 11) {
                                    // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 3);
                                    // breakingIndex = breakingIndex + breakIncident*3;
                                    breakingIndex = breakingIndex + 3;
    
                                } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 11 && (gpsSpeedData[i] - gpsSpeedData[i + 1]) < 13) {
                                    // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 4);
                                    // breakingIndex = breakingIndex + breakIncident*4;
                                    breakingIndex = breakingIndex + 4;
    
                                } else if ((gpsSpeedData[i] - gpsSpeedData[i + 1]) > 13) {
                                    // breakingIndex = breakingIndex + ((gpsSpeedData[i] - gpsSpeedData[i + 1]) * 5);
                                    // breakingIndex = breakingIndex + breakIncident*5;
                                    breakingIndex = breakingIndex + 5;
    
                                }
                            }
                        }
    
                        if (delta) {
                            accelerationIndex = accelerationIndex / Number(delta);
                            breakingIndex = breakingIndex / Number(delta);
    
                            console.log('ACCELEARION INDEX', accelerationIndex)
                            console.log('Breaking Index', breakingIndex);
    
                            let averageIndex = (accelerationIndex + breakingIndex) / 2;
                            let driverScore = (100 - averageIndex).toFixed(2);
                            console.log('Driver Score Individual', driverScore);
    
                            if (driverScore != 'NaN' && driverScore != '-Infinity' && driverScore != 'Infinity') {
                                tripsScore.push(driverScore)
                            }
                        }
                        console.log('TripScore Length', tripsScore.length);
    
                    }
                // } catch (err) {
                //     console.log(err)
                // }
    
            };
        console.log('Outside ForEach of Data', tripsScore)
        

        let average = 0;

        tripsScore.forEach(item => {
            if(item != 'NaN') {            
                let items = parseFloat(item);                        
                average = average + items;
            }
        });
        console.log('Average', average);

        let averageScore = average / tripsScore.length;
        let dataScore = {
            driverScore: averageScore,
            user: userDetails,
            tripsScore
        }
        console.log('Average Score', averageScore);

        if (averageScore) {
            res.send(dataScore);
        } else {
            res.send({ data: 'No Score Available' });
        }

    } catch (e) {
        console.log('Error fetching vehicle live logs', vin, ':', e);
        res.send({ status: 500, data: "TRY AGAIN LATER" });
    }
}

export const tripByCompanyController = async (request: Request, response: Response, next: Next) => {
    /*
    Controller for getting trips of a company within date range
    params:
        companyId:  company id (required) 
        start:      start date time
        end:        end date time
    returns:
        tripData
    */
    let reqCompanyId = request.query.companyId;
    let start = request.query.start;
    let end = request.query.end;
    // let first = request.query.first;
    // let skip = request.query.skip;
    try {
        const getTrips: any = async() => {
            const getVinsQuery = `
                fragment vinsOfCompany on Company{
                    vehicles{
                        vin
                        model{
                            name
                            protocol
                            config {
                                speedLimit
                                underVoltageLimit
                                batteryMinVoltage
                            }
                        }
                        status
                        company {
                            id
                            name
                        }
                        rentalStatus
                    }
                }`
            let filter = {
                id: reqCompanyId,
            };
            const vinsOfCompany = await prisma.companies({ 
                where: filter 
            }).$fragment(getVinsQuery);
            let vinsInfoMap = new Map();
            let vins = vinsOfCompany[0].vehicles.map(item => {
                vinsInfoMap.set(item.vin, item);
                return item.vin;
            });
            const tripData = `
                fragment trips on Trips{ 
                    id
                    tripId
                    vin
                    startTime
                    endTime
                    maxGpsSpeed
                    maxWheelRPM
                    avgGpsSpeed
                    batteryVoltageAdc
                    distance
                    location{
                        lat
                        lng
                    }
                }
            `
            let trips: any = await prisma.trips({
                where: {
                    vin_in: vins,
                    // vin_in: ["AIMA_OFFICE"],
                    AND: [
                        {createdAt_lte: start},
                        {createdAt_gte: end}
                    ]
                },
                orderBy: 'updatedAt_DESC',
                // first: parseInt(first),
                // skip: parseInt(skip)
            }).$fragment(tripData);
            return {tripRawData: trips, vinsInfo: vinsInfoMap};
        };
        try {
            let {tripRawData, vinsInfo } = await getTrips();
            let tripData = tripRawData.map(item => {
                let vinsInfoObj = vinsInfo.get(item.vin);
                return {
                    company: vinsInfoObj.company.name,
                    companyId: vinsInfoObj.company.id,
                    vin: item.vin,
                    model: vinsInfoObj.model.name,
                    protocol: vinsInfoObj.model.protocol,
                    status: vinsInfoObj.status,
                    tripId: item.tripId,
                    startTime: item.startTime,
                    endTime: item.endTime,
                    tripDuration: calculateDuration(item.startTime, item.endTime),
                    maxGpsSpeed: item.maxGpsSpeed,
                    maxWheelRpm: item.maxWheelRPM,
                    avgGpsSpeed: item.avgGpsSpeed,
                    endingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[item.batteryVoltageAdc.length-1] : null,
                    startingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[0] : null,
                    distance: item.distance,
                    fromLocation: item.location ? item.location[item.location.length-1] : null,
                    toLocation: item.location ? item.location[0] : null,
                    vehicleRentalStatus: vinsInfoObj.rentalStatus,
                    vehicleConfig: vinsInfoObj.model.config
                }
            })
            response.send({status: 200, data: tripData, message: 'SUCCESS'});
        } catch (err) {
            Logger.error(err.stack);
        }        
    } catch (err) {
        Logger.error(`Error in getting all trips of a company ${err.stack}`);
        response.send({ status: 401, data: null, message: err.message});
    }
};

export const allCompanyTripsController = async (request: Request, response: Response, next: Next) => {
    /*
    Controller for getting trips of a company within date range
    params:
        companyId:  company id (required) 
        start:      start date time
        end:        end date time
    returns:
        tripData
    */
    let start = request.query.start;
    let end = request.query.end;
    let first = request.query.first;
    let skip = request.query.skip;
    try {
        const getTrips: any = async() => {
            const tripData = `
                fragment trips on Trips{ 
                    id
                    tripId
                    vin
                    startTime
                    endTime
                    maxGpsSpeed
                    maxWheelRPM
                    avgGpsSpeed
                    batteryVoltageAdc
                    distance
                    location{
                        lat
                        lng
                    }
                }
            `
            let trips: any = await prisma.trips({
                where: {
                    AND: [
                        {startTime_gt: start},
                        {endTime_lt: end}
                    ]
                },
                orderBy: 'updatedAt_DESC',
                first: parseInt(first),
                skip: parseInt(skip)
            }).$fragment(tripData);

            let vins: Set<string> = new Set()
            trips.map(trip => {
                vins.add(trip.vin)
            })
            const getVinsQuery = `
                fragment vinsOfCompany on vehicles{
                    vin
                    model{
                        name
                        protocol
                        config {
                            speedLimit
                            underVoltageLimit
                            batteryMinVoltage
                        }
                    }
                    status
                    company {
                        id
                        name
                    }
                    rentalStatus
                }`
            let vinsData = [];
            vinsData = await prisma.vehicles({
                where: {
                    vin_in: [...vins]
                },
                orderBy: "updatedAt_DESC"
            }).$fragment(getVinsQuery);
            // // console.log(vinsByCompanies.slice(0, 5));
            let vinsInfoMap = new Map();
            vinsData.map(item => {
                vinsInfoMap.set(item.vin, item);
            });
            return {tripRawData: trips, vinsInfo: vinsInfoMap};
        };
        try {
            let {tripRawData, vinsInfo } = await getTrips();
            let tripData = tripRawData.map(item => {
                let vinsInfoObj = vinsInfo.get(item.vin);
                return {
                    company: vinsInfoObj.company.name,
                    companyId: vinsInfoObj.company.id,
                    vin: item.vin,
                    model: vinsInfoObj.model.name,
                    protocol: vinsInfoObj.model.protocol,
                    status: vinsInfoObj.status,
                    tripId: item.tripId,
                    startTime: item.startTime,
                    endTime: item.endTime,
                    tripDuration: calculateDuration(item.startTime, item.endTime),
                    maxGpsSpeed: item.maxGpsSpeed,
                    maxWheelRpm: item.maxWheelRPM,
                    avgGpsSpeed: item.avgGpsSpeed,
                    endingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[item.batteryVoltageAdc.length-1] : null,
                    startingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[0] : null,
                    distance: item.distance,
                    fromLocation: item.location ? item.location[item.location.length-1] : null,
                    toLocation: item.location ? item.location[0] : null,
                    vehicleRentalStatus: vinsInfoObj.rentalStatus,
                    vehicleConfig: vinsInfoObj.model.config
                }
            })
            response.send({status: 200, data: tripData, message: 'SUCCESS'});
        } catch (err) {
            Logger.error(err.stack);
        }        
    } catch (err) {
        Logger.error(`Error in getting all trips of a company ${err.stack}`);
        response.send({ status: 401, data: null, message: err.message});
    }
};

export const tripByVinController = async (request: Request, response: Response, next: Next) => {
    /*
    Controller for getting trips of a vin within date range
    params:
        reqVin:     vin (required) 
        start:      start date time
        end:        end date time
    returns:
        tripData
    */
    let reqVin = request.query.vin;
    let start = request.query.start;
    let end = request.query.end;
    // let first = request.query.first;
    // let skip = request.query.skip;
    try {
        const getTrips: any = async() => {
            const tripData = `
                fragment trips on Trips{ 
                    id
                    tripId
                    vin
                    startTime
                    endTime
                    maxGpsSpeed
                    maxWheelRPM
                    avgGpsSpeed
                    batteryVoltageAdc
                    distance
                    location{
                        lat
                        lng
                    }
                }
            `
            let trips: any = await prisma.trips({
                where: {
                    vin: reqVin,
                    // vin_in: ["AIMA_OFFICE"],
                    AND: [
                        {createdAt_lte: start},
                        {createdAt_gte: end}
                    ]
                },
                orderBy: 'updatedAt_DESC',
                // first: parseInt(first),
                // skip: parseInt(skip)
            }).$fragment(tripData);
            return {tripRawData: trips};
        };
        try {
            let {tripRawData, vinsInfo } = await getTrips();
            let tripData = tripRawData.map(item => {
                let vinsInfoObj = vinsInfo.get(item.vin);
                return {
                    company: vinsInfoObj.company.name,
                    companyId: vinsInfoObj.company.id,
                    vin: item.vin,
                    model: vinsInfoObj.model.name,
                    protocol: vinsInfoObj.model.protocol,
                    status: vinsInfoObj.status,
                    tripId: item.tripId,
                    startTime: item.startTime,
                    endTime: item.endTime,
                    tripDuration: calculateDuration(item.startTime, item.endTime),
                    maxGpsSpeed: item.maxGpsSpeed,
                    maxWheelRpm: item.maxWheelRPM,
                    avgGpsSpeed: item.avgGpsSpeed,
                    endingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[item.batteryVoltageAdc.length-1] : null,
                    startingVoltage: item.batteryVoltageAdc ? item.batteryVoltageAdc[0] : null,
                    distance: item.distance,
                    fromLocation: item.location ? item.location[item.location.length-1] : null,
                    toLocation: item.location ? item.location[0] : null,
                    vehicleRentalStatus: vinsInfoObj.rentalStatus,
                    vehicleConfig: vinsInfoObj.model.config
                }
            })
            response.send({status: 200, data: tripData, message: 'SUCCESS'});
        } catch (err) {
            Logger.error(err.stack);
        }        
    } catch (err) {
        Logger.error(`Error in getting all trips of a company ${err.stack}`);
        response.send({ status: 401, data: null, message: err.message});
    }
};