import { Request, Response, Next } from 'express';
import { prisma } from '../../../Shared/DBManager/Prisma/prisma-client';
import * as _ from 'lodash';
import Logger from '../../utilities/logger';

import * as moment from "moment";

export const chargerController =  async (req: Request, res: Response, next: Next) => {
    // To fetch all charger booking between startTime and endTime
    Logger.info("Charger Controller called...")
    let reqCId = req.query.id;
    let from = req.query.startTime
    let to = req.query.endTime
    let filter = {};
    let master = req.query.master;
    if (reqCId === "5cfa443da7b11b00073f9657" && master === 'true') {
        filter = {
            asset: {
                type: "COMPONENT",          
            },
            AND: [
                { createdAt_lte: to },
                { createdAt_gte: from },
            ]
        }
    } else {
        filter = {
            asset: {
                type: "COMPONENT",
                component: {
                    owner: {
                        id: reqCId
                    }
                }
            },
            AND: [
                { createdAt_lte: to },
                { createdAt_gte: from },
            ]
        }
    }
  
    try {
        const chargerBookings: any = async () => {
            const chargerBookingLeaseData = `
                fragment lease on Leases {
                    id
                    asset {
                        component {
                            id
                            UID
                            specs {
                                id
                                key
                                value
                            }
                        }
                    }
                    leasor {
                        company {
                            id
                            name
                        }
                    }
                    leasee {
                        id
                        vehicle {
                            vin
                        }
                        owner {
                            vehicle
                        }
                    }
                    contract {
                        txInfo {
                            invoice {
                                passbook {
                                    amount
                                }
                            }
                        }
                    }
                    leaseeConsumption
                    createdAt
                    updatedAt
                    bookingTime
                    returnTime
                    startTime
                    endTime
                    status
                }
            `
            let chargerBookings: any = await prisma.leases({
                where: filter,
                orderBy: 'updatedAt_DESC',
            }).$fragment(chargerBookingLeaseData);
            return chargerBookings;
        };
        try {
            let data = [];
  
            data = await chargerBookings();
            const chargerData = data.map(item => {
                let bookingDuration = null;
                let chargingDuration = null;
                let amount = 0;
                let chargerType = null;
                
                if (item.startTime && item.endTime)
                    bookingDuration = moment(item.endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(item.startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"), 'minutes');
                if (item.startTime && item.endTime)
                    chargingDuration = moment(item.endTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]").diff(moment(item.startTime, "YYYY-MM-DDTHH:mm:ss.SSS[Z]"), 'minutes');
                if (item.contract && item.contract.txInfo && item.contract.txInfo.invoice && item.contract.txInfo.invoice.passbook)
                    amount = item.contract.txInfo.invoice.passbook.reduce((a, b) => a + b, 0);
                if (item.asset.component.specs){
                    const chargerInfo = item.asset.component.specs.filter(specsItem => {
                        if (specsItem.key === "usageType")
                            return true
                        return false
                    })
                    chargerType = chargerInfo[0]['value']
                }

                return {
                    chargerUID: item.asset.component.UID,
                    dateActivated: item.bookingTime,
                    bookingId: item.id,
                    startTime: item.startTime,
                    endTime: item.endTime,
                    bookingDuration,
                    chargingDuration,
                    status: item.status,
                    amount,
                    energyConsumption: item.leaseeConsumption,
                    chargerType,
                    vin: item.leasee.vehicle ? item.leasee.vehicle.vin : null
                }
            })
  
            res.send(chargerData)
        } catch (err) {
            console.log(err);
        }
    } catch (err) {
        console.log(err);
        res.send({ status: 401, data: 'UNAUTHORIZED' });
    }
}