/**
 * Required External Modules
 */

import * as dotenv from 'dotenv';
dotenv.config();
import * as express from 'express';
import * as _http from 'http';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
// import { isAuthenticated } from './auth/index';

/**
 * Routes
 */
import userRouter from './routes/user';
import chargerRouter from './routes/charger';
import vehicleRouter from './routes/vehicle';
import tripRouter from './routes/trip';
import driverRouter from './routes/driverscore';

/**
 * Analytics Routes
 */
import chargerAnalyticsRouter from './routes/analytics/charger';
import tripAnalyticsRouter from './routes/analytics/trip';

/** 
 * Discovery Routes
*/

import chargerDiscoveryRouter from './routes/discover/charger';

import Logger from './utilities/logger';
import ExceptionHandler from './utilities/decorators/exception-handler';
let mongooseURL = process.env.DB_ENDPOINT;
mongoose.connect(mongooseURL, { useNewUrlParser: true, useUnifiedTopology: true, dbName: 'trips' });
/*
* Main App Class
*/

class App {
    public express: express.Application;

    constructor() {
        Logger.info(`Initialising Datafeed server`)
        this.express = express();
        this.middleware();
        this.routes();
        this.launchConf();
        // this.kafkaConf();
        // this.fixNaNTrips();
        // this.recreateTrips();
    }

    public middleware(): void {
        Logger.info(`Initialising Datafeed server middleware`)
        this.express.set("port", process.env.PORT || 4000);
        this.express.use(cors());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(bodyParser.json());
        this.express.options('*', cors());
    }

    public routes(): void {
        Logger.info(`Setting Datafeed server routes`);
        // this.express.use(isAuthenticated);

        this.express.use(userRouter);
        this.express.use(chargerRouter);
        this.express.use(vehicleRouter);
        this.express.use(tripRouter);
        this.express.use(driverRouter);

        // Analytics Endpoints
        this.express.use(chargerAnalyticsRouter);
        this.express.use(tripAnalyticsRouter);

        // Discovery Endpoints
        this.express.use(chargerDiscoveryRouter);

        this.express.get("/logger", (req, res) => {
            Logger.error("This is an error log");
            Logger.warn("This is a warn log");
            Logger.info("This is a info log");
            Logger.http("This is a http log");
            Logger.debug("This is a debug log");
            Logger.log({
                level: "info",
                route: req.url.split("?")[0],
                function: this.routes.name,
                message: "This is an info message",
            });
            res.send("Hello world");
        });
        /* ------ Users API'S ------ */
        // this.express.get('/user/v1/vehicles', getLeaseVehicleUsers);
        // this.express.get('/user/v1/charger', getLeaseChargerUsers);
        // this.express.get('/user/v1/charger/individual', getIndividualChargerUserDetails);
        // this.express.get('/user/v1/vehicles/individual', getIndividualVehicleUserDetails);
 
        // this.express.get('/home', (req, res) => {
        //     res.send("This is from my old router")
        // })
        // this.express.get('/trips/:tripId', getTripsLogs);
        // this.express.get('/trips', tripsController.getTripsDataVin);
        // this.express.get('/trip', tripsController.getTripsDataTripId);
        // this.express.get('/tripV2', tripsController.getTripsDataTripIdV2);
        // this.express.get('/analytics', tripsController.getAllTrips);
        // this.express.get('/analytics/alltrips', tripsController.getAllTripsAnalyticsV3);
        // this.express.get('/analytics/leasedata', tripsController.getLeaseData);
        // this.express.get('/analytics/tripsdata', tripsController.getTripsDataV2);
        // this.express.get('/analytics/health', tripsController.getLeaseVehicleHealth);
        // this.express.get('/v1/vehicle/vin', tripsController.getIndividualVehicleDetailsV1);

        // /* ------ Charger API'S ------ */
        // this.express.get('/analytics/charger', tripsController.getChargerData);
        // this.express.get('/analytics/allcharger', tripsController.getAllChargerData);
        // this.express.get('/v1/charger/leases', getAllChargerLeasesV1);
        // this.express.get('/v1/charger/consumption', getChargerConsumptionV1);
        // this.express.get('/v1/charger/uid', getIndividualChargerDetailsV1);

        // this.express.get('/v2/vehicleSnapshot', vehicleController.getVehicleLiveLoaction);
        // this.express.get('/v2/vehicleLogsV2', vehicleController.getVehicleLiveLogs);
        // this.express.get('/v2/trips', tripsController.getTripsDataVinV3);
        // this.express.post('/v2/getAllRentalVehicles', vehicleController.getAllRentalVehiclesV2)
        // this.express.get('/v2/tripsrange', tripsController.getTripsDataVinV2Range);

        // this.express.get('/driverscore', getDriverScore);
        // this.express.get('/driverscoretrip', getDriverScoreTrip);

        // /* ------ Users API'S ------ */
        // this.express.get('/user/v1/vehicles', getLeaseVehicleUsers);
        // this.express.get('/user/v1/charger', getLeaseChargerUsers);
        // this.express.get('/user/v1/charger/individual', getIndividualChargerUserDetails);
        // this.express.get('/user/v1/vehicles/individual', getIndividualVehicleUserDetails);


        // /* ------ chargers analytics ------ */
        // // active chargers last month
        // this.express.get('/v3/chargersBookingsLastMonth', getAllChargerBookings);
        // this.express.get('/v3/freeUsersLastMonth', getAllFreeUsers);


        // // each charger usage
        // this.express.get('/v3/eachChargerUsage', getEachChargerUsage);
        
    }

    public launchConf() {
        Logger.info(`Launching Datafeed server on PORT ${this.express.get("port")}`)
        const http = _http.createServer(this.express)

        http.listen(this.express.get("port"), () => {
            Logger.info('Datafeed server listening on PORT: ', this.express.get("port"));
        });
    }
}


export default new App().express;

