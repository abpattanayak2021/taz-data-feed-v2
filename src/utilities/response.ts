/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Response Object for standard api responses
 *
**/

export class BaseResponse {
    statusCode: number;
    data: object;
    message: string;

    constructor(statusCode: number, data: object, message: string){
        this.statusCode = statusCode;
        this.data = data;
        this.message = message;
    }
}

export class OkResponse extends BaseResponse{

    constructor(data: object){
        super(200, data, "SUCCESS");
    }
}

export class BadResponse extends BaseResponse{

    constructor(message: string){
        super(400, null, message);
    }
}

export class CreatedResponse extends BaseResponse{

    constructor(data: object){
        super(201, data, "CREATED");
    }
}