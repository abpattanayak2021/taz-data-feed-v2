import { elastic } from '../../../Shared/DBManager/Elastic/elastic';

export const searchFromElastic = async ({ query, index }) => {
    return elastic.search({
        index,
        query,
    });
};