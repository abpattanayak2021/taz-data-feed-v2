import * as _ from "lodash";
import { elastic } from '../../../Shared/DBManager/Elastic/elastic';
import { prisma } from '../../../Shared/DBManager/Prisma/prisma-client';
import { searchFromElastic } from '../elastic_wrappers/common';
import Logger from '../logger';


const VEHICLE_LEVEL_0_INDEX = "vehicle000";

export const TripWrapper = async (args) => {
    let {        
        size,
        vin,
        skip,
        tripId,
    } = args;
    if (!size) {
        size = 10;
    }
    
    try {
        let _source = [];
        let vehicle:any = await prisma.vehicle({
            vin,
        });
        vehicle = {
            ...vehicle,
            model: await prisma.vehicle({vin}).model(),
        };
        if (vehicle.model && vehicle.model.protocol === "PNP") {
            _source = ["tripId", "latitude", "longitude", "timestamp", "batteryVoltageAdc", "gpsSpeed"];
            const query: any = {
                bool: {
                    must: [{
                        match_all: {},
                    },
                    {
                        match_phrase: {
                            "vin": {
                                query: vin,
                            }
                        },
                    },
                    {
                        match_phrase: {
                            "type": {
                                query: "ANALYTICS",
                            },
                        },
                    },
                    {
                        exists: {
                            field: "tripId",
                        },
                    }],
                    must_not: [{
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }],
                },
            };
            if (skip) {
                const aggs = {
                    uniq_tripId: {
                        terms: {
                            field: "tripId.keyword",
                            order : {
                            "_key" : "desc",
                            },
                            size: skip,
                        },
                    },
                };
                let dataAggs = await searchFromElastic({
                    index: VEHICLE_LEVEL_0_INDEX,
                    query: {
                        _source,
                        query,
                        aggs,
                        size: 1,
                        sort: [
                            {
                                timestamp: {
                                    order: "desc",
                                    unmapped_type: "boolean",
                                },
                            },
                        ],
                    },
                });
                const aggregations = dataAggs.aggregations.uniq_tripId.buckets;
                // Logger.info(`dataAggs:  ${JSON.stringify(dataAggs)}`);
                const ind = skip > 0 ? skip - 1 : 0;
                query.bool.must.push({
                    range: {
                        "tripId" : {
                            lt: aggregations[ind].key,
                        },
                    },
                });
                // Logger.info(JSON.stringify(aggregations[ind].key));
            }
            let searchData, total;
            const data = await searchFromElastic({
                index: VEHICLE_LEVEL_0_INDEX,
                query: {
                    _source,
                    query,
                    size: 10000,
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                                unmapped_type: "boolean",
                            },
                        },
                    ]
                },
            });
            searchData = data.searchData;
            // Logger.info(JSON.stringify(searchData));
            total = data.total;
            let tripsFetched = 0;
            let i = 0;
            const trips = [];
            const maxGpsSpeed = searchData[i]._source.gpsSpeed;
            const minGpsSpeed = searchData[i]._source.gpsSpeed;
            // Logger.info(`Location data: ${searchData[i]._source.latitude}`);
            const avgSpeedTotal = 0;
            const totalPositiveSpeedLog = 0;
            while (tripsFetched < size && i < searchData.length) {
                const ind = trips.findIndex((trip) => {
                    return trip.tripId === searchData[i]._source.tripId;
                });
                if (ind === -1) {
                    trips.push({
                        avgSpeedTotal: 0,
                        avgGpsSpeed: 0,
                        batteryVoltageAdc: (searchData[i]._source.batteryVoltageAdc ? [searchData[i]._source.batteryVoltageAdc] : []),
                        distance: 0,
                        // endTime: moment(searchData[i]._source.timestamp).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]"),
                        endTime: new Date(searchData[i]._source.timestamp).toISOString(),
                        energy: 0,
                        location: [{
                            lat: parseFloat(searchData[i]._source.latitude),
                            lng: parseFloat(searchData[i]._source.longitude),
                        }],                            
                        maxGpsSpeed: searchData[i]._source.gpsSpeed || 0,
                        minGpsSpeed: searchData[i]._source.gpsSpeed || 0,
                        totalPositiveSpeedLog: 0,
                        tripId: searchData[i]._source.tripId,
                        vin,

                    });
                    tripsFetched++;
                } else {
                    trips[ind].location.push({
                        lat: parseFloat(searchData[i]._source.latitude),
                        lng: parseFloat(searchData[i]._source.longitude),
                    });
                    // trips[ind].startTime = moment(searchData[i]._source.timestamp).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
                    trips[ind].startTime = new Date(searchData[i]._source.timestamp).toISOString();

                    if (searchData[i]._source.gpsSpeed && searchData[i]._source.gpsSpeed > 0) {
                        trips[ind].totalPositiveSpeedLog = trips[ind].totalPositiveSpeedLog + 1;
                        trips[ind].avgSpeedTotal = trips[ind].avgSpeedTotal + searchData[i]._source.gpsSpeed;
                        trips[ind].avgGpsSpeed = trips[ind].avgSpeedTotal / trips[ind].totalPositiveSpeedLog;
                    }
                    trips[ind].batteryVoltageAdc.push(searchData[i]._source.batteryVoltageAdc);
                    if (searchData[i]._source.gpsSpeed !== 0 &&
                        ((trips[ind].minGpsSpeed !== 0 &&
                        searchData[i]._source.gpsSpeed < trips[ind].minGpsSpeed) ||
                        trips[ind].minGpsSpeed === 0)) {
                            trips[ind].minGpsSpeed = searchData[i]._source.gpsSpeed;
                    }
                    if (searchData[i]._source.gpsSpeed !== 0 &&
                        ((trips[ind].maxGpsSpeed !== 0 &&
                        searchData[i]._source.gpsSpeed > trips[ind].maxGpsSpeed) ||
                        trips[ind].maxGpsSpeed === 0)) {
                            trips[ind].maxGpsSpeed = searchData[i]._source.gpsSpeed;
                    }
                }
                i++;
            }

            trips.map((trip, ind) => {
                let distance = 0;
                const polyline = trip.location;
                for (let i = 0; i < polyline.length; i++) {
                    if ( polyline[i].lat === 0 && polyline[i].lng === 0) {
                        polyline.splice(i, 1);
                        i--;
                    }
                }
                if (polyline.length > 2) {
                    for (let i = 0; i < polyline.length - 1; i++) {
                        const rad = (x) => {
                            return x * Math.PI / 180;
                        };
                        const R = 6378137; // Earth’s mean radius in meter
                        const dLat = rad(polyline[i + 1].lat - polyline[i].lat);
                        const dLong = rad(polyline[i + 1].lng - polyline[i].lng);
                        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                            Math.cos(rad(polyline[i].lat)) * Math.cos(rad(polyline[i + 1].lat)) *
                            Math.sin(dLong / 2) * Math.sin(dLong / 2);
                        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                        const d = R * c;
                        distance = distance + d; // returns the distance in meter
                    }
                }
                trips[ind].distance = distance;
            });
            return trips;
        }
        if (vin === "pnp_test_mohit_01") {
            return [{
                vin,
                tripId: "1",
                distance: 1,
                energy: 1.2,
                maxWheelRPM: 68,
                minWheelRPM: 1,
                mode: null,
                startTime: "2020-03-23T20:03:29.790Z",
                endTime: "2020-03-23T20:13:29.790Z",
                batteryVoltageAdc: [51.16, 51.12, 51.10, 51.06, 51.05, 51.02, 50.76].reverse(),
            }];
        }
        let type;
        if (["PNP"].indexOf(vehicle.model && vehicle.model.protocol || "") > -1) {
            type = "ANALYTICS";
        }
        let query;
        if (type) {
            query = {
                bool: {
                    must: [{
                        match_all: {},
                    },
                    {
                        match_phrase: {
                            "vin": {
                                query: vin,
                            },
                        },
                    },
                    {
                        match_phrase: {
                            "type": {
                                query: type,
                            },
                        },
                    },
                    {
                        exists: {
                            field: "tripId",
                        },
                    }],
                    must_not: [{
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }],
                },
            };
        } else {
            query = {
                bool: {
                    must: [{
                        match_all: {},
                    },
                    {
                        match_phrase: {
                            "vin": {
                                query: vin,
                            },
                        },
                    },
                    {
                        exists: {
                            field: "tripId",
                        },
                    },
                    ],
                    must_not: [{
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    },
                    {
                        match_phrase: {
                            "type": {
                                query: "ANALYTICS",
                            },
                        },
                    }],
                },
            };
        }
        let searchData, total;
        let data = await searchFromElastic({
            index: VEHICLE_LEVEL_0_INDEX,
            query: {
                _source,
                query,
                size: 10000,
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                            unmapped_type: "boolean",
                        },
                    },
                ],
            },
        });
        searchData = data.searchData;
        // total = searchData.total;
        let latestTripId = searchData[0]._source.tripId;
        if (skip) {
            latestTripId = latestTripId - skip;
        }
        if (latestTripId <= 0 ) {
            return [];
        }
        const body = [];
        let tripsFetched = 0;
        let tempTripId = latestTripId;
        while (tripsFetched < size && tempTripId !== 0) {
            const newQuery = _.cloneDeep(query);
            newQuery.bool.must.push({
                match_phrase: {
                    "tripId": {
                        query: tempTripId,
                    },
                },
            });
            data = await searchFromElastic({
                index: VEHICLE_LEVEL_0_INDEX,
                query: {
                    _source,
                    query: newQuery,
                    size: "all",
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                                unmapped_type: "boolean",
                            },
                        },
                    ],
                },
            });
            let tempSearchData, tempTotal;
            tempSearchData = data.searchData;
            tempSearchData = _.map(tempSearchData, "_source");
            tempTotal = data.total;
            if (tempTotal > 0) {
                const distance = parseFloat(tempSearchData[0].odometer) -
                    parseFloat(tempSearchData[tempTotal - 1].odometer);
                if (distance > 100) {
                    body.push(...tempSearchData);
                    tripsFetched++;
                }
            }
            tempTripId--;
        }
        const trips = {};
        let i = 0;
        let currentTripId = body[i] && body[i].tripId;
        while (i < body.length) {
            let energyConsumed = 0;
            let endTime = new Date(body[i].timestamp).toISOString();
            let startTime = endTime;
            const endOdo = parseFloat(body[i].odometer);
            let startOdo = endOdo;
            let maxWheelRPM = body[i].wheelRpm;
            let minWheelRPM = body[i].wheelRpm;
            let maxGpsSpeed = body[i].gpsSpeed;
            let minGpsSpeed = body[i].gpsSpeed;
            const avgSpeedTotal = 0;
            const totalPositiveSpeedLog = 0;
            const mode = {
                ECONOMY: 0,
                RIDE: 0,
                SPORT: 0,
            };
            const location = [];
            while (i < body.length && currentTripId === body[i].tripId) {
                const power = parseFloat(body[i].batteryCurrent) * parseFloat(body[i].batteryVoltage);
                energyConsumed = energyConsumed + power;
                startOdo = parseFloat(body[i].odometer);
                startTime = new Date(body[i].timestamp).toISOString();
                if (maxWheelRPM < body[i].wheelRpm) {
                    maxWheelRPM = body[i].wheelRpm;
                }
                if ((minWheelRPM === 0 && body[i].minWheelRPM !== 0) ||
                    (minWheelRPM > body[i].wheelRpm && body[i].wheelRpm !== 0)) {
                    minWheelRPM = body[i].wheelRpm;
                }
                if (maxGpsSpeed < body[i].gpsSpeed) {
                    maxGpsSpeed = body[i].gpsSpeed;
                }
                if ((minGpsSpeed === 0 && body[i].minGpsSpeed !== 0) ||
                    (minGpsSpeed > body[i].gpsSpeed && body[i].gpsSpeed !== 0)) {
                    minGpsSpeed = body[i].gpsSpeed;
                }
                if (body[i].mode === 0) {
                    mode.ECONOMY = mode.ECONOMY + 1;
                }
                if (body[i].mode === 1) {
                    mode.RIDE = mode.RIDE + 1;
                }
                if (body[i].mode === 2) {
                    mode.SPORT = mode.SPORT + 1;
                }
                if (body[i].latitude && body[i].longitude) {
                    location.push({lat: parseFloat(body[i].latitude), lng: parseFloat(body[i].longitude)});
                }
                i++;
            }
            const distance = (endOdo - startOdo);
            energyConsumed = energyConsumed / 3600000;
            if (trips[currentTripId]) {
                trips[currentTripId] = {
                    distance: parseFloat(trips[currentTripId].distance) + (distance || 0),
                    endTime: trips[currentTripId].endTime,
                    energy: parseFloat(trips[currentTripId].energy) + (energyConsumed || 0),
                    location: (trips[currentTripId].location.lng && trips[currentTripId].location.lat) ? [...trips[currentTripId].location, ...location] : [...location],
                    maxWheelRPM: trips[currentTripId].maxWheelRPM > maxWheelRPM ?
                        trips[currentTripId].maxWheelRPM : maxWheelRPM,
                    minWheelRPM: trips[currentTripId].minWheelRPM < minWheelRPM &&
                        trips[currentTripId].minWheelRPM !== 0 ?
                            trips[currentTripId].minWheelRPM : minWheelRPM,
                    maxGpsSpeed: trips[currentTripId].maxGpsSpeed > maxGpsSpeed ?
                        trips[currentTripId].maxGpsSpeed : maxGpsSpeed,
                    minGpsSpeed: trips[currentTripId].minGpsSpeed < minGpsSpeed &&
                        trips[currentTripId].minGpsSpeed !== 0 ?
                            trips[currentTripId].minGpsSpeed : minGpsSpeed,
                    mode: {
                        ECONOMY: trips[currentTripId].mode.ECONOMY + mode.ECONOMY,
                        RIDE: trips[currentTripId].mode.RIDE + mode.RIDE,
                        SPORT: trips[currentTripId].mode.SPORT + mode.SPORT,
                    },
                    startTime,
                    tripId: currentTripId,
                    vin: body[i - 1].vin,
                    batteryVoltageAdc: [],
                    avgGpsSpeed: avgSpeedTotal / totalPositiveSpeedLog,
                };
            } else {
                trips[currentTripId] = {
                    distance,
                    endTime,
                    energy: energyConsumed || 0,
                    location,
                    maxWheelRPM,
                    minWheelRPM,
                    maxGpsSpeed,
                    minGpsSpeed,
                    mode,
                    startTime,
                    tripId: currentTripId,
                    vin: body[i - 1].vin,
                    batteryVoltageAdc: [],
                    avgGpsSpeed: avgSpeedTotal / totalPositiveSpeedLog,
                };
            }
            if (i < body.length) {
                currentTripId = body[i].tripId;
            }
        }
        const response: any = Object.values(trips);
        response.forEach((trip: any, index) => {
            const mode: any = trip.mode || {ECONOMY: 0, RIDE: 0, SPORT: 0};
            const count = trip.location.length;
            if (count > 0) {
                mode["ECONOMY"] = mode && ((mode.ECONOMY * 100) / count).toFixed(2) || 0;
                mode["RIDE"] = mode && ((mode.RIDE * 100) / count).toFixed(2) || 0;
                mode["SPORT"] = mode && ((mode.SPORT * 100) / count).toFixed(2) || 0;
                response[index].mode = mode;
            } else {
                let total = mode.ECONOMY + mode.RIDE + mode.SPORT;
                total = total === 0 ? 1 : total;
                mode["ECONOMY"] = mode.ECONOMY * 100 / total;
                mode["RIDE"] = mode.RIDE * 100 / total;
                mode["SPORT"] = mode.SPORT * 100 / total;
                response[index].mode = mode;
            }
        });
        return response;
    } catch (err) {
        Logger.error(`error: ${err.stack}`);
        return [];
    }
};