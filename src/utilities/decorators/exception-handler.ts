
import Logger from '../logger'

const ExceptionHandler = (msg: string): any => {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        // target: class constructor or current object's prototype
        // propertyKey: name of the decorated method
        // descriptor: property descriptor of the method
        Logger.error("Decorator called !! ", msg)
    }
}

export default ExceptionHandler;