/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Logger Object for logging using Winston
 *
**/

import { Request } from 'express';

export class Log {
    level: string;
    route: string;
    function_name: string;
    message: string;

    constructor(level: string, request: Request, function_name: string, message: string){
        this.level = level;
        this.route = request.path;
        this.function_name = function_name;
        this.message = message;
    }
}