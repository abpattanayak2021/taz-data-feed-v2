/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author ishaan
 * @description app configs
 *
 */

// export const appConfig = {
//     aws: {
//         accessKeyId: 'AKIAIB7I6E7EUXB2MD2Q',
//         region: 'ap-south-1',
//         accessKey: 'Su9NzaHmy8H4vFmvYoj1CEUQZVvka9mIVnWs5vSI',
//     },
//     elastic: {
//         uri: 'https://search-rev-elastic-pavbwqniud3xfj2ufsvvrkiive.ap-south-1.es.amazonaws.com',
//     },
//     redis: {
//         enable: false,
//         uri: '',
//         port: 6379
//     },
//     tcp: {
//         port: 8000,
//     }
// }

export const appConfig = {
    aws: {
        accessKey: "Su9NzaHmy8H4vFmvYoj1CEUQZVvka9mIVnWs5vSI",
        accessKeyId: "AKIAIB7I6E7EUXB2MD2Q",
        region: "ap-south-1",
    },
    elastic: {
        uri: "https://search-rev-elastic-pavbwqniud3xfj2ufsvvrkiive.ap-south-1.es.amazonaws.com",
    },
    messageBroker: {
        uri: "https://webhook.revos.in",
    },
    redis: {
        enable: false,
        port: 6379,
        uri: "localhost",
    },
    tcp: {
        port: 8000,
    },
};
