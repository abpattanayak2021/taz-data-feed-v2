/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Saurabh Kr. Gunjan
 * @description Vehicle API'S
 *
**/

import { Router } from 'express';
import * as _ from 'lodash';

import { vehicleSnapshotController, vehicleLogsController,
    vehicleRentalController, individualVehicleController } from '../controllers/vehicle';


const vehicleRouter = Router();

vehicleRouter.get('/vehicle/snapshot', vehicleSnapshotController);
vehicleRouter.get('/vehicle/logs', vehicleLogsController);
// vehicleRouter.get('/vehicle/rentalAll', vehicleRentalController); // implement properly
vehicleRouter.get('/vehicle/vin', individualVehicleController);

export default vehicleRouter;