/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Saurabh Kr. Gunjan
 * @description User API'S
 *
**/

import { Router } from 'express';
import * as _ from 'lodash';

import { userVehicleController, userIndividualVehicleController, 
    userChargerController, userIndividualChargerController } from '../controllers/user'

const userRouter = Router();

userRouter.get('/user/vehicle', userVehicleController)
userRouter.get('/user/vehicle/individual', userIndividualVehicleController)

userRouter.get('/user/charger', userChargerController)
userRouter.get('/user/charger/individual', userIndividualChargerController)

export default userRouter