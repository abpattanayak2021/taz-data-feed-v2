/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Saurabh Kr. Gunjan
 * @description Charger API'S
 *
**/

import { Router } from 'express';
import * as _ from 'lodash';

import { tripsV2Controller, tripsV2RangeController, tripsController, 
    tripsIdController, tripsElasticController, tripsAnalyticsController,
    tripsAnalyticsAllController, tripsAnalyticsLeaseController, tripsAnalyticsV2Controller,
    tripsAnalyticsLeaseHealthController, tripsControllerDashboard
} from '../controllers/trip';


const tripsRouter = Router();

tripsRouter.get('/trip/v2', tripsV2Controller);
tripsRouter.get('/trip/dashboard', tripsControllerDashboard);
tripsRouter.get('/trip/v2/range', tripsV2RangeController);
tripsRouter.get('/trip/', tripsController);
tripsRouter.get('/trip/id', tripsIdController);
tripsRouter.get('/trip/elastic', tripsElasticController);
tripsRouter.get('/trip/analytics', tripsAnalyticsController);
tripsRouter.get('/trip/analytics/all', tripsAnalyticsAllController);
tripsRouter.get('/trip/analytics/leasedata', tripsAnalyticsLeaseController);
tripsRouter.get('/trip/analytics/tripsdata', tripsAnalyticsV2Controller);

export default tripsRouter;