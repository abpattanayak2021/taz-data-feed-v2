/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Saurabh Kr. Gunjan
 * @description Charger API'S
 *
**/

import { Router } from 'express';
import * as _ from 'lodash';

import { chargerAnalyticsController, allChargerAnalyticsController,
    allChargerLeasesController, chargerConsumptionController,
    chargerIndividualDetailsController } from '../controllers/charger'


const chargerRouter = Router();

chargerRouter.get('/charger/analytics', chargerAnalyticsController) // lease wise data
chargerRouter.get('/charger/analytics/all', allChargerAnalyticsController) // component wise data
chargerRouter.get('/charger/leases', allChargerLeasesController)
chargerRouter.get('/charger/consumption', chargerConsumptionController)
chargerRouter.get('/charger/individual', chargerIndividualDetailsController)


export default chargerRouter