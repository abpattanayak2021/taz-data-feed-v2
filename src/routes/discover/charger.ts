/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Charger API'S for Discoverability Functionality
 *
**/

import { Router } from 'express';
import { chargerController } from '../../controllers/discover/charger';



const chargerDiscoveryRouter = Router();

chargerDiscoveryRouter.get('/discover/charger', chargerController);


export default chargerDiscoveryRouter;