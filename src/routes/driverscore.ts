/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Saurabh Kr. Gunjan
 * @description Driver Score API'S
 *
**/

import { Router } from 'express';
import * as _ from 'lodash';

import { driverScoreController, driverScoreTripController } from '../controllers/driverscore';


const driverscoreRouter = Router();

driverscoreRouter.get('/driverscore/', driverScoreController);
driverscoreRouter.get('/driverscore/trip', driverScoreTripController);
// driverscoreRouter.get('/vehicle/logs', vehicleLogsController);

export default driverscoreRouter;