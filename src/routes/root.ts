import { NextFunction, Request, Response, Router } from "express";

// import * as homeController from '../controllers/homeController';

class Root {
  public router: Router;
  public constructor() {
    this.router = Router();
    this.init();
  }
  private init() {
    this.router.get("/home", homeController.index);
    
  }
}

const rootRoutes = new Root();
export default rootRoutes.router;