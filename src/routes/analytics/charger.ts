/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Charger API'S for Analytics Server
 *
**/

import { Router } from 'express';
import { chargerController } from '../../controllers/analytics/charger';



const chargerAnalyticsRouter = Router();

chargerAnalyticsRouter.get('/charger', chargerController);


export default chargerAnalyticsRouter;