/**
 * Copyright (C) 2021 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author Abhishekh Pattanayak
 * @description Trips API'S for Analytics Server 
 *
**/

import { Router } from 'express';
import { tripByCompanyController, allCompanyTripsController, getDriverScore } from '../../controllers/analytics/trip'



const tripAnalyticsRouter = Router();

tripAnalyticsRouter.get('/analytics/trip', tripByCompanyController);
tripAnalyticsRouter.get('/analytics/trip/all', allCompanyTripsController);

tripAnalyticsRouter.get('/analytics/tripDriverScore', getDriverScore);


export default tripAnalyticsRouter;