import * as _ from 'lodash';
import * as moment from "moment";
import { elastic } from './elastic';
const VEHICLE_LEVEL_0_INDEX = 'vehicle000';
const VEHICLE_LEVEL_2_INDEX = 'vehicle002';
import { prisma } from '../Prisma/prisma-client/';
import Trips from '../../../src/models/tripDocument';
const searchFromElastic = async ({query, index}) => {
    return elastic.search({
        index,
        query,
    });
};

const createQuery = (type, vin, startTime, endTime, tripIdExists) => {
    let query: any = {
        bool: {
            must: [{
                match_all: {},
            },
            {
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            },
            {
                range: {
                    timestamp: {
                        gte: startTime,
                        lte: endTime,
                    },
                },
            }],
            must_not: [{
                match_phrase: {
                    "timestampVerified": {
                        query: false,
                    },
                },
            }],
        },
    };
    if (type) {
        query.bool.must.push({
            match_phrase: {
                "type": {
                    query: type,
                },
            },
        });
    } else {
        query.bool.must_not.push({
            match_phrase: {
                "type": {
                    query: "ANALYTICS",
                },
            },
        })
    }
    if (tripIdExists) {
        query.bool.must.push({
            exists: {
              field: "tripId"
            }
        });
    }
    return query;
}

const createQueryV2 = (type, vin, startTime, endTime, isSmart) => {
    const query: any = {
        bool: {
            must: [{
                match_all: {},
            },
            {
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            }],
            must_not: [
                {
                    match_phrase: {
                        "timestampVerified": {
                            query: false,
                        },
                    },
                }, 
                {
                    match_phrase: {
                        "latitude": {
                            query: 0
                        }
                    }
                },
                {
                    match_phrase: {
                        "longitude": {
                            query: 0
                        }
                    }
                }
            ],
        },
    };
    if (!isSmart) {
        query.bool.must_not.push({
            match_phrase: {
                "batteryVoltageAdc": 0
            }
        });
    }
    if (startTime && endTime) {
        console.log("startendtime", startTime, endTime, parseInt(startTime));
        query.bool.must.push({
            range: {
                timestamp: {
                    gte: startTime,
                    lte: endTime,
                },
            },
        });
    }
    if (Array.isArray(type)) {
        query.bool.must.push({
            bool: {
                should: [],
                minimum_should_match: 1
            }
        })
        type.forEach((t) => {
            query.bool.must[2].bool.should.push({
                match_phrase: {
                    "type": t,
                },
            });
        });
    } else if (type) {
        query.bool.must.push({
            match_phrase: {
                "type": {
                    query: type,
                },
            },
        });
    } 
    // else {
    //     query.bool.must_not.push({
    //         match_phrase: {
    //             "type": {
    //                 query: "ANALYTICS",
    //             },
    //         },
    //     });
    // }
    return query;
};

export const searchVehicleLogsV2 = async (args) => {
    let {
        vin,
        tripId,
        startTime,
        endTime,
    } = args;
    try {
        let vinRide;
        let endTimeCurrent;
        let startTimeCurrent;
        if (tripId) {
            let currentRide: any = await prisma.ride({id: tripId});
            currentRide = {
                ...currentRide,
                vehicle: await prisma.ride({id: tripId}).vehicle(),
            };
            vinRide = currentRide.vehicle.vin;
            startTimeCurrent = currentRide.startTime ? Date.parse(currentRide.startTime) : Date.now();
            endTimeCurrent = currentRide.endTime ? Date.parse(currentRide.endTime) : Date.now();
        }
        if (vinRide) {
            vin = vinRide;
        }
        let isSmart = false;
        let vehicle: any = await prisma.vehicle({vin});
        if (!vehicle) {
            throw new Error('INVALID VIN');
        }
        vehicle = {
            ...vehicle,
            model: await prisma.vehicle({vin}).model(),
        }
        
        if (vehicle.model && ["SMART", "SMART_PLUS"].includes(vehicle.model.protocol)) {
            isSmart = true;
        }
        if (!endTime && !endTimeCurrent) {
            endTime = Date.now();
        }
        if (!startTime && !startTimeCurrent) {
            const d = new Date();
            d.setDate(d.getDate() - 7);
            startTime = Date.parse(d.toString());
        }
        let _source = [];
        let query = createQueryV2(null, vin || vinRide, startTime || startTimeCurrent, endTime || endTimeCurrent, isSmart);
        let data = await searchFromElastic({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source,
                query,
                size: "all",
                sort: [
                    {
                      timestamp: {
                        order: "desc",
                      },
                    },
                  ],
            },
        });
        let body = data.searchData;
        if (data.total === 0) {
            query = createQueryV2(null, vin || vinRide, null, null, isSmart);
            data = await elastic.searchWithoutScroll({
                index: VEHICLE_LEVEL_2_INDEX,
                query: {
                    _source,
                    query,
                    collapse: {
                        field: "type.keyword"
                    },
                    size: 10,
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                            },
                        },
                    ],
                },
            });
            body = data.searchData;
            // console.log('body', body);
        }
        let resp: any = {
            vin: vin || vinRide,
            ignition: [],
            battery: [],
            uart: [],
            location: [],
            alarm: []
        }
        body.forEach(log => {
            const type = log._source.type;
            if (typeof log._source.timestamp === 'number') {
                log._source.timestamp = new Date(log._source.timestamp);
            }
            switch(type) {
                case 'I':
                    resp.ignition.push(log._source)
                    break;
                case 'L':
                    resp.location.push(log._source)
                    break;
                case 'B':
                    resp.battery.push(log._source)
                    break;
                case 'A':
                    resp.alarm.push(log._source)
                    break;
                case 'U':
                    resp.uart.push(log._source)
                    break;
            }
        });
        return resp;
    } catch (e) {
        console.log("Error fetching logs", e);
        throw new Error("Error fetching logs");
    }
    return;
}

export const searchVehicleSnapshotV2 = async (args) => {
    const {
        vin
    } = args;
    try {
        let _source = [];
        console.log('VIN', vin);
        let vehicle: any = await prisma.vehicle({
            vin
        });
        vehicle = {
            ...vehicle,
            model: await prisma.vehicle({vin}).model()
        }
        if (!vehicle) {
            console.log('INVALID VIN', vin);
            throw new Error("INVAID VIN");
        }
        let isSmart = false;
        if (vehicle.model && ["SMART", "SMART_PLUS"].includes(vehicle.model.protocol)) {
            isSmart = true;
        }
        let query = createQueryV2(null, vin, null, null, isSmart);
        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source,
                query,
                collapse: {
                    field: "type.keyword"
                },
                size: 10,
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        let resp: any = {
            vin: vin,
            ignition: [],
            battery: [],
            uart: [],
            location: [],
            alarm: []
        }
        body.forEach(log => {
            const type = log._source.type;
            if (typeof log._source.timestamp === 'number') {
                log._source.timestamp = new Date(log._source.timestamp);
            }
            switch(type) {
                case 'I':
                    resp.ignition.push(log._source)
                    break;
                case 'L':
                    resp.location.push(log._source)
                    break;
                case 'B':
                    resp.battery.push(log._source)
                    break;
                case 'A':
                    resp.alarm.push(log._source)
                    break;
                case 'U':
                    resp.uart.push(log._source)
                    break;
            }
        });
        let finalResp = {
            vin,
            ignition: resp.ignition.length && resp.ignition[0] || {},
            battery: resp.battery.length && resp.battery[0] || {},
            uart: resp.uart.length && resp.uart[0] || {},
            location: resp.location.length && resp.location[0] || {},
            alarm: resp.alarm.length && resp.alarm[0] || {},
        }
        return finalResp;
    } catch (e) {
        console.log("Error fetching logs", e);
        throw new Error("Error fetching logs");
    }
}

const searchVehicleLogs = async (args) => {
    let {
        size,
        vin,
        startTime,
        endTime,
    } = args;
    try {
        let _source = [];

        // TODO save protocol in vehicle obj redis
        const protocol = await prisma.vehicle({
            vin,
        }).model().protocol();
        
        let type;
        if (["PNP", "SMART_PLUS", "VIEW_PLUS"].indexOf(protocol) > -1) {
            type = "ANALYTICS";
        }
        if (startTime) {
            startTime = startTime.toString();
            if (endTime) {
                endTime = endTime.toString();
            } else {
                endTime = Date.now().toString();
            }
        }
        let query = createQuery(type, vin, startTime, endTime, false);

        let data = await searchFromElastic({
            index: VEHICLE_LEVEL_0_INDEX,
            query: {
                _source,
                query,
                size: size || 10,
            },
        });
        data = _.sortBy(_.map(data, '_source'), 'timestamp');
        return data;
    } catch (err) {
        console.log('err searchVehicleLogs:', err);
        return [];
    }
};

const getTripLogs = async ({
    fields = 'latitude, longitude',    
    tripId,
}) => {
    // const user = await context.user;
        try {
            let _source = [];
            if (fields) {
                _source = fields.split(',');
            }
            let query: any = {
                bool: {
                    must: [
                    {
                        match_phrase: {
                            'tripId': {
                                query: tripId,
                            },
                        },
                    }, {
                        match_phrase: {
                            'type': {
                                query: 'ANALYTICS',
                            },
                        },
                    } ],
                    must_not: [{
                        match_phrase: {
                            'timestampVerified': {
                                query: false,
                            },
                        },
                    }],
                },
            };
            const searchData = await searchFromElastic({
                index: VEHICLE_LEVEL_0_INDEX,
                query: {
                    _source,
                    query,
                    size: 10000,
                },
            });
            let body = searchData;
            body = _.map(body, '_source');
            return _.sortBy(body, 'timestamp');
            // if (body && body[0].riderId) {
            //     const rider = await context.prisma.user({id: body[0].riderId});
            //     body[0].riderId = rider.firstName + " " + rider.lastName;
            // }
            // if (body && body[0].manufacturerId) {
            //     const manuafacturer = await context.prisma.company({id: body[0].manufacturerId});
            //     body[0].manufacturerId = manuafacturer.name;
            // }
            // const permission = await context.prisma.trackingPermissions({
            //     where: {
            //         accessors_some: {
            //             id: context.user.company.id,
            //         },
            //         endTime_gte: new Date(),
            //         status: "GRANTED",
            //         vehicle: {
            //             vin,
            //         },
            //     },
            // });
            // if (!(permission && permission.length > 0)) {
            //     body = _.map(body, (log) => {
            //         return {
            //             ...log,
            //             latitude: 401,
            //             longitude: 401,
            //         };
            //     });
            // }
            return body;
        } catch (err) {
            console.log('err getTripLogs:', err);
            return err;
        }
};

const getTripData = async (    
    tripId
) => {
    // const user = await context.user;
        try {
            let _source = [];
            console.log('Trip Id from params', tripId);            
            let query: any = {
                bool: {
                    must: [
                    {
                        match_phrase: {
                            'tripId': {
                                query: tripId,
                            },
                        },
                    }],
                    must_not: [{
                        match_phrase: {
                            'timestampVerified': {
                                query: false,
                            },
                        },
                    }],
                },
            };
            console.log("Hello");
            const searchData = await searchFromElastic({
                index: VEHICLE_LEVEL_0_INDEX,
                query: {
                    _source,
                    query,
                    size: 10000,
                },
            });
            let body = searchData;
            
            body = _.map(body, '_source');
            let body1 =  _.sortBy(body, 'timestamp');            
            return body1;
        } catch (err) {
            console.log('err getTripData:', err);
            return err;
        }
};

const tripIdsV2 = async (args) => {
    let {        
        size,
        vin,
        skip,
        vins,
        startTime,
        endTime,
        _source,
        ranges,
        lastTripId
    } = args;
    try {
        const query: any = {
            bool: {
                must: [
                    {
                        match_all: {},
                    },
                    // {
                    //     match_phrase: {
                    //         "vin": {
                    //             query: vin,
                    //         },
                    //     },
                    // },
                    {
                        exists: {
                          field: "tripId",
                        },
                    }
                ],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }, 
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };
        if (vin) {
            console.log('Inside VIN')
            query.bool.must.push({
                match_phrase: {
                    "vin": {
                        query: vin,
                    },
                },
            })
        }
        if (vins && vins.length) {
            let shouldMatchVins = [];
            vins.forEach(v => {
                shouldMatchVins.push({
                    match_phrase: {
                        "vin": v
                    }
                })
            });
            query.bool.must.push({
                bool: {
                    should: shouldMatchVins,
                    minimum_should_match: 1
                }
            })
        }
        if (startTime && endTime) {
            console.log('Inside Start And End Time')
            query.bool.must.push({
                "range": {
                  "timestamp": {
                    "gte": startTime,
                    "lt": endTime
                  }
                }
              },)
        }
        if (ranges) {
            query.bool['filter'] = [
                {
                    bool: {
                        should: [...ranges]
                    }
                }
            ]
        }
        if (lastTripId) {
            query.bool.must.push({
                "range": {
                  "tripId": {
                    "lt": lastTripId,
                  }
                }
              })
        }
        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: _source || ["tripId", "vin"],
                query,
                collapse: {
                    field: "tripId"
                },
                size: size,
                from: skip || 0,
                aggs: {
                    uniq_tripId: {
                        cardinality: {
                          field: "tripId",
                        },
                    },
                },
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        // console.log('tripIdsV2 body VIN: ', JSON.stringify(data));
        let tripIds = body.length && body.map(b => b._source.tripId.toString()) || [];
        let tripCount = data.aggs && data.aggs.uniq_tripId && data.aggs.uniq_tripId.value || 0;
        return {tripIds, tripCount}
    } catch (e) {
        console.log('Error fetching trip Ids: ', e);
        return {tripIds: [], tripCount: 0}
    }
}

const tripV2Logs = async (args) => {
    let {        
        size,
        vin,
        vins,
        skip,
        tripIds,
    } = args;
    if (!size) {
        size = 10;
    }
    // if (user.role && user.role !== "UNAUTHORISED") {
        try {
            const trips: any = async () => {
                let trips = [];
                const query: any = {
                    bool: {
                        must: [
                            {
                                match_all: {},
                            },
                            {
                                bool: {
                                    should: [{
                                        match_phrase: {
                                            "type": 'L'
                                        }
                                    }, {
                                        match_phrase: {
                                            "type": 'B'
                                        }
                                    }, {
                                        match_phrase: {
                                            "type": 'U'
                                        }
                                    }],
                                    minimum_should_match: 1
                                }
                                
                            }
                        ],
                        
                        must_not: [
                            {
                                match_phrase: {
                                    "timestampVerified": {
                                        query: false,
                                    },
                                },
                            }, 
                            {
                                match_phrase: {
                                    "latitude": {
                                        query: 0
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    "longitude": {
                                        query: 0
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    "batteryVoltageAdc": 0
                                }
                            }
                        ],
                    },
                };
                tripIds.length && tripIds.forEach(trip => {
                    query.bool.must.push({bool: {
                        should: [],
                        minimum_should_match: 1
                    }})
                    query.bool.must[2].bool.should.push({
                        match_phrase: {
                            "tripId": trip,
                        },
                    })
                });
                if (vin) {
                    query.bool.must.push({
                        match_phrase: {
                            "vin": {
                                query: vin
                            }
                        }
                    })
                }
                if (vins && vins.length) {
                    let should_vins = vins.map(v => {
                        return {
                            match_phrase: {
                                "vin": v
                            }
                        }
                    });
                    query.bool.must.push({
                        bool: {
                            should: should_vins,
                            minimum_should_match: 1
                        }
                    })
                }
                let data = await searchFromElastic({
                    index: VEHICLE_LEVEL_2_INDEX,
                    query: {
                        _source: ["tripId", "vin", "batteryVoltageAdc", "batteryVoltage", "latitude", "longitude", "timestamp", "type", "batteryVoltage"],
                        query,
                        size: "all",
                        sort: [
                            {
                              timestamp: {
                                order: "desc",
                              },
                            },
                          ],
                    },
                });
                let body = data.searchData;
                // console.log('body', body, tripIds);
                tripIds.forEach((tripId, i) => {
                    
                    const tripLogs = body.filter(log => {
                        return log._source.tripId.toString() === tripId.toString();
                    });
                    let trip: any = {tripId: tripId.toString(), vin: tripLogs.length && tripLogs[0]._source.vin};
                    let batteryVoltageAdc = [];
                    let locations = tripLogs.length && tripLogs.map(log => {
                        log = log._source
                        if (log.type === 'L') {
                            return {
                                timestamp: log.timestamp,
                                lat: log.latitude,
                                lng: log.longitude
                            }
                        }
                        if (log.type === 'B') {
                            batteryVoltageAdc.push({
                                voltage: log.batteryVoltageAdc,
                                timestamp: log.timestamp
                            });
                        }
                        if (log.type = 'U') {
                            batteryVoltageAdc.push({
                                voltage: log.batteryVoltage || log.batteryVoltageAdc,
                                timestamp: log.timestamp
                            });
                        }
                    }) || []
                    locations = _.compact(locations);
                    trip.location = locations;
                    trip.batteryVoltageAdc = batteryVoltageAdc;
                    trips.push(trip);
                });
                // res.send(resp);

                return {trips};
                
            };
            try {
                let data = {};
                data = await trips();
                return data;
            } catch (err) {
                console.log(err);
            }
        } catch (err) {
            console.log("error", err);
            return {trips: []};
        }

}


const tripV2 = async (args) => {
    let {        
        size,
        vin,
        vins,
        skip,
        tripIds,
    } = args;
    if (!size) {
        size = 10;
    }
    // if (user.role && user.role !== "UNAUTHORISED") {
        try {
            const trips: any = async () => {
                let trips = [];
                const query: any = {
                    bool: {
                        must: [
                            {
                                match_all: {},
                            },
                            {
                                bool: {
                                    should: [{
                                        match_phrase: {
                                            "type": 'L'
                                        }
                                    }, {
                                        match_phrase: {
                                            "type": 'B'
                                        }
                                    }, {
                                        match_phrase: {
                                            "type": 'U'
                                        }
                                    }],
                                    minimum_should_match: 1
                                }
                                
                            }
                        ],
                        
                        must_not: [
                            {
                                match_phrase: {
                                    "timestampVerified": {
                                        query: false,
                                    },
                                },
                            }, 
                            {
                                match_phrase: {
                                    "latitude": {
                                        query: 0
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    "longitude": {
                                        query: 0
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    "batteryVoltageAdc": 0
                                }
                            }
                        ],
                    },
                };
                tripIds.length && tripIds.forEach(trip => {
                    query.bool.must.push({bool: {
                        should: [],
                        minimum_should_match: 1
                    }})
                    query.bool.must[2].bool.should.push({
                        match_phrase: {
                            "tripId": trip,
                        },
                    })
                });
                if (vin) {
                    query.bool.must.push({
                        match_phrase: {
                            "vin": {
                                query: vin
                            }
                        }
                    })
                }
                if (vins && vins.length) {
                    let should_vins = vins.map(v => {
                        return {
                            match_phrase: {
                                "vin": v
                            }
                        }
                    });
                    query.bool.must.push({
                        bool: {
                            should: should_vins,
                            minimum_should_match: 1
                        }
                    })
                }
                let data = await searchFromElastic({
                    index: VEHICLE_LEVEL_2_INDEX,
                    query: {
                        _source: ["tripId", "vin", "batteryVoltageAdc", "batteryVoltage", "latitude", "longitude", "timestamp", "type", "gpsSpeed", "wheelRpm", "mode", "batteryCurrent", "batteryVoltage", "odometer"],
                        query,
                        size: "all",
                        sort: [
                            {
                              timestamp: {
                                order: "desc",
                              },
                            },
                          ],
                    },
                });
                let body = data.searchData;
                // console.log('body', body, tripIds);
                tripIds.forEach((tripId, i) => {
                    const tripLogs = body.filter(log => {
                        return log._source.tripId.toString() === tripId.toString();
                    });
                    let trip: any = {tripId: tripId.toString(), vin: tripLogs.length && tripLogs[0]._source.vin};
                    let batteryVoltageAdc = [];
                    let minGpsSpeed = 0;
                    let maxGpsSpeed = 0;
                    let avgGpsSpeed = 0;
                    let gpsLogsCount = 0;
                    let maxWheelRPM = 0;
                    let minWheelRPM = 0;
                    let energy = 0;
                    let mode = {
                        RIDE: 0,
                        ECONOMY: 0,
                        SPORT: 0
                    }
                    let modeCount = 0;
                    let locations = tripLogs.length && tripLogs.map(log => {
                        log = log._source
                        // trip.vin = log.vin;
                        if (log.type === 'L') {
                            if (log.gpsSpeed && log.gpsSpeed !== 0) {
                                if (minGpsSpeed === 0 || minGpsSpeed > log.gpsSpeed) {
                                    minGpsSpeed = log.gpsSpeed
                                }
                                if (maxGpsSpeed === 0 || maxGpsSpeed < log.gpsSpeed) {
                                    maxGpsSpeed = log.gpsSpeed
                                }
                                avgGpsSpeed += log.gpsSpeed;
                                gpsLogsCount++;
                            }
                            return {
                                timestamp: log.timestamp,
                                lat: log.latitude,
                                lng: log.longitude
                            }
                        }
                        if (log.type === 'B') {
                            batteryVoltageAdc.push({
                                voltage: log.batteryVoltageAdc,
                                timestamp: log.timestamp
                            });
                        }
                        if (log.type === 'U') {
                            batteryVoltageAdc.push({
                                voltage: log.batteryVoltage || log.batteryVoltageAdc,
                                timestamp: log.timestamp
                            });
                            if (log.wheelRpm && log.wheelRpm !== 0) {
                                if (minWheelRPM === 0 || minWheelRPM > log.wheelRpm) {
                                    minWheelRPM = log.wheelRpm
                                }
                                if (maxWheelRPM === 0 || maxWheelRPM < log.wheelRpm) {
                                    maxWheelRPM = log.wheelRpm
                                }
                            }
                            if (log.batteryCurrent && log.batteryVoltage) {
                                const power = parseFloat(log.batteryCurrent) * parseFloat(log.batteryVoltage);
                                energy = energy + power;
                            }
                            if (log.mode === 0) {
                                mode.ECONOMY += 1;
                            }
                            if (log.mode === 1) {
                                mode.RIDE += 1;
                            }
                            if (log.mode === 2) {
                                mode.SPORT += 1;
                            }
                            modeCount++;
                        }
                    }) || []
                    locations = _.compact(locations);
                    trip.location = locations;
                    trip.batteryVoltageAdc = batteryVoltageAdc;
                    trip.avgGpsSpeed = avgGpsSpeed/(gpsLogsCount || 1);
                    trip.maxGpsSpeed = maxGpsSpeed;
                    trip.minGpsSpeed = minGpsSpeed;
                    trip.maxWheelRPM = maxWheelRPM;
                    trip.minWheelRPM = minWheelRPM;
                    trip.energy = energy / 3600000;
                    trip.mode = {
                        ECONOMY: mode.ECONOMY * 100 / modeCount,
                        RIDE: mode.RIDE * 100 / modeCount,
                        SPORT: mode.SPORT * 100 / modeCount
                    }
                    let distance = 0;
                    let tripLogEndIndex = tripLogs.length && _.findIndex(tripLogs, l => l._source.type === 'U');
                    let tripLogStartIndex = tripLogs.length && _.findLastIndex(tripLogs, l => l._source.type === 'U');
                    if (tripLogStartIndex > -1 && tripLogEndIndex > -1) {
                        let endOdo = tripLogs[tripLogEndIndex]._source.odometer;
                        let startOdo = tripLogs[tripLogStartIndex]._source.odometer;
                        distance = endOdo - startOdo;
                    }
                    if (distance === 0) {
                        const polyline = locations;
                        for (let i = 0; i < polyline.length; i++) {
                            if ( polyline[i].lat === 0 && polyline[i].lng === 0) {
                                polyline.splice(i, 1);
                                i--;
                            }
                        }
                        if (polyline.length > 2) {
                            for (let i = 0; i < polyline.length - 1; i++) {
                                const rad = (x) => {
                                    return x * Math.PI / 180;
                                };
                                const R = 6378137; // Earth’s mean radius in meter
                                const dLat = rad(polyline[i + 1].lat - polyline[i].lat);
                                const dLong = rad(polyline[i + 1].lng - polyline[i].lng);
                                const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                    Math.cos(rad(polyline[i].lat)) * Math.cos(rad(polyline[i + 1].lat)) *
                                    Math.sin(dLong / 2) * Math.sin(dLong / 2);
                                const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                                const d = R * c;
                                // console.log('Distance in meters', d);
                                // returns the distance in meter
                                if(d < 1000){
                                    distance = distance + d;  
                                }
                            }
                        }
                    }
                    trip.distance = distance;
                    trip.startTime = new Date(parseInt(tripId))
                    if (tripLogs.length) {
                        // console.log('tripLogs', tripLogs[0]);
                        trip.endTime = typeof tripLogs[0]._source.timestamp === 'number' ? new Date(tripLogs[0]._source.timestamp) : tripLogs[0]._source.timestamp;
                        trip.startTime = typeof tripLogs[tripLogs.length - 1]._source.timestamp === 'number' ? new Date(tripLogs[tripLogs.length - 1]._source.timestamp) : tripLogs[tripLogs.length - 1]._source.timestamp;
                    } else {
                        trip.endTime = trip.startTime;
                    }
                    trips.push(trip);
                });
                // res.send(resp);

                return {trips};
                
            };
            try {
                let data = {};
                data = await trips();
                return data;
            } catch (err) {
                console.log(err);
            }
        } catch (err) {
            console.log("error", err);
            return {trips: []};
        }

}

const trip = async (args) => {
    // const user = await context.user;
    // let latestInDb = Trips.findById(args.vin, function (err, doc) {

    // })
    let {        
        size,
        vin,
        skip,
        tripId,
    } = args;
    if (!size) {
        size = 10;
    }
    // if (user.role && user.role !== "UNAUTHORISED") {
        try {
            let _source = [];
            // if (fields) {
            //     _source = fields.split(",");
            // }
            let vehicle:any = await prisma.vehicle({
                vin,
            });
            vehicle = {
                ...vehicle,
                model: await prisma.vehicle({vin}).model(),
            };
            if (vehicle.model && vehicle.model.protocol === "PNP") {
                _source = ["tripId", "latitude", "longitude", "timestamp", "batteryVoltageAdc", "gpsSpeed"];
                const query: any = {
                    bool: {
                        must: [{
                            match_all: {},
                        },
                        {
                            match_phrase: {
                                "vin": {
                                    query: vin,
                                },
                                // "startTime": {
                                //     query:   
                                // },
                            },
                        },
                        {
                            match_phrase: {
                                "type": {
                                    query: "ANALYTICS",
                                },
                            },
                        },
                        {
                            exists: {
                              field: "tripId",
                            },
                        }],
                        must_not: [{
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        }],
                    },
                };
                // const ts = await prisma.trips({
                //     first: size,
                //     orderBy: "createdAt_DESC",
                //     skip,
                //     where: {
                //         distance_gt: 0,
                //         vin,
                //     },
                // });
                // const tripIds = [];
                // for (const {tripId} of ts) {
                //     tripIds.push({
                //         match_phrase: {
                //             tripId,
                //         },
                //     });
                // }
                // query.bool.must.push({bool: {should: tripIds}});
                if (skip) {
                    const aggs = {
                        uniq_tripId: {
                            terms: {
                              field: "tripId.keyword",
                              order : {
                                "_key" : "desc",
                              },
                              size: skip,
                            },
                        },
                    };
                    let dataAggs = await searchFromElastic({
                        index: VEHICLE_LEVEL_0_INDEX,
                        query: {
                            _source,
                            query,
                            aggs,
                            size: 1,
                            sort: [
                                {
                                    timestamp: {
                                        order: "desc",
                                        unmapped_type: "boolean",
                                    },
                                },
                            ],
                        },
                    });
                    const aggregations = dataAggs.aggregations.uniq_tripId.buckets;
                    console.log('dataAggs', JSON.stringify(dataAggs));
                    const ind = skip > 0 ? skip - 1 : 0;
                    query.bool.must.push({
                        range: {
                            "tripId" : {
                                lt: aggregations[ind].key,
                            },
                          },
                    });
                    console.log(aggregations[ind].key)
                }
                let searchData, total;
                const data = await searchFromElastic({
                    index: VEHICLE_LEVEL_0_INDEX,
                    query: {
                        _source,
                        query,
                        size: 10000,
                        sort: [
                            {
                                timestamp: {
                                    order: "desc",
                                    unmapped_type: "boolean",
                                },
                            },
                        ],
                        // range: {
                        //     timestamp:{ 
                        //         gt: latestInDb;
                        //     }
                        // },
                    },
                });
                searchData = data.searchData;
                console.log(searchData);
                total = data.total;
                let tripsFetched = 0;
                let i = 0;
                const trips = [];
                const maxGpsSpeed = searchData[i]._source.gpsSpeed;
                const minGpsSpeed = searchData[i]._source.gpsSpeed;
                console.log('Location data', searchData[i]._source.latitude);
                const avgSpeedTotal = 0;
                const totalPositiveSpeedLog = 0;
                while (tripsFetched < size && i < searchData.length) {
                    const ind = trips.findIndex((trip) => {
                        return trip.tripId === searchData[i]._source.tripId;
                    });
                    if (ind === -1) {
                        trips.push({
                            // tslint:disable-next-line:max-line-length
                            avgSpeedTotal: 0,
                            avgGpsSpeed: 0,
                            batteryVoltageAdc: (searchData[i]._source.batteryVoltageAdc ? [searchData[i]._source.batteryVoltageAdc] : []),
                            distance: 0,
                            // endTime: moment(searchData[i]._source.timestamp).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]"),
                            endTime: new Date(searchData[i]._source.timestamp).toISOString(),
                            energy: 0,
                            location: [{
                                lat: parseFloat(searchData[i]._source.latitude),
                                lng: parseFloat(searchData[i]._source.longitude),
                            }],                            
                            maxGpsSpeed: searchData[i]._source.gpsSpeed || 0,
                            minGpsSpeed: searchData[i]._source.gpsSpeed || 0,
                            totalPositiveSpeedLog: 0,
                            tripId: searchData[i]._source.tripId,
                            vin,

                        });
                        tripsFetched++;
                    } else {
                        trips[ind].location.push({
                            lat: parseFloat(searchData[i]._source.latitude),
                            lng: parseFloat(searchData[i]._source.longitude),
                        });
                        // trips[ind].startTime = moment(searchData[i]._source.timestamp).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
                        trips[ind].startTime = new Date(searchData[i]._source.timestamp).toISOString();

                        if (searchData[i]._source.gpsSpeed && searchData[i]._source.gpsSpeed > 0) {
                            trips[ind].totalPositiveSpeedLog = trips[ind].totalPositiveSpeedLog + 1;
                            trips[ind].avgSpeedTotal = trips[ind].avgSpeedTotal + searchData[i]._source.gpsSpeed;
                            trips[ind].avgGpsSpeed = trips[ind].avgSpeedTotal / trips[ind].totalPositiveSpeedLog;
                        }
                        trips[ind].batteryVoltageAdc.push(searchData[i]._source.batteryVoltageAdc);
                        if (searchData[i]._source.gpsSpeed !== 0 &&
                            ((trips[ind].minGpsSpeed !== 0 &&
                            searchData[i]._source.gpsSpeed < trips[ind].minGpsSpeed) ||
                            trips[ind].minGpsSpeed === 0)) {
                                trips[ind].minGpsSpeed = searchData[i]._source.gpsSpeed;
                        }
                        if (searchData[i]._source.gpsSpeed !== 0 &&
                            ((trips[ind].maxGpsSpeed !== 0 &&
                            searchData[i]._source.gpsSpeed > trips[ind].maxGpsSpeed) ||
                            trips[ind].maxGpsSpeed === 0)) {
                                trips[ind].maxGpsSpeed = searchData[i]._source.gpsSpeed;
                        }
                    }
                    i++;
                }

                trips.forEach((trip, ind) => {
                    let distance = 0;
                    const polyline = trip.location;
                    for (let i = 0; i < polyline.length; i++) {
                        if ( polyline[i].lat === 0 && polyline[i].lng === 0) {
                            polyline.splice(i, 1);
                            i--;
                        }
                    }
                    if (polyline.length > 2) {
                        for (let i = 0; i < polyline.length - 1; i++) {
                            const rad = (x) => {
                                return x * Math.PI / 180;
                            };
                            const R = 6378137; // Earth’s mean radius in meter
                            const dLat = rad(polyline[i + 1].lat - polyline[i].lat);
                            const dLong = rad(polyline[i + 1].lng - polyline[i].lng);
                            const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(rad(polyline[i].lat)) * Math.cos(rad(polyline[i + 1].lat)) *
                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            const d = R * c;
                            distance = distance + d; // returns the distance in meter
                        }
                    }
                    trips[ind].distance = distance;
                });
                // for (const {distance, endTime, tripId} of trips) {
                //     console.log(tripId)
                //     await prisma.updateTrip({
                //         data: {
                //             distance,
                //             endTime,
                //         },
                //         where: {
                //             tripId: `${tripId}`,
                //         },
                //     });
                // }
                return trips;
            }
            if (vin === "pnp_test_mohit_01") {
                return [{
                    vin,
                    tripId: "1",
                    distance: 1,
                    energy: 1.2,
                    maxWheelRPM: 68,
                    minWheelRPM: 1,
                    mode: null,
                    startTime: "2020-03-23T20:03:29.790Z",
                    endTime: "2020-03-23T20:13:29.790Z",
                    batteryVoltageAdc: [51.16, 51.12, 51.10, 51.06, 51.05, 51.02, 50.76].reverse(),
                }];
            }
            let type;
            if (["PNP"].indexOf(vehicle.model && vehicle.model.protocol || "") > -1) {
                type = "ANALYTICS";
            }
            let query;
            if (type) {
                query = {
                    bool: {
                        must: [{
                            match_all: {},
                        },
                        {
                            match_phrase: {
                                "vin": {
                                    query: vin,
                                },
                            },
                        },
                        {
                            match_phrase: {
                                "type": {
                                    query: type,
                                },
                            },
                        },
                        {
                            exists: {
                              field: "tripId",
                            },
                        }],
                        must_not: [{
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        }],
                    },
                };
            } else {
                query = {
                    bool: {
                        must: [{
                            match_all: {},
                        },
                        {
                            match_phrase: {
                                "vin": {
                                    query: vin,
                                },
                            },
                        },
                        {
                            exists: {
                              field: "tripId",
                            },
                        },
                        ],
                        must_not: [{
                            match_phrase: {
                                "timestampVerified": {
                                    query: false,
                                },
                            },
                        },
                        {
                            match_phrase: {
                                "type": {
                                    query: "ANALYTICS",
                                },
                            },
                        }],
                    },
                };
            }
            let searchData, total;
            let data = await searchFromElastic({
                index: VEHICLE_LEVEL_0_INDEX,
                query: {
                    _source,
                    query,
                    size: 10000,
                    sort: [
                        {
                            timestamp: {
                                order: "desc",
                                unmapped_type: "boolean",
                            },
                        },
                    ],
                },
            });
            searchData = data.searchData;
            // total = searchData.total;
            let latestTripId = searchData[0]._source.tripId;
            if (skip) {
                latestTripId = latestTripId - skip;
            }
            if (latestTripId <= 0 ) {
                return [];
            }
            const body = [];
            let tripsFetched = 0;
            let tempTripId = latestTripId;
            while (tripsFetched < size && tempTripId !== 0) {
                const newQuery = _.cloneDeep(query);
                newQuery.bool.must.push({
                    match_phrase: {
                        "tripId": {
                            query: tempTripId,
                        },
                    },
                });
                data = await searchFromElastic({
                    index: VEHICLE_LEVEL_0_INDEX,
                    query: {
                        _source,
                        query: newQuery,
                        size: "all",
                        sort: [
                            {
                                timestamp: {
                                    order: "desc",
                                    unmapped_type: "boolean",
                                },
                            },
                        ],
                    },
                });
                let tempSearchData, tempTotal;
                tempSearchData = data.searchData;
                tempSearchData = _.map(tempSearchData, "_source");
                tempTotal = data.total;
                if (tempTotal > 0) {
                    const distance = parseFloat(tempSearchData[0].odometer) -
                        parseFloat(tempSearchData[tempTotal - 1].odometer);
                    if (distance > 100) {
                        body.push(...tempSearchData);
                        tripsFetched++;
                    }
                }
                tempTripId--;
            }
            // }
            const trips = {};
            let i = 0;
            let currentTripId = body[i] && body[i].tripId;
            while (i < body.length) {
                let energyConsumed = 0;
                // let endTime = moment(body[i].timestamp).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
                let endTime = new Date(body[i].timestamp).toISOString();
                // console.log(endTime);
                // if (!endTime.isValid()) {
                //     // tslint:disable-next-line:radix
                //     endTime = moment(body[i].timestamp);
                // }
                let startTime = endTime;
                const endOdo = parseFloat(body[i].odometer);
                let startOdo = endOdo;
                let maxWheelRPM = body[i].wheelRpm;
                let minWheelRPM = body[i].wheelRpm;
                let maxGpsSpeed = body[i].gpsSpeed;
                let minGpsSpeed = body[i].gpsSpeed;
                const avgSpeedTotal = 0;
                const totalPositiveSpeedLog = 0;
                const mode = {
                    ECONOMY: 0,
                    RIDE: 0,
                    SPORT: 0,
                };
                const location = [];
                while (i < body.length && currentTripId === body[i].tripId) {
                    const power = parseFloat(body[i].batteryCurrent) * parseFloat(body[i].batteryVoltage);
                    energyConsumed = energyConsumed + power;
                    startOdo = parseFloat(body[i].odometer);
                    startTime = new Date(body[i].timestamp).toISOString();
                    // if (!startTime.isValid()) {
                    //     // tslint:disable-next-line:radix
                    //     startTime = moment(parseInt(body[i].timestamp));
                    // }
                    if (maxWheelRPM < body[i].wheelRpm) {
                        maxWheelRPM = body[i].wheelRpm;
                    }
                    if ((minWheelRPM === 0 && body[i].minWheelRPM !== 0) ||
                        (minWheelRPM > body[i].wheelRpm && body[i].wheelRpm !== 0)) {
                        minWheelRPM = body[i].wheelRpm;
                    }
                    if (maxGpsSpeed < body[i].gpsSpeed) {
                        maxGpsSpeed = body[i].gpsSpeed;
                    }
                    if ((minGpsSpeed === 0 && body[i].minGpsSpeed !== 0) ||
                        (minGpsSpeed > body[i].gpsSpeed && body[i].gpsSpeed !== 0)) {
                        minGpsSpeed = body[i].gpsSpeed;
                    }
                    if (body[i].mode === 0) {
                        mode.ECONOMY = mode.ECONOMY + 1;
                    }
                    if (body[i].mode === 1) {
                        mode.RIDE = mode.RIDE + 1;
                    }
                    if (body[i].mode === 2) {
                        mode.SPORT = mode.SPORT + 1;
                    }
                    if (body[i].latitude && body[i].longitude) {
                        location.push({lat: parseFloat(body[i].latitude), lng: parseFloat(body[i].longitude)});
                    }
                    i++;
                }
                const distance = (endOdo - startOdo);
                energyConsumed = energyConsumed / 3600000;
                if (trips[currentTripId]) {
                    trips[currentTripId] = {
                        distance: parseFloat(trips[currentTripId].distance) + (distance || 0),
                        endTime: trips[currentTripId].endTime,
                        energy: parseFloat(trips[currentTripId].energy) + (energyConsumed || 0),
                        location: (trips[currentTripId].location.lng && trips[currentTripId].location.lat) ? [...trips[currentTripId].location, ...location] : [...location],
                        maxWheelRPM: trips[currentTripId].maxWheelRPM > maxWheelRPM ?
                            trips[currentTripId].maxWheelRPM : maxWheelRPM,
                        minWheelRPM: trips[currentTripId].minWheelRPM < minWheelRPM &&
                            trips[currentTripId].minWheelRPM !== 0 ?
                                trips[currentTripId].minWheelRPM : minWheelRPM,
                        maxGpsSpeed: trips[currentTripId].maxGpsSpeed > maxGpsSpeed ?
                            trips[currentTripId].maxGpsSpeed : maxGpsSpeed,
                        minGpsSpeed: trips[currentTripId].minGpsSpeed < minGpsSpeed &&
                            trips[currentTripId].minGpsSpeed !== 0 ?
                                trips[currentTripId].minGpsSpeed : minGpsSpeed,
                        mode: {
                            ECONOMY: trips[currentTripId].mode.ECONOMY + mode.ECONOMY,
                            RIDE: trips[currentTripId].mode.RIDE + mode.RIDE,
                            SPORT: trips[currentTripId].mode.SPORT + mode.SPORT,
                        },
                        startTime,
                        tripId: currentTripId,
                        vin: body[i - 1].vin,
                        batteryVoltageAdc: [],
                        avgGpsSpeed: avgSpeedTotal / totalPositiveSpeedLog,
                    };
                } else {
                    trips[currentTripId] = {
                        distance,
                        endTime,
                        energy: energyConsumed || 0,
                        location,
                        maxWheelRPM,
                        minWheelRPM,
                        maxGpsSpeed,
                        minGpsSpeed,
                        mode,
                        startTime,
                        tripId: currentTripId,
                        vin: body[i - 1].vin,
                        batteryVoltageAdc: [],
                        avgGpsSpeed: avgSpeedTotal / totalPositiveSpeedLog,
                    };
                }
                if (i < body.length) {
                    currentTripId = body[i].tripId;
                    // endOdo = parseFloat(body[i].odometer);
                    // startOdo = endOdo;
                    // maxWheelRPM = body[i].wheelRpm;
                    // minWheelRPM = body[i].wheelRpm;
                    // location = [];
                }
            }
            const response: any = Object.values(trips);
            response.forEach((trip: any, index) => {
                // console.log(trip)
                
                const mode: any = trip.mode || {ECONOMY: 0, RIDE: 0, SPORT: 0};
                const count = trip.location.length;
                if (count > 0) {
                    mode["ECONOMY"] = mode && ((mode.ECONOMY * 100) / count).toFixed(2) || 0;
                    mode["RIDE"] = mode && ((mode.RIDE * 100) / count).toFixed(2) || 0;
                    mode["SPORT"] = mode && ((mode.SPORT * 100) / count).toFixed(2) || 0;
                    response[index].mode = mode;
                } else {
                    let total = mode.ECONOMY + mode.RIDE + mode.SPORT;
                    total = total === 0 ? 1 : total;
                    mode["ECONOMY"] = mode.ECONOMY * 100 / total;
                    mode["RIDE"] = mode.RIDE * 100 / total;
                    mode["SPORT"] = mode.SPORT * 100 / total;
                    response[index].mode = mode;
                }
            });
            return response;
        } catch (err) {
            console.log("error", err);
            return [];
        }
    // }
    // return [];
};

//TripIds from Elastic Based ON range
const tripIdsV2Range = async (args) => {
    let {       
        vin,        
        startTime,
        endTime,
        skip       
    } = args;
    try {
        const query: any = {
            bool: {
                must: [
                    {
                        match_all: {},
                    },
                    {
                        match_phrase: {
                            "vin": {
                                query: vin,
                            },
                        },
                    },
                    {
                        exists: {
                          field: "tripId",
                        },
                    },
                    {
                        range: {
                            "timestamp": {
                                "gte": startTime,
                                "lte": endTime
                            }
                        }
                    }
                ],
                must_not: [
                    {
                        match_phrase: {
                            "timestampVerified": {
                                query: false,
                            },
                        },
                    }, 
                    {
                        match_phrase: {
                            "latitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "longitude": {
                                query: 0
                            }
                        }
                    },
                    {
                        match_phrase: {
                            "batteryVoltageAdc": 0
                        }
                    }
                ],
            },
        };        
        let data = await elastic.searchWithoutScroll({
            index: VEHICLE_LEVEL_2_INDEX,
            query: {
                _source: ["tripId", "vin"],
                query,
                collapse: {
                    field: "tripId"
                },
                size: 10000,
                from:  skip,
                aggs: {
                    uniq_tripId: {
                        cardinality: {
                          field: "tripId",
                        },
                    },
                },
                sort: [
                    {
                        timestamp: {
                            order: "desc",
                        },
                    },
                ],
            },
        });
        let body = data.searchData;
        // console.log('tripIdsV2 body VIN: ', JSON.stringify(data));
        let tripId = body.length && body.map(b => b._source.tripId.toString()) || [];
        let tripCount = data.aggs && data.aggs.uniq_tripId && data.aggs.uniq_tripId.value || 0;
        return {tripId, tripCount}
    } catch (e) {
        console.log('Error fetching trip Ids: ', e);
        return {tripIds: [], tripCount: 0}
    }
}


export {
    getTripData,
    getTripLogs,
    searchVehicleLogs,
    trip,
    tripV2,
    tripIdsV2,
    tripV2Logs,
    tripIdsV2Range
};
