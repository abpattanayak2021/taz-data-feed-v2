import * as AWS from 'aws-sdk';
import { Client } from 'elasticsearch';
import * as esAWSHandler from 'http-aws-es';
import * as _ from 'lodash';
import { appConfig } from '../../../src/config';
import { ITransport } from './transport';

function checkConfig(...args) {
  return !_.some(args, _.isEmpty);
}
class ElasticTransport implements ITransport {
  client: Client;
  APP_INDEX: string;

  constructor() {
    const accessKeyId = appConfig.aws.accessKeyId;
    const secretAccessKey = appConfig.aws.accessKey;
    const region = appConfig.aws.region;
    const esHost = appConfig.elastic.uri;
    this.APP_INDEX = "rev-es";
    this.client = null;
    if (checkConfig(accessKeyId, secretAccessKey, region, esHost)) {
      this.client = new Client({
        awsConfig: new AWS.Config({
          accessKeyId,
          region,
          secretAccessKey,
        }),
        connectionClass: esAWSHandler,
        hosts: [esHost],
      });
      /* this.createIndex(defaultMapping, "vehicle000").then((result) => {
        console.log("ESService", result);
        this.createIndex(defaultMapping, 'vehicle001').then((result) => {
          console.log('ESService', result);
          this.ready = true;
        }).catch((error) => {
          console.error('ESService', error);
        });
      }).catch((error) => {
        console.error("ESService", error);
      }); */

    }
  }

  async createIndex(mappings, index): Promise<any> {
    const appIndexId = this.APP_INDEX;    
    if (this.client) {
      mappings.aliases = {
        "all-events": {},
      };
      const indexExists = await this.client.indices.exists({
        index: `events-${appIndexId}-${index}`,
      });
      if (!indexExists) {
        return this.client.indices.create({
          body: {},
          index: `events-${appIndexId}-${index}`,
        });
      } else {
        return {
          already_exists: true,
        };
      }
    }
    return Promise.reject("No Client Found");
  }

  async track({event, index}): Promise<any> {
    const appIndexId = this.APP_INDEX;
    console.log("Logging to ES");
    if (this.client) {
      return this.client.index({
        body: event,
        index: `events-${appIndexId}-${index}`,
      });
    }
    return Promise.reject("No Client Found");
  }

  async searchWithoutScroll({query, index}): Promise<any> {
    const appIndexId = this.APP_INDEX;
    if (this.client) {
      const data: any = [];
      
      let res = await this.client.search({
        body: query,
        index: `events-${appIndexId}-${index}`,
      });
      res.hits.hits.forEach((hit) => {
        data.push(hit);
      });
      return {searchData: data, total: res.hits.total, aggs: res.aggregations};
    }
  }

  async search({query, index}): Promise<any> {
    const appIndexId = this.APP_INDEX;    
    if (this.client) {
      // console.log('Client Entry');
      const data: any = [];
      let querySize = query.size || 10000;
      query.size = query.size < 10000 ? query.size : 10000;
      let res = await this.client.search({
        body: query,
        index: `events-${appIndexId}-${index}`,
        scroll: "10s",
      });
      res.hits.hits.forEach((hit) => {
        data.push(hit);
      });
      if (querySize === "all") {
        querySize = res.hits.total;
      }
      if (querySize > 10000) {
        let maxDataLength;
        if (querySize <= res.hits.total) {
          maxDataLength = querySize;
        } else {
          maxDataLength = res.hits.total;
        }
        while (data.length < maxDataLength) {
          const scrollId = res._scroll_id;
          res = await this.client.scroll({
            scroll: "10s",
            scrollId,
          });
          res.hits.hits.forEach((hit) => {
            data.push(hit);
          });
        }
      };
      let aggregations: any = [];
      if (res.aggregations) {
        aggregations = res.aggregations;
      }
      return {searchData: data, total: res.hits.total, aggregations};
    }

    return Promise.reject("No Client Found");
  }
}

const defaultMapping = {
  mappings: {
    event: {
      properties: {
        _id: {
          type: "keyword",
          index: "not_analyzed",
        },
        timestamp: {
          type: "date",
        },
        vin: {
          analyzer: "standard",
          type: "keyword",
        },
      },
    },
  },
};

const elastic = new ElasticTransport();
export {
  elastic,
};

