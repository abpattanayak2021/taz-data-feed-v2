export enum eventType {
    CHANGE_LOG = "CHANGE_LOG",
    TRIP = "TRIP",
    VEHICLE_LOG = "VEHICLE_LOG",
  }
  
  interface IESEvent {
    type: eventType;
    timestamp: Date;
  }
  
  export interface ITransport {
    track(data: {
      event: IESEvent;
      index: string;
    }): Promise<any>;
    search(data: {
      query: any;
      index: string;
    }): Promise<any>;
  }
  