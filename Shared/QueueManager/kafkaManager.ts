import {Kafka} from 'kafkajs';


const brokers = [
  'kafka-0.kafka-headless.default.svc.cluster.local:9092',
  'kafka-1.kafka-headless.default.svc.cluster.local:9092',
]

const brokersProd = [
  'kafka-0.kafka-headless.default.svc.cluster.local:9092',
  'kafka-1.kafka-headless.default.svc.cluster.local:9092',
  'kafka-1.kafka-headless.default.svc.cluster.local:9092',
]

const brokersExternal = [
  "a2c9e7bb95ee14306a3cebf566e249ed-759729163.ap-southeast-1.elb.amazonaws.com:9094",
  "ad604c2d3080a41d5b484563642048aa-1398554707.ap-southeast-1.elb.amazonaws.com:9094",
]

const brokersExternalProd = [
  "afcaf52df769f4f9488418e19b4cad0a-1534225722.ap-southeast-1.elb.amazonaws.com:9094",
  "a9cb8db78bb1244999cc5cd4ef98ba67-1053439353.ap-southeast-1.elb.amazonaws.com:9094",
  "a6e4cb26a666c40c595631929fd2468e-2122440510.ap-southeast-1.elb.amazonaws.com:9094"
]


let kafkaBrokers;
if (process.env.NODE_ENV === "LOCAL") {
    kafkaBrokers = brokersExternal;
} else if (process.env.NODE_ENV === 'DEV'){
    kafkaBrokers = brokers;
} else {
  kafkaBrokers = brokersProd;
}
const kafka = new Kafka({
  clientId: 'my-app',
  brokers: kafkaBrokers,
  sasl: {
      mechanism:"plain",
      username:"user",
      password:"kafka_1234"
  }
})

export const publishToTopic = async (topic, messages) => {
    // console.log('publishToTopic', topic, messages);

    const producer = kafka.producer()

    await producer.connect()
    

    const payload = {topic, messages}
    await producer.send(payload);

    await producer.disconnect()
    return true;
}

export const getConsumerForTopic = async (topic) => {
    console.log('getConsumerForTopic: ', topic);
    const consumer = kafka.consumer({ groupId: topic, readUncommitted: true })

    await consumer.connect()
    await consumer.subscribe({ topic, fromBeginning: false })
    return consumer;
}