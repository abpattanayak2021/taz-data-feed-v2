/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author ishaan
 * @description message queues config
 *
 */

import * as Queue from 'bull';

export const queueList = {
  SOCKET_POINTER: 'SOCKET_POINTER',
  VEHICLE_LOGS_MOBILE: 'VEHICLE_LOGS_MOBILE',
  VEHICLE_LOGS_POINTER: 'VEHICLE_LOGS_POINTER',
  VEHICLE_COMMANDS: 'VEHICLE_COMMAND',
  VEHICLE_LOGS_ICONCOX: 'VEHICLE_LOGS_ICONCOX',
};

export const queues = {
  [queueList.VEHICLE_LOGS_MOBILE]: new Queue(
    queueList.VEHICLE_LOGS_MOBILE,
    process.env.REDIS_URL
  ),
  [queueList.VEHICLE_LOGS_POINTER]: new Queue(
    queueList.VEHICLE_LOGS_POINTER,
    process.env.REDIS_URL
  ),
  [queueList.VEHICLE_COMMANDS]: new Queue(
    queueList.VEHICLE_COMMANDS,
    process.env.REDIS_URL
  ),
  [queueList.SOCKET_POINTER]: new Queue(
    queueList.SOCKET_POINTER,
    process.env.REDIS_URL
  ),
  [queueList.VEHICLE_LOGS_ICONCOX]: new Queue(
    queueList.VEHICLE_LOGS_ICONCOX,
    process.env.REDIS_URL
  )
};