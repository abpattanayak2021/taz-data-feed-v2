import { company } from './company';
import { user } from './user';
import { vehicle } from './vehicle';

export const data = {
    company,
    user,
    vehicle,
}
