/**
 * Copyright (C) 2020 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author ishaan
 * @description user prisma
 *
 */
import { db } from '../CacheManager/cacheManager';
import { prisma } from '../DBManager/Prisma/prisma-client';

const get = async (key) => {
    let user = await db.get(`USER_${key}`);
    if (!user) {
        user = await prisma.user({id: key});
        if (user) {
            await db.set(`USER_${key}`, user);
        }
    }
    return user;
};

export const user = {
    get,
};