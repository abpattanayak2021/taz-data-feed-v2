import { db } from '../CacheManager/cacheManager';
import { prisma } from '../DBManager/Prisma/prisma-client';

const getFromToken = async (key) => {
    let company = await db.get(`COMPANY_TOKEN_${key}`);
    if (!company) {
        company = (await prisma.companies({where: {token: key}}))[0];
        if (company) {
            await db.set(`COMPANY_TOKEN_${key}`, company);
        }
    }
    return company;
};

export const company = {
    getFromToken,
};