import { db } from '../CacheManager/cacheManager';
import { prisma } from '../DBManager/Prisma/prisma-client';

const fence = async (key) => {
    let fence = await db.get(`VEHICLE_FENCE_${key}`);
    if (!fence) {
        fence = await prisma.vehicle({vin: key}).fence().vertices();
        if (fence) {
            await db.set(`VEHICLE_FENCE_${key}`, fence);
        }
    }
    return fence;
};

const _vehicle = async (key) => {
    let vehicle = await db.get(`VEHICLE_${key}`);
    if (!vehicle) {
        vehicle = await prisma.vehicle({vin: key});
        const owner = await prisma.vehicle({vin: key}).owner();
        vehicle = {
            ...vehicle,
            owner,
        };
        if (vehicle) {
            await db.set(`VEHICLE_${key}`, vehicle);
        }
    }
    return vehicle;
};

const setFenceNotificationFlag = async (key, value) => {
    let vehicle = await db.get(`VEHICLE_${key}`);
    if (!vehicle) {
        vehicle = await prisma.vehicle({vin: key});
        const owner = await prisma.vehicle({vin: key}).owner();
        vehicle = {
            ...vehicle,
            owner,
        };
    }
    if (vehicle) {
        vehicle = {
            ...vehicle,
            fenceBreachNotified: value,
        };
        await db.set(`VEHICLE_${key}`, vehicle);
    }
    return vehicle;
};

export const vehicle = {
    fence,
    setFenceNotificationFlag,
    get: _vehicle,
};