/**
 * Copyright (C) 2019 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * @author ishaan
 * @description cache manager
 *
 */

import * as Redis from 'ioredis';
import { v4 as uuidV4 } from 'uuid';
import Logger from '../../src/utilities/logger';

const REDIS_URL_DEV = 'localhost:6379'
 
 const REDIS_URL = {
     sentinels: [
     { host: "redis.default.svc.cluster.local", port: 26379 },
     ],
     name: "redis-master",
     password: "redis_1234",
     sentinelPassword: "redis_1234"
   } 
 let redis;
 
 if (process.env.NODE_ENV === "LOCAL") {
     redis = new Redis(REDIS_URL_DEV);
 } else {
     redis = new Redis(REDIS_URL);
 }

const set = async (key, value, expiry = 240) => {
    try {
        if (expiry === null) {
            return redis.set(key, JSON.stringify(value));
        }
        return redis.set(key, JSON.stringify(value), 'EX', expiry);
    } catch (e) {
        Logger.error(`Error in setting key value on Redis: ${e.stack}`);
        return;
    }
}
const get = async (key) => {
    try {
        return JSON.parse(await redis.get(key));
    } catch (e) {
        Logger.error(`Error in getting key value from Redis: ${e.stack}`);
        return;
    }
}
const getAll = async () => {
    try {
        const keys = await redis.collection.keys('*');
        const values = await redis.collection.mget(keys);
        return JSON.parse(values);
    } catch (e) {
        Logger.error(`Error in getting all key values from Redis: ${e.stack}`);
        return;
    }
}
const remove = async (key) => {
    try {
        return redis.del(key);
    } catch(e) {
        Logger.error(`Error in removing key value from Redis: ${e.stack}`);
        return;
    }
}

export const db = {
    get,
    getAll,
    set,
    remove
};
