FROM node:11
LABEL maintainer="himanshu@revos.in"  
WORKDIR /var/www/app/taz-datafeed-v2-server

# install deps
COPY package.json /var/www/app/taz-datafeed-v2-server
COPY .env /var/www/app/taz-datafeed-v2-server
RUN npm install

# Setup workdir
COPY build/. /var/www/app/taz-datafeed-v2-server/build

# run
EXPOSE 4000
CMD ["npm", "start"]